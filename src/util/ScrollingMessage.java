package util;

public class ScrollingMessage {
	public String text;
	public int width;
	public java.awt.Color c = java.awt.Color.BLACK;
	public int pixelAt;

	public ScrollingMessage(String s, int width, int pixelAt) {
		this.text = s;
		this.width = width;
		this.pixelAt = pixelAt;
	}
}
package util;

import java.awt.*;
import java.awt.image.*;
import javax.swing.JPanel;

import framework.Point2D;

import java.util.*;

public class Methods {
	public static double intPow(double num, int pow) {
		double tot = 1;
		for (int i = 0; i < pow; i++)
			tot *= num;
		return tot;
	}

	public static Color randomColor() {
		return new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
	}

	public static Point m() {
		return MouseInfo.getPointerInfo().getLocation();
	}

	public static String getNumberWithin(double num, double prec) {
		return "" + ((double) ((int) (num / prec)) * (double) prec);
	}

	public static boolean colorsAreClose(int tolerence, int... colors) {
		int r1 = (colors[0] >> 16) & 0xff;
		int g1 = (colors[0] >> 8) & 0xff;
		int b1 = (colors[0] >> 0) & 0xff;
		for (int c : colors) {
			int r = (c >> 16) & 0xff;
			int g = (c >> 8) & 0xff;
			int b = (c >> 0) & 0xff;
			if (!(Math.abs(r - r1) <= tolerence && Math.abs(g - g1) <= tolerence && Math.abs(b - b1) <= tolerence))
				return false;
		}
		return true;
	}

	public static <T> T[] getArray(ArrayList<T> list, T[] destination) {
		for (int i = 0; i < list.size(); i++)
			destination[i] = list.get(i);

		return destination;
	}

	public static Point getClosestPointOn(Rectangle rect, Point p) {
		if (rect.contains(p))
			return p;

		if (p.x - rect.x < rect.width && p.x - rect.x > 0)
			if (p.y < rect.y)
				return new Point(p.x, rect.y);
			else
				return new Point(p.x, rect.y + rect.height);

		if (p.y - rect.y < rect.height && p.y - rect.y > 0)
			if (p.x < rect.x)
				return new Point(rect.x, p.y);
			else
				return new Point(rect.x + rect.width, p.y);

		if (p.x < rect.x && p.y < rect.y)
			return new Point(rect.x, rect.y);
		if (p.x < rect.x && p.y > rect.y + rect.height)
			return new Point(rect.x, rect.y + rect.height);
		if (p.x > rect.x + rect.width && p.y < rect.y)
			return new Point(rect.x + rect.width, rect.y);
		return new Point(rect.x + rect.width, rect.y + rect.height);
	}

	public static double getAngle(double ang, boolean positive, boolean abs) {
		if (positive)
			while (ang < 0)
				ang += Math.PI * 2;
		else
			while (ang > 0)
				ang -= Math.PI * 2;

		ang %= 360;
		return (abs ? Math.abs(ang) : ang);
	}

	public static boolean contains(int w, int h, int x, int y, int a, int b) {
		if ((w | h) < 0)// At least one of the dimensions is negative...
			return false;

		// Note: if either dimension is zero, tests below must return false...
		if (a < x || b < y)
			return false;
		w += x;
		h += y;

		return ((w < x || w > a) && (h < y || h > b));
	}

	public static Image scale(Image img, int width, int height) {
		// Pairs: (width,h) and (w,height)
		int w, h;
		{
			double scalar = width / (double) img.getWidth(null);
			h = (int) (scalar * img.getHeight(null));
		}
		{
			double scalar = height / (double) img.getHeight(null);
			w = (int) (scalar * img.getWidth(null));
		}

		boolean b = (w < width);
		return img.getScaledInstance(b ? w : width, b ? height : h, Image.SCALE_SMOOTH);
	}

	public static Image fuzz(Image img, int rgb, int r, JPanel p) {
		int w = img.getWidth(null), h = img.getHeight(null);
		int[] pixels = new int[w * h];
		PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pixels, 0, w);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
			System.err.println("interrupted waiting for pixels!");
		}
		if ((pg.getStatus() & ImageObserver.ABORT) != 0)
			System.err.println("image fetch aborted or errored");

		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				double d = min(i, w - i, j, h - j);
				if (d < r)
					pixels[i + j * w] = (int) (pixels[i + j * w] * (d / (double) r));
			}
		}

		MemoryImageSource source = new MemoryImageSource(w, h, pixels, 0, w);
		return p.createImage(source);
	}

	public static double normalize(double angle) {
		while (angle < -Math.PI)
			angle += 2 * Math.PI;
		while (angle >= Math.PI)
			angle -= 2 * Math.PI;

		return angle;
	}

	public static Point2D getRotatedPoint(Point2D p, double angle) {
		return new Point2D(p.x * Math.cos(angle) - p.y * Math.sin(angle), p.y * Math.cos(angle) + p.x * Math.sin(angle));
	}

	public static void rotate(Point2D p, double angle) {
		p.set(p.x * Math.cos(angle) - p.y * Math.sin(angle), p.y * Math.cos(angle) + p.x * Math.sin(angle));
	}

	public static double getRotateX(double x, double y, double angle) {
		return x * Math.cos(angle) - y * Math.sin(angle);
	}

	public static double getRotateY(double x, double y, double angle) {
		return y * Math.cos(angle) + x * Math.sin(angle);
	}

	public static Point2D getRotatedPoint(double x, double y, double angle) {
		return new Point2D(x * Math.cos(angle) - y * Math.sin(angle), y * Math.cos(angle) + x * Math.sin(angle));
	}

	/*
	 * http://www.dreamincode.net/code/snippet516.htm
	 */
	public static void insertionSort(int[] list, int length) {
		int firstOutOfOrder, location, temp;

		for (firstOutOfOrder = 1; firstOutOfOrder < length; firstOutOfOrder++) { // Starts
																					// at
																					// second
																					// term,
																					// goes
																					// until
																					// the
																					// end
																					// of
																					// the
																					// array.
			if (list[firstOutOfOrder] < list[firstOutOfOrder - 1]) { // If the
																		// two
																		// are
																		// out
																		// of
																		// order,
																		// we
																		// move
																		// the
																		// element
																		// to
																		// its
																		// rightful
																		// place.
				temp = list[firstOutOfOrder];
				location = firstOutOfOrder;

				do { // Keep moving down the array until we find exactly where
						// it's supposed to go.
					list[location] = list[location - 1];
					location--;
				} while (location > 0 && list[location - 1] > temp);

				list[location] = temp;
			}
		}
	}

	public static <T> boolean contains(T[] array, T object) {
		for (T obj : array)
			if (obj.equals(object))
				return true;
		return false;
	}

	public static <T> void shuffle(T[] stuff) {
		for (int i = 0; i < 40; i++)
			swap(stuff, (int) (Math.random() * stuff.length), (int) (Math.random() * stuff.length));
	}

	public static <T> void swap(T[] stuff, int posA, int posB) {
		T inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static double max(double... data) {
		double max = Double.MIN_VALUE;

		for (double d : data)
			if (d > max)
				max = d;

		return max;
	}

	public static double min(double... data) {
		double min = Double.MAX_VALUE;

		for (double d : data)
			if (d < min)
				min = d;

		return min;
	}

	public static double linearInterpolate(double a, double b, double x) {
		return a * (1 - x) + b * x;
	}

	public static Point2D lineIntersect(Point2D p1, Point2D p2, Point2D p3, Point2D p4) {
		return new Point2D(((p1.x * p2.y - p1.y * p2.x) * (p3.x - p4.x) - (p1.x - p2.x) * (p3.x * p4.y - p3.y * p4.x))
				/ ((p1.x - p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x - p4.x)), ((p1.x * p2.y - p1.y * p2.x)
				* (p3.y - p4.y) - (p1.y - p2.y) * (p3.x * p4.y - p3.y * p4.x))
				/ ((p1.x - p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x - p4.x)));
	}
}
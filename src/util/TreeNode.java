package util;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {
	public T data;
	public List<TreeNode<T>> children;
	public int level;
	public TreeNode<T> parent;

	TreeNode() {
		children = new ArrayList<TreeNode<T>>();
	}

	TreeNode(T data) {
		this.data = data;
	}

	TreeNode(T data, List<TreeNode<T>> childs) {
		this.data = data;
		this.children = childs;
	}

	public void setData(T data2) {
		this.data = data2;
	}

	public T getData() {
		return data;
	}

	public void setChildAt(TreeNode<T> t, int index) {
		children.set(index, t);
	}

	public void removeChild(TreeNode<T> tn) {
		children.remove(tn);
	}

	public List<TreeNode<T>> getChildren() {
		return children;
	}

	public int getNumChildren() {
		return children.size();
	}

	public void addChild(TreeNode<T> child) {
		children.add(child);
	}

	public void setChildren(TreeNode<T>[] children) {
		this.children.clear();
		for (TreeNode<T> t : children)
			this.children.add(t);
	}

	public void setChildren(ArrayList<TreeNode<T>> ch) {
		this.children = ch;
	}
}
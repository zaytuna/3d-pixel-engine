package util;

import java.awt.*;
import javax.swing.*;

public class Colorful extends JPanel {
	private static final long serialVersionUID = 6415414821824157824L;
	public Color c1 = new Color(100, 250, 200);
	public Color c2 = Color.black;
	public Point p = null;
	public Point p2 = null;
	Paint paint = null;

	public Colorful() {
		c1 = null;
		c2 = null;
	}

	public Colorful(Color c12, Color c21) {
		super(true);
		c1 = c12;
		c2 = c21;
	}

	public Colorful(Color c1, Color c2, Point p) {
		super(true);
		this.c2 = c2;
		this.c1 = c1;
		this.p = p;
	}

	public Colorful(Color c1, Color c2, Point p, Point p2) {
		super(true);
		this.c2 = c2;
		this.c1 = c1;
		this.p = p;
		this.p2 = p2;
	}

	public void paint() {
		repaint();
	}

	public void setPaint(Paint p) {
		this.paint = p;
	}

	public void paintComponent(Graphics g22) {
		super.paintComponent(g22);
		Graphics2D g = (Graphics2D) g22;

		if (!(c1 == null || c2 == null)) {
			boolean point = p == null;
			boolean point2 = p2 == null;
			g.setPaint(paint == null ? new GradientPaint(point2 ? 0 : p2.x, point2 ? 0 : p2.y, c1, point ? getWidth()
					: p.x, point ? getHeight() : p.y, c2) : paint);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}
}
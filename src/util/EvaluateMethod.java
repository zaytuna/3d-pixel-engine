package util;

import javax.swing.JOptionPane;

public class EvaluateMethod {
	EvaluateMethod() {
		do {
			JOptionPane.showMessageDialog(null, "The answer is "
					+ evaluate(JOptionPane.showInputDialog("Enter an expression")));
		} while (true);
	}

	public static double evaluate(String line) {
		line.replaceAll("log", "L");
		line.replaceAll("ln", "N");
		line.replaceAll("sin", "S");
		line.replaceAll("cos", "C");
		line.replaceAll("tan", "T");
		line.replaceAll("arcsin", "s");
		line.replaceAll("arccos", "c");
		line.replaceAll("arctan", "t");
		line.replaceAll("Log", "L");
		line.replaceAll("Ln", "N");
		line.replaceAll("Sin", "S");
		line.replaceAll("Cos", "C");
		line.replaceAll("Tan", "T");
		line.replaceAll("Arcsin", "s");
		line.replaceAll("Arccos", "c");
		line.replaceAll("Arctan", "t");
		int bounds1 = -1;
		int bounds2 = -1;
		double answer = 0;
		StringBuilder build = new StringBuilder(line);

		for (int i = 1; i < line.length(); i++) {
			char c = line.charAt(i);
			if (c == '-' && (Character.isDigit(line.charAt(i - 1)) || line.charAt(i - 1) == '.')) {
				build.replace(i, i + 1, "m");
				line = build.toString();
			}
		}
		while (line.indexOf(")(") > 0) {
			build.replace(build.indexOf(")("), build.indexOf(")(") + 2, ")*(");
			line = build.toString();
		}
		while (line.indexOf(")") >= 0) {
			if (line.indexOf("(") >= 0) {
				bounds2 = line.indexOf(")");
				for (int i = (bounds2 - 1); i >= 0; i--) {
					if (build.charAt(i) == '(') {
						bounds1 = i;
						break;
					}
				}
				build.replace(bounds1, (bounds2 + 1), ""
						+ EvaluateMethod.noParenthesis(build.toString().substring(bounds1, (bounds2 + 1))));
				line = build.toString();
			} else {
				JOptionPane.showMessageDialog(null, "ERROR!!!\n\nMore ending parenthisis than begining ones!\n\n"
						+ line, "JKL:", 0);
				break;
			}
		}
		answer = EvaluateMethod.noParenthesis(line);
		return answer;
	}

	public static double noParenthesis(String line) {
		int center = 0;
		int first = 0;
		int last = 0;
		double result = 0;
		StringBuilder builder = new StringBuilder(line);
		if (line.charAt(0) == '(')
			builder.deleteCharAt(0);
		if (builder.charAt(builder.length() - 1) == ')')
			builder.deleteCharAt(builder.length() - 1);
		line = builder.toString();
		while (line.indexOf(" ") >= 0) {
			builder.deleteCharAt(builder.indexOf(" "));
			line = builder.toString();
		}
		for (int i = 0; i < line.length(); i++) {
			try {
				if (line.charAt(i) == '-' && (Character.isDigit(line.charAt(i - 1)) || line.charAt(i - 1) == '.')) {
					builder.replace(i, i + 1, "m");
				}
				line = builder.toString();
			} catch (IndexOutOfBoundsException e) {
			}
		}

		do {
			if (line.indexOf('C') > 0) {
				center = line.indexOf("C");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + EvaluateMethod.calculateCombinations((int) num1, (int) num2));
				line = build.toString();
			} else if (line.indexOf('P') > 0) {
				center = line.indexOf("P");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + EvaluateMethod.calculatePermutations((int) num1, (int) num2));
				line = build.toString();
			} else if (line.indexOf('R') > 0) {
				center = line.indexOf("R");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == 'E') == false) && ((line.charAt(i) == '-') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + (num2 * ((int) ((1 / num2) * (num1 + (num2 / 2))))));
				line = build.toString();
			} else if (line.indexOf("L") >= 0) {
				center = line.indexOf("L");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.log10(num));
				line = build.toString();
			} else if (line.indexOf("N") >= 0) {
				center = line.indexOf("N");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.log(num));
				line = build.toString();
			} else if (line.indexOf("S") >= 0) {
				center = line.indexOf("S");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.sin(num));
				line = build.toString();
			} else if (line.indexOf("O") >= 0) {
				center = line.indexOf("O");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.cos(num));
				line = build.toString();
			} else if (line.indexOf("T") >= 0) {
				center = line.indexOf("T");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.tan(num));
				line = build.toString();
			} else if (line.indexOf("s") >= 0) {
				center = line.indexOf("s");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.asin(num));
				line = build.toString();
			} else if (line.indexOf("c") >= 0) {
				center = line.indexOf("c");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.acos(num));
				line = build.toString();
			} else if (line.indexOf("t") >= 0) {
				center = line.indexOf("t");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false) && ((line
							.charAt(i) == '-' && Character.isDigit(line.charAt(i - 1))) || line.charAt(i) == '.'))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.atan(num));
				line = build.toString();
			} else if (line.indexOf("F") >= 0) {
				center = line.indexOf("F");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + EvaluateMethod.fibinacci((long) num));
				line = build.toString();
			} else if (line.indexOf("A") >= 0) {
				center = line.indexOf("A");
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(center, (last), "" + Math.abs(num));
				line = build.toString();
			} else if (line.indexOf("!") >= 0) {
				last = line.indexOf("!");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				int num = (int) Double.parseDouble(line.substring(first, (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last + 1), "" + EvaluateMethod.factorial(num));
				line = build.toString();
			} else if (line.indexOf("^") >= 0) {
				center = line.indexOf("^");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + Math.pow(num1, num2));
				line = build.toString();
			} else if (line.indexOf("*") > 0) {
				center = line.indexOf("*");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + (num1 * num2));
				line = build.toString();
			} else if (line.indexOf("/") > 0) {
				center = line.indexOf("/");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + (num1 / num2));
				line = build.toString();
			} else if (line.indexOf("%") > 0) {
				center = line.indexOf("%");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + (num1 % num2));
				line = build.toString();
			} else if (line.indexOf("+") > 0) {
				center = line.indexOf("+");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + (num1 + num2));
				line = build.toString();
			} else if (line.indexOf("m") >= 0
					&& (Character.isDigit(line.charAt(line.indexOf("m") - 1)) || line.charAt(line.indexOf("m") - 1) == '.')) {
				center = line.indexOf("m");
				for (int i = (center - 1); i >= 0; i--) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == 0) {
						first = i;
						if (first != 0)
							first++;
						break;
					}
				}
				for (int i = (center + 1); i < line.length(); i++) {
					if (((Character.isDigit(line.charAt(i)) == false) && ((line.charAt(i) == '.') == false)
							&& ((line.charAt(i) == '-') == false) && ((line.charAt(i) == 'E') == false))
							|| i == line.length() - 1) {
						last = i;
						if (i == line.length() - 1)
							last++;
						break;
					}
				}
				double num1 = Double.parseDouble(line.substring(first, (center)));
				double num2 = Double.parseDouble(line.substring((center + 1), (last)));
				StringBuilder build = new StringBuilder(line);
				build.replace(first, (last), "" + (num1 - num2));
				line = build.toString();
			} else
				break;
		} while (true);
		// JOptionPane.showMessageDialog(null,original+" = "+line);
		result = Double.parseDouble(line);
		return result;
	}

	public static double calculateCombinations(int intOne, int intTwo) {
		double answer2 = 1;
		double number3 = 1;
		double number4 = 1;

		if (intOne == 0) {
			answer2 = 1;
		}
		for (int v = (int) intOne; v > 0; v--) {
			if (v == 0)
				break;

			answer2 = (answer2 * v);
		}
		number3 = intTwo;
		if ((intTwo - 1) == 0) {
			number3 = 1;
		}
		for (int q = (int) (intTwo - 1); q > 0; q--) {
			if (q == 0)
				break;

			number3 = (number3 * q);
		}
		number4 = (intOne - intTwo);
		if ((intOne - intTwo) == 0) {
			number4 = 1;
		}
		for (int z = (int) (intOne - intTwo - 1); z > 0; z--) {
			if (z == 0)
				break;

			number4 = (number4 * z);
		}
		answer2 = (answer2 / number4) / number3;
		return (double) answer2;
	}

	public static String factorNumber(String a) {
		int[] primelst = new int[50];
		int[] factorslst = new int[150];

		double number = 0;
		String factorsList = "";
		String primeList = "";
		int numOfFactors = 0;
		int nextFactor;
		String output;
		boolean isNegative = false;
		boolean isPrime = false;

		if (a.charAt(0) == '-') {
			isNegative = true;
		}// end of negative
		number = Math.abs(Double.parseDouble(a));
		for (int i = 1; i < number; i++) {
			if (number % i == 0) {
				factorsList = factorsList + " " + i + ",";
				numOfFactors++;
				factorslst[numOfFactors] = i;
			}// end of find factors
		}

		if (numOfFactors == 1) {
			isPrime = true;
		} else {
			int primes = 0;
			nextFactor = (int) number;
			for (int i = 2; i <= nextFactor; i++) {
				if (nextFactor % i == 0 || nextFactor == i) {
					primes++;
					primeList = primeList + i + " x ";
					primelst[primes] = i;

					if (i == nextFactor) {
						break;
					} else {
						nextFactor = (nextFactor / i);
						i = 1;
					}
				}

			}// end of prime factors
		}
		if (isNegative == false) {
			if (isPrime == true) {
				output = "The number is prime\nThe number's prime factors are 1 and " + number;

			} else {
				output = "The prime factors are " + primeList + "1" + "\nThe other factors are " + factorsList
						+ " and " + number;
			}
		} else {
			if (isPrime == true) {
				output = "The number is prime\nThe number's prime factors are -1 and " + number;
			} else {
				output = "The prime factors are " + primeList + "-1" + "\nThe other factors are " + factorsList
						+ " and " + number;
			}
		}
		primeList = "";
		factorsList = "";
		numOfFactors = 0;
		isPrime = false;
		isNegative = false;
		return output;
	}

	public static long fibinacci(long n) {
		if (n == 0 || n == 1)
			return n;
		else
			return fibinacci((n - 1)) + fibinacci((n - 2));
	}

	public static double factorial(int number) {
		double answer = 1;
		for (int i = number; i > 0; i--)
			answer *= i;
		return answer;
	}

	public static double calculatePermutations(int intOne, int intTwo) {
		double answer2 = 1;
		double number4 = 1;

		if (intOne == 0) {
			answer2 = 1;
		}
		for (int v = (int) intOne; v > 0; v--) {
			if (v == 0)
				break;

			answer2 = (answer2 * v);
		}
		number4 = (intOne - intTwo);
		if ((intOne - intTwo) == 0) {
			number4 = 1;
		}
		for (int z = (int) ((intOne - intTwo) - 1); z > 0; z--) {
			if (z == 0)
				break;

			number4 = (number4 * z);
		}
		answer2 = answer2 / number4;
		return (double) answer2;
	}

	public static void main(String args[]) {
		new EvaluateMethod();
	}
}
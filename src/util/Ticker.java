package util;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Ticker extends Colorful implements Runnable {
	private static final long serialVersionUID = -3072519662468037628L;

	ArrayList<String> things = new ArrayList<String>();
	ArrayList<ScrollingMessage> messages = new ArrayList<ScrollingMessage>();
	ArrayDeque<ScrollingMessage> waiting = new ArrayDeque<ScrollingMessage>();
	int lastPixel = 0;
	Lock lock = new ReentrantLock();
	ScrollingMessage lastMess;
	FontMetrics fm;

	public Ticker() {
		super(new Color(0, 160, 0), new Color(170, 150, 90), new Point(300, 200));
		// setOpaque(false);
		try {
			Scanner scanner = new Scanner(new java.io.File("headlines.txt"));
			while (scanner.hasNext()) {
				String s = scanner.nextLine();
				if (!s.contains("//"))
					;
				things.add(s);
			}
			scanner.close();
			things.add("Welcome to oliver's game");
			new Thread(this).start();

		} catch (java.io.IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void newString() {
		repaint();
		lock.lock();
		if (waiting.size() > 0)
			lastMess = waiting.poll();
		else
			lastMess = new ScrollingMessage(new String(things.get((int) (Math.random() * things.size()))), fm
					.stringWidth(new String(things.get((int) (Math.random() * things.size())))), getWidth());

		messages.add(lastMess);
		lock.unlock();
	}

	public void addMessage(String s, Color c) {
		ScrollingMessage v = new ScrollingMessage(s, fm.stringWidth(s), getWidth());
		v.c = c;
		waiting.offer(v);
	}

	public void paint(Graphics g) {
		super.paint(g);
		lock.lock();
		g.setFont(new Font("OLD ENGLISH TEXT MT", Font.PLAIN, 26));
		if (fm == null)
			fm = g.getFontMetrics();
		for (ScrollingMessage s : messages) {
			g.setColor(s.c);
			g.drawString(s.text, s.pixelAt, getHeight() - 10);
		}
		lock.unlock();
	}

	public void run() {
		ArrayList<ScrollingMessage> rem = new ArrayList<ScrollingMessage>();
		try {
			Thread.sleep(1000);
			while (true) {
				if (lastPixel < getWidth())
					newString();
				Thread.sleep(30);

				lock.lock();
				for (ScrollingMessage s : messages) {
					if (s.pixelAt - 4 + fm.stringWidth(s.text) < 0)
						rem.add(s);
					else
						s.pixelAt -= 2;
				}
				lock.unlock();

				try {
					lastPixel = lastMess.pixelAt + fm.stringWidth(lastMess.text) + 40;
				} catch (NullPointerException e) {
					lastPixel = 0;
				}

				for (ScrollingMessage s : rem)
					messages.remove(s);
				repaint();
				rem.clear();
			}
		} catch (InterruptedException exc) {
		}
	}
}
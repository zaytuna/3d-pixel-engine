package util;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class TypeThisField extends JTextField implements FocusListener, ActionListener {
	private static final long serialVersionUID = -2344659081280789119L;
	String display;
	public String text = "";

	public TypeThisField(String to) {
		display = to;
		addFocusListener(this);
		addActionListener(this);
		setForeground(Color.LIGHT_GRAY);
		setText(display + ": " + text);
	}

	public void focusGained(FocusEvent evt) {
		setForeground(Color.BLACK);
		setText(text);
	}

	public void focusLost(FocusEvent evt) {
		text = getText();
		setForeground(Color.LIGHT_GRAY);
		setText(display + ": " + text);
	}

	public void actionPerformed(ActionEvent evt) {
		transferFocus();
	}

}
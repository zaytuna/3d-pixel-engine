package util;

import java.awt.Color;

public class ColorScheme {
	public final static int ALPHA = 100;
	public Color[] colors;
	public String[] text;

	public ColorScheme(String[] text, Color... colors) {
		this.text = text;
		this.colors = colors;
	}

	public Color getColorForType(int t) {
		return getColorForType(t, ALPHA);
	}

	public Color getColorForType(int t, int al) {
		Color c = colors[t % colors.length];
		return new Color(c.getRed(), c.getGreen(), c.getBlue(), al);
	}

	public int size() {
		return Math.min(colors.length, text.length);
	}
}
package util;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;

/**
 * Example code found
 * at:http://www.anyexample.com/programming/java/java_play_wav_sound_file.xml
 * and modified to my own uses: e.g. volume control, fading, reorganizing code,
 * etc.
 */
public class Sound extends Thread {

	private String filename;
	private Position curPosition;
	private final int EXTERNAL_BUFFER_SIZE = 12 * 4096; // 12Kb
	private int nBytesRead;
	private byte[] abData;
	private SourceDataLine auline = null;
	private AudioInputStream audioInputStream = null;
	double fadeDir = 0;
	double volume = .5D;
	double counter = 0;
	public static double LIMIT = 4;
	private boolean scale = true;
	int numLoops = 1;
	boolean mute = false;
	boolean suicide = false;

	enum Position {
		LEFT, RIGHT, NORMAL
	};

	public Sound(File wavfile) {
		this(wavfile.getAbsolutePath());
	}

	public Sound(String wavfile) {
		this(wavfile, Position.NORMAL);
	}

	public Sound(String wavfile, Position p) {
		filename = wavfile;
		curPosition = p;
		File soundFile = new File(filename);
		if (!soundFile.exists()) {
			System.err.println("Wave file not found: " + filename);
			return;
		}

		try {
			audioInputStream = AudioSystem.getAudioInputStream(soundFile);
		} catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
			return;
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}

		AudioFormat format = audioInputStream.getFormat();
		auline = null;
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);

		try {
			auline = (SourceDataLine) AudioSystem.getLine(info);
			auline.open(format);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			return;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (auline.isControlSupported(FloatControl.Type.PAN)) {
			FloatControl pan = (FloatControl) auline.getControl(FloatControl.Type.PAN);
			if (curPosition == Position.RIGHT)
				pan.setValue(1.0f);
			else if (curPosition == Position.LEFT)
				pan.setValue(-1.0f);
		}

		auline.start();
		nBytesRead = 0;
		abData = new byte[EXTERNAL_BUFFER_SIZE];
	}

	public void mute(boolean b) {
		if (auline.isControlSupported(BooleanControl.Type.MUTE)) {
			mute = true;
			BooleanControl mute = (BooleanControl) auline.getControl(BooleanControl.Type.MUTE);
			mute.setValue(b);
		}
	}

	public void setVolume(double value, boolean scaling) {
		this.scale = scaling;

		if (scale)
			counter = -Math.log(3 / (double) value - 1);
		else
			volume = value;
	}

	public void loop(int numTimes) {
		numLoops = numTimes;
	}

	public double getVolume() {
		return volume;
	}

	public void end() {
		suicide = true;
		auline.drain();
		auline.close();
	}

	public void setCounter(double d) {
		counter = d;
	}

	public void setFade(double dir) {
		scale = true;
		fadeDir = dir;
	}

	public void run() {
		int loop = 0;
		try {
			mainloop: while (loop++ < numLoops) {
				nBytesRead = 0;
				abData = new byte[EXTERNAL_BUFFER_SIZE];
				audioInputStream = AudioSystem.getAudioInputStream(new File(filename));
				auline.start();

				while (nBytesRead != -1) {
					if (auline.isControlSupported(FloatControl.Type.MASTER_GAIN) && !mute) {
						counter += fadeDir / (double) 10;

						if (counter > LIMIT)
							counter = LIMIT - .1;
						else if (counter < -LIMIT && !scale)
							end();

						FloatControl gainControl = (FloatControl) auline.getControl(FloatControl.Type.MASTER_GAIN);
						volume = (scale ? (double) 2 / (1 + Math.pow(Math.E, -counter)) : volume); // number
																									// between
																									// 0
																									// and
																									// 2
																									// (loudest)
						float dB = (float) (Math.log(volume) / Math.log(10.0) * 20.0);
						gainControl.setValue(dB);
					}
					nBytesRead = audioInputStream.read(abData, 0, abData.length);
					if (suicide)
						break mainloop;
					if (nBytesRead >= 0)
						auline.write(abData, 0, nBytesRead);
				}
			}
			auline.drain();
			auline.close();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			auline.drain();
			auline.close();
		}
	}

	public static void main(String args[]) {
	}
}
package util;

public class BooleanReference {
	private boolean x, y;

	public void setValue(boolean b) {
		this.x = b;
		this.y = b;
	}

	public void setValueX(boolean b) {
		this.x = b;
	}

	public void setValueY(boolean b) {
		this.y = b;
	}

	public void orX(boolean b) {
		x = b || x;
	}

	public void orY(boolean b) {
		y = b || y;
	}

	public void andX(boolean b) {
		x = b && x;
	}

	public void andY(boolean b) {
		y = b && y;
	}

	public boolean getX() {
		return x;
	}

	public boolean getY() {
		return y;
	}

	public boolean getBoolean() {
		return x || y;
	}
}
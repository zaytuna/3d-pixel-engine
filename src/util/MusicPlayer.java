package util;

import javax.sound.sampled.*;

import main.GameDriver;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.io.File;

public class MusicPlayer implements Runnable {
	public static final File[] files;
	java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
	Sound currentMusic = null;
	List<Sound> sounds = new ArrayList<Sound>();
	ReentrantLock lock = new ReentrantLock();
	boolean mute = false;

	static {
		File f = new File("Music/Game/");

		if (f.exists())
			files = f.listFiles();
		else
			files = new File[0];
	}

	public MusicPlayer() {
		this((int) (Math.random() * files.length));
	}

	public MusicPlayer(Sound startMusic) {
		if (startMusic != null) {
			currentMusic = startMusic;
			sounds.add(startMusic);
			startMusic.setFade(1);
			service.execute(startMusic);
			service.execute(this);
		}
	}

	public MusicPlayer(int index) {
		this(files.length == 0 ? null : new Sound(files[index % files.length]));
	}

	public static Sound getMenuSound() {
		File[] menu = new File("Music/Menu/").listFiles();

		if (menu != null)
			for (File f : menu)
				if (f.getName().endsWith(".wav"))
					return new Sound(f);

		return null;
	}

	public void setMusicTo(Sound s) {
		s.setVolume(.05, true);
		service.execute(s);
		s.setFade(2);
		for (Sound i : sounds)
			i.setFade(-3);
		currentMusic.setFade(-3);
		lock.lock();
		sounds.add(s);
		lock.unlock();

		this.currentMusic = s;
	}

	public void setMusicTo(int index) {
		setMusicTo(new Sound(files[index % files.length]));
	}

	public void playSoundEffect(String dir) {
		try {
			Clip c = AudioSystem.getClip();
			AudioInputStream au = AudioSystem.getAudioInputStream(new File(dir));
			c.open(au);
			c.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mute(boolean b) {
		if (mute == b)
			return;
		mute = b;

		lock.lock();
		for (Sound s : sounds) {
			s.mute(b);
		}
		lock.unlock();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (Exception e) {
			}
			lock.lock();
			for (int i = 0; i < sounds.size(); i++) {
				if (sounds.get(i).getVolume() < .05 && !mute && !GameDriver.paused) {
					sounds.get(i).end();
					sounds.remove(i);
					i--;
				}
			}
			lock.unlock();
		}
	}

	public void finish() {
		service.shutdown();
	}

	public static void main(String[] args) {
		MusicPlayer mp = new MusicPlayer();
		int counter = 0;

		while (true) {
			try {
				Thread.sleep(10000);
			} catch (Exception e) {
			}
			mp.setMusicTo(++counter);
		}
	}

}
package util;

import java.util.ArrayList;
import java.util.List;

public class Tree<T> {
	public TreeNode<T> root;

	public Tree() {
		setLevels(root, 0);
		validateParentIndicators(root);
	}

	public void setLevels(TreeNode<T> current, int level) {
		if (current == null)
			return;
		current.level = level;

		for (TreeNode<T> node : current.getChildren())
			setLevels(node, level + 1);
	}

	public void validateParentIndicators(TreeNode<T> current) {
		if (current == null)
			return;

		for (TreeNode<T> node : current.getChildren()) {
			node.parent = current;
			validateParentIndicators(node);
		}
	}

	public List<TreeNode<T>> list() {
		List<TreeNode<T>> list = new ArrayList<TreeNode<T>>();
		listing(root, list);
		return list;
	}

	private void listing(TreeNode<T> current, List<TreeNode<T>> list) {
		if (current == null)
			return;
		list.add(current);
		for (TreeNode<T> n : current.getChildren())
			listing(n, list);
	}
}
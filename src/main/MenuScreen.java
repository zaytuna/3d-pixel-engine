package main;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import util.Methods;
import util.MusicPlayer;
import util.Sound;
import creators.CreateMap;
import framework.Debug;
import framework.Terrain;

public class MenuScreen extends JPanel implements MouseListener, Runnable {
	private static final long serialVersionUID = -6610774406805437251L;
	public static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	public static final String[][] options = { { "Single Player", "Multi Player" },// Play
			{ "Map" },// Create
			{ "AI", "Networking", "Color Schemes", "Sound", "Miscellaneous" } // Options
	};
	public static final int[] dx = { 0, 0, 1, 2, 2, 2, 2, 2 };
	public static final int[] dy = { 0, 1, 0, 0, 1, 2, 3, 4 };
	public static final double[] angles = { -Math.PI / 6, -Math.PI / 3, Math.PI / 4, 13 * Math.PI / 12,
			7 * Math.PI / 6, 5 * Math.PI / 4, 4 * Math.PI / 3, 17 * Math.PI / 12 };
	public static final int[] posX = { 0, 0, 0, screen.width, screen.width, screen.width, screen.width, screen.width };
	public static final int[] posY = { 0, 0, screen.height, 0, 0, 0, 0, 0 };
	public static final int LENGTH = dx.length;
	public static final int ALPHA = 140;
	public static final Color[] colors = { new Color(140, 0, 0, ALPHA), new Color(100, 100, 0, ALPHA),
			new Color(0, 140, 0, ALPHA), new Color(0, 0, 140, ALPHA), new Color(0, 100, 100, ALPHA),
			new Color(100, 0, 100, ALPHA), new Color(138, 140, 34, ALPHA), new Color(50, 50, 80, ALPHA) };

	public int[][] places = { { -200, -200 }, { -200 }, { -200, -200, -200, -200, -200 } };
	public int[][] sliding = { { 0, 0 }, { 0 }, { 0, 0, 0, 0, 0 } };
	int phase = -1;
	int selected = -1;
	int selectedOption = -1;

	SinglePlayerPanel spp = new SinglePlayerPanel(this);
	MultiPlayerPanel mpp = new MultiPlayerPanel();
	CreateMap cmt = new CreateMap(GameDriver.schemes[0], screen.width, screen.height, this);

	Sound sound = MusicPlayer.getMenuSound();
	File[] pics = new File("Pictures For Intro").listFiles();
	Image[] pictures = new Image[pics.length];
	int current = 0;
	float fade = 0f;
	int fadeDir = 1;

	MenuScreen() {
		super(true);
		setBackground(Color.BLACK);
		addMouseListener(this);
		setLayout(null);

		add(spp);
		add(mpp);
		cmt.setBounds(0, 0, screen.width, screen.height);
		super.add(cmt);

		spp.setVisible(false);
		mpp.setVisible(false);
		cmt.setVisible(false);

		try {
			for (int i = 0; i < pics.length; i++)
				pictures[i] = Methods.scale(new ImageIcon(pics[i].getAbsolutePath()).getImage(), screen.width,
						screen.height);
		} catch (Exception e) {
			e.printStackTrace();
		}

		java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();

		if (sound != null) {
			sound.loop(100);
			service.execute(sound);
		}
		service.execute(this);
		service.shutdown();

	}

	public void setPhase(int p) {
		this.phase = p;
		spp.setVisible(p == 0);
		mpp.setVisible(p == 1);
		cmt.setVisible(p == 2);
		repaint();
	}

	public BufferedImage getImage() {
		try {
			if (spp.loadImage.getSelectedFile() != null)
				return javax.imageio.ImageIO.read(spp.loadImage.getSelectedFile());
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	public void add(JPanel p) {
		p.setBounds(100, 100, screen.width - 200, screen.height - 200);
		super.add(p);
	}

	public void paint(Graphics gg) {
		super.paint(gg);
		Graphics2D g = (Graphics2D) gg;
		if (phase == -1) {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, fade));
			g.drawImage(pictures[current], (getWidth() - pictures[current].getWidth(null)) / 2,
					(getHeight() - pictures[current].getHeight(null)) / 2, this);
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));

			g.setPaint(new GradientPaint(10, 10, selected == 0 ? Color.WHITE : new Color(180, 0, 0), 100, 100,
					Color.ORANGE));
			g.fillOval(-100, -100, 200, 200);
			g.setPaint(new GradientPaint(0, getHeight(), selected == 1 ? Color.WHITE : Color.MAGENTA, 100,
					getHeight() - 100, Color.CYAN));
			g.fillOval(-100, getHeight() - 100, 200, 200);
			g.setPaint(new GradientPaint(getWidth() - 100, 100, Color.YELLOW, getWidth(), 0,
					selected == 2 ? Color.WHITE : new Color(0, 140, 0)));
			g.fillOval(getWidth() - 100, -100, 200, 200);

			g.setColor(Color.BLACK);
			g.setFont(new Font("SERIF", Font.BOLD, 30));

			g.drawString("Play", 10, 47);

			g.rotate(Math.PI / 4);
			g.drawString("Create", (int) Methods.getRotateX(5, getHeight() - 63, -Math.PI / 4), (int) Methods
					.getRotateY(5, getHeight() - 63, -Math.PI / 4));
			g.drawString("Options", (int) Methods.getRotateX(getWidth() - 85, 16, -Math.PI / 4), (int) Methods
					.getRotateY(getWidth() - 85, 16, -Math.PI / 4));
			g.rotate(-Math.PI / 4);

			for (int i = 0; i < LENGTH; i++) {
				int a = ((places[dx[i]][dy[i]] + 200) * 200) / 350;
				g.translate(posX[i], posY[i]);
				g.rotate(-angles[i]);

				if (angles[i] > Math.PI / 2 && angles[i] < 3 * Math.PI / 2) {
					g.rotate(Math.PI);
					g.setColor(new Color(colors[i].getRed(), colors[i].getGreen(), colors[i].getBlue(), a));
					g.fillRect(-places[dx[i]][dy[i]] - 190, -12, 220, 35);
					g.setColor(selectedOption == i ? Color.BLACK : new Color(255, 255, 255, a));
					g.drawString(options[dx[i]][dy[i]], -places[dx[i]][dy[i]] - 170, 13);
					g.rotate(-Math.PI);
				} else {
					g.setColor(new Color(colors[i].getRed(), colors[i].getGreen(), colors[i].getBlue(), a));
					g.fillRect(places[dx[i]][dy[i]] - 10, -25, 220, 35);
					g.setColor(selectedOption == i ? Color.BLACK : new Color(255, 255, 255, a));
					g.drawString(options[dx[i]][dy[i]], places[dx[i]][dy[i]], 0);
				}
				g.rotate(angles[i]);
				g.translate(-posX[i], -posY[i]);
			}
		}
		g.setPaint(new GradientPaint(getWidth() - 100, getHeight() - 100, Color.PINK, getWidth() + 100,
				getHeight() + 100, selected == 3 ? Color.WHITE : Color.RED));
		g.fillOval(getWidth() - 100, getHeight() - 100, 200, 200);
		g.setColor(Color.BLACK);
		g.setFont(new Font("SERIF", Font.BOLD, 30));
		g.drawString("Exit", getWidth() - 60, getHeight() - 27);
	}

	public void waitTillDone() {
		while (phase != -2)
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "ERROR: " + e);
			}

		try {
			if (sound != null)
				sound.end();
		} catch (Exception e) {
			Debug.log(e);
		}

		return;
	}

	public void mousePressed(MouseEvent evt) {
		if (phase == -1) {
			if (evt.getX() * evt.getX() + evt.getY() * evt.getY() <= 10000) {
				selected = (selected == 0) ? -1 : 0;

				for (int i = 0; i < LENGTH; i++)
					sliding[dx[i]][dy[i]] = (selected == dx[i]) ? 1 : -1;

			} else if (evt.getX() * evt.getX() + (evt.getY() - getHeight()) * (evt.getY() - getHeight()) <= 10000) {
				selected = (selected == 1) ? -1 : 1;
				for (int i = 0; i < LENGTH; i++)
					sliding[dx[i]][dy[i]] = (selected == dx[i]) ? 1 : -1;
			} else if ((evt.getX() - getWidth()) * (evt.getX() - getWidth()) + evt.getY() * evt.getY() <= 10000) {
				selected = (selected == 2) ? -1 : 2;
				for (int i = 0; i < LENGTH; i++)
					sliding[dx[i]][dy[i]] = (selected == dx[i]) ? 1 : -1;
			}
		}
		if ((evt.getX() - getWidth()) * (evt.getX() - getWidth()) + (evt.getY() - getHeight())
				* (evt.getY() - getHeight()) <= 10000)
			selected = (selected == 3) ? -1 : 3;

		repaint();
	}

	public void mouseReleased(MouseEvent evt) {
		if ((phase == -1 || phase == 0 || phase == 1)
				&& (evt.getX() - getWidth()) * (evt.getX() - getWidth()) + (evt.getY() - getHeight())
						* (evt.getY() - getHeight()) <= 10000) {
			System.exit(0);
		}

		if (phase == -1)
			for (int i = 0; i < LENGTH; i++) {
				Rectangle r = new Rectangle(places[dx[i]][dy[i]] - 10, -25, 200, 35);
				Point p = new Point((int) Methods.getRotateX(evt.getX() - posX[i], evt.getY() - posY[i], angles[i]),
						(int) Methods.getRotateY(evt.getX() - posX[i], evt.getY() - posY[i], angles[i]));

				if (r.contains(p) && places[dx[i]][dy[i]] > 10 && selected == dx[i]) {
					selectedOption = i;

					new Thread(new Runnable() {
						public void run() {
							try {
								Thread.sleep(400);
							} catch (Exception e) {
							}
							setPhase(selectedOption);
							selectedOption = -1;
						}
					}).start();

					break;
				}
			}
	}

	public Terrain getTerrain() {
		return spp.terrain;
	}

	public Color getPlayerColor() {
		return spp.colorChooser.getColor();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
			}
			for (int i = 0; i < LENGTH; i++) {
				if (sliding[dx[i]][dy[i]] == 1 && places[dx[i]][dy[i]] < 150)
					places[dx[i]][dy[i]] += 10;
				else if (sliding[dx[i]][dy[i]] == -1 && places[dx[i]][dy[i]] > -200)
					places[dx[i]][dy[i]] -= 10;
			}
			if (fadeDir == 1) {
				if (fade < .997)
					fade += .003;
				else
					fadeDir = -1;
			} else if (fadeDir == -1) {
				if (fade > .003)
					fade -= .003;
				else {
					fade = 0;
					current = (current + 1) % pics.length;
					fadeDir = 1;
				}
			}

			repaint();
		}
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void mouseClicked(MouseEvent evt) {
	}
}

class SinglePlayerPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1588167622181567512L;
	public static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	String[] terrainOptNames = { "Smoothed Noise", "From file", "From image", "Fractal Terrain" };
	JPanel terrainOptions = new JPanel();
	JRadioButton[] buttons = new JRadioButton[terrainOptNames.length];
	JButton start = new JButton("Play");
	String[] fileNames = new File("Maps/").list();
	PictureList mapChooser = new PictureList("saved.jpg", fileNames);
	JLabel imageDisplay = new JLabel("", JLabel.CENTER);
	JFileChooser loadImage = new JFileChooser("C:\\Users\\Oliver\\Pictures\\");
	JColorChooser colorChooser = new JColorChooser();
	MenuScreen owner;
	JButton back = new JButton("Back");
	JLabel[] labels = new JLabel[] { new JLabel("Seed", JLabel.CENTER), new JLabel("Noise Size", JLabel.CENTER),
			new JLabel("Persistence", JLabel.CENTER), new JLabel("Octaves", JLabel.CENTER) };

	JLabel randomTerrain = new JLabel(new ImageIcon("Perlin_Noise.jpg"), JLabel.CENTER);// int
	// seed,
	// double
	// noiseSize,
	// double
	// persistence,
	// int
	// octaves
	ArrayList<RTPanel> mults = new ArrayList<RTPanel>();
	JButton addRTPanel = new JButton(new ImageIcon("plus.jpg"));
	JTextField xField = new JTextField();
	JTextField yField = new JTextField();
	JSlider smoothing = new JSlider(0, 100, 5);
	JLabel smoothL = new JLabel("Smoothed 5 times");

	// fractal vars
	String[] fParamNames = { "Fraction Water", "Roughness", "Detail" };
	JLabel[] fLabels = new JLabel[fParamNames.length];
	JTextField[] fFields = new JTextField[fParamNames.length];
	JLabel fractalPanel = new JLabel(new ImageIcon("Fractal.jpg"), JLabel.CENTER);

	Terrain terrain;

	SinglePlayerPanel(MenuScreen owner) {
		this.owner = owner;
		setLayout(null);

		ButtonGroup b = new ButtonGroup();
		setOpaque(false);
		terrainOptions.setLayout(new FlowLayout(FlowLayout.CENTER));
		fractalPanel.setLayout(null);

		for (int i = 0; i < buttons.length; i++) {
			buttons[i] = new JRadioButton(terrainOptNames[i]);
			b.add(buttons[i]);
			terrainOptions.add(buttons[i]);
			buttons[i].setForeground(Color.WHITE);
			buttons[i].setBackground(Color.BLACK);
			buttons[i].addActionListener(this);
		}
		for (int i = 0; i < 4; i++) {
			labels[i].setBounds(i * 100 + 100, 10, 90, 25);
			labels[i].setForeground(new Color(255, 204, 102));
			randomTerrain.add(labels[i]);
		}
		for (int i = 0; i < fParamNames.length; i++) {
			fLabels[i] = new JLabel(fParamNames[i], JLabel.CENTER);
			fFields[i] = new JTextField("4");
			fLabels[i].setBounds(50 + i * 100, 50, 90, 25);
			fLabels[i].setForeground(new Color(255, 204, 102));
			fractalPanel.add(fLabels[i]);
			fFields[i].setBounds(50 + i * 100, 90, 90, 25);
			fractalPanel.add(fFields[i]);
		}
		fFields[0].setText(".15");
		fFields[1].setText("15");
		fFields[2].setText("6");
		smoothL.setBounds(230, 330, 150, 25);
		randomTerrain.add(smoothL);
		buttons[0].setSelected(true);
		xField.setBounds(0, 20, 35, 20);
		yField.setBounds(40, 20, 35, 20);
		smoothing.setBounds(80, 330, 150, 25);
		smoothing.setOpaque(false);
		smoothing.addChangeListener(new javax.swing.event.ChangeListener() {
			public void stateChanged(javax.swing.event.ChangeEvent evt) {
				smoothL.setText("Smoothed " + smoothing.getValue() + " times");
			}
		});
		randomTerrain.add(smoothing);
		xField.setText("40");
		yField.setText("40");
		randomTerrain.add(xField);
		randomTerrain.add(yField);
		randomTerrain.setOpaque(false);
		addRTPanel.setBounds(280, 60, 40, 30);
		randomTerrain.add(addRTPanel);
		addRTPanel.addActionListener(this);

		imageDisplay.setBounds(0, 50, 250, 350);
		loadImage.setBounds(260, 50, 350, 350);
		colorChooser.setBounds(660, 50, 415, 350);
		add(colorChooser);
		add(loadImage);
		add(imageDisplay);

		colorChooser.setOpaque(false);

		loadImage.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("JPEG file", "jpg", "jpeg"));
		loadImage.setControlButtonsAreShown(true);
		loadImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					imageDisplay.setIcon(new ImageIcon(javax.imageio.ImageIO.read(loadImage.getSelectedFile())
							.getScaledInstance(230, 320, Image.SCALE_SMOOTH)));
				} catch (Exception e) {
				}
			}
		});

		loadImage.setBackground(new Color(230, 238, 238));
		loadImage.setOpaque(true);
		imageDisplay.setBorder(new TitledBorder(new LineBorder(Color.RED), "Preview",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("SERIF", Font.PLAIN, 18),
				new Color(240, 230, 200)));

		terrainOptions.setBounds(0, 0, 550, 30);
		add(terrainOptions);
		randomTerrain.setBounds(0, 50, 600, 350);
		fractalPanel.setBounds(0, 50, 600, 350);
		add(randomTerrain);
		add(fractalPanel);
		terrainOptions.setOpaque(false);
		mapChooser.setBounds(0, 50, 250, 350);
		add(mapChooser);
		start.setBounds(screen.width / 2 - 175, screen.height - 235, 150, 35);
		back.setBounds(screen.width - 350, 0, 150, 35);
		add(back);
		add(start);
		back.setFont(new Font("SERIF", Font.BOLD, 25));
		back.setBorder(new BevelBorder(BevelBorder.RAISED));
		back.setBackground(new Color(0, 0, 160));
		back.setForeground(Color.ORANGE);
		start.setFont(new Font("SERIF", Font.BOLD, 25));
		start.setBorder(new BevelBorder(BevelBorder.RAISED));
		start.setBackground(new Color(0, 0, 160));
		start.setForeground(Color.ORANGE);

		back.addActionListener(this);
		start.addActionListener(this);

		mapChooser.setVisible(false);
		loadImage.setVisible(false);
		imageDisplay.setVisible(false);
		fractalPanel.setVisible(false);
	}

	private class RTPanel extends JPanel {
		private static final long serialVersionUID = 3276550013745229386L;
		JSpinner[] spinners = new JSpinner[4];
		boolean isLast = false;

		RTPanel() {
			super(null);
			setOpaque(false);

			spinners[0] = new JSpinner(new SpinnerNumberModel((int) (Math.random() * 600) - 300, -300, 300, 1));
			spinners[1] = new JSpinner(new SpinnerNumberModel(20, 0, 100, .1));
			spinners[2] = new JSpinner(new SpinnerNumberModel(6, 0, 100, .1));
			spinners[3] = new JSpinner(new SpinnerNumberModel(5, 0, 20, 1));

			for (int i = 0; i < 4; i++) {
				spinners[i].setBounds(i * 100, 4, 90, 25);
				add(spinners[i]);
			}
			addMouseListener(new MouseAdapter() {
				public void mouseReleased(MouseEvent evt) {
					if (new Rectangle(getWidth() - 30, 7, 20, 16).contains(evt.getPoint())) {
						randomTerrain.remove(RTPanel.this);
						mults.remove(RTPanel.this);
						randomTerrain.revalidate();
						for (int i = 0; i < mults.size(); i++) {
							mults.get(i).setBounds(100, i * 80 + 60, 500, 75);
							mults.get(i).isLast = (i == mults.size() - 1);
							randomTerrain.add(mults.get(i));
							mults.get(i).repaint();
						}
						randomTerrain.repaint();
						addRTPanel.setBounds(addRTPanel.getX(), mults.size() * 80 + 60, addRTPanel.getWidth(),
								addRTPanel.getHeight());

					}

				}
			});

		}

		public void paintComponent(Graphics gg) {
			super.paintComponent(gg);

			Graphics2D g = (Graphics2D) gg;
			g.setStroke(new BasicStroke(6));
			mapChooser.setListData(new File("Maps/").list());

			if (!isLast) {
				g.setColor(Color.RED);
				g.drawLine(getWidth() / 2 - 10, getHeight() - 25, getWidth() / 2 + 10, getHeight() - 5);
				g.drawLine(getWidth() / 2 - 10, getHeight() - 5, getWidth() / 2 + 10, getHeight() - 25);
			}

			g.setStroke(new BasicStroke(16));
			g.setColor(Color.BLUE);
			g.drawLine(getWidth() - 30, 15, getWidth() - 10, 15);

			g.setStroke(new BasicStroke(1));
		}

		public Terrain getTerrain(int x, int y) {
			return Terrain.randomize(x, y, (Integer) spinners[0].getValue(), (Double) spinners[1].getValue(),
					(Double) spinners[2].getValue(), (Integer) spinners[3].getValue());
		}
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == back)
			owner.setPhase(-1);
		else if (evt.getSource() == addRTPanel) {
			mults.add(new RTPanel());
			for (int i = 0; i < mults.size(); i++) {
				mults.get(i).setBounds(100, i * 80 + 60, 500, 75);
				mults.get(i).isLast = (i == mults.size() - 1);
				randomTerrain.add(mults.get(i));
				mults.get(i).repaint();
			}
			randomTerrain.revalidate();
			randomTerrain.repaint();
			addRTPanel.setBounds(addRTPanel.getX(), mults.size() * 80 + 60, addRTPanel.getWidth(), addRTPanel
					.getHeight());
		} else if (evt.getSource() == start) {
			try {
				if (buttons[0].isSelected()) {
					int x = Integer.parseInt(xField.getText());
					int y = Integer.parseInt(yField.getText());

					if (mults.size() == 0)
						terrain = Terrain.random(x, y, .15, smoothing.getValue());
					else {
						Terrain t = mults.get(0).getTerrain(x, y);
						for (int i = 1; i < mults.size(); i++)
							t.multiply(mults.get(i).getTerrain(x, y));

						t.scale(50);
						t.enforceMax(80);

						for (int i = 0; i < smoothing.getValue(); i++)
							t.smooth(2);

						t.tile(.15);
						terrain = t;
					}
				} else if (buttons[1].isSelected()) {
					terrain = Terrain.load(new File("Maps/" + (String) mapChooser.getSelectedValue()));
				} else if (buttons[2].isSelected()) {
					try {
						terrain = Terrain.terrainFromImage(javax.imageio.ImageIO.read(loadImage.getSelectedFile()),
								Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()));
						terrain.tile(0);
					} catch (java.io.IOException e) {
						e.printStackTrace();
					}
				} else if (buttons[3].isSelected()) {
					terrain = Terrain.fractal(Double.parseDouble(fFields[0].getText()), Double.parseDouble(fFields[1]
							.getText()), Integer.parseInt(fFields[2].getText()));
				} else
					return;
				owner.phase = -2;
			} catch (Exception e) {
				StackTraceElement[] s = e.getStackTrace();
				String string = "";
				for (int i = 0; i < s.length; i++) {
					string += "\n\tAt " + s[i].toString();
				}
				JOptionPane.showMessageDialog(this, "ERROR: " + e + "\n\n" + string, "", 0);
			}
		} else {
			randomTerrain.setVisible(buttons[0].isSelected());
			mapChooser.setVisible(buttons[1].isSelected());
			imageDisplay.setVisible(buttons[2].isSelected());
			loadImage.setVisible(buttons[2].isSelected());
			fractalPanel.setVisible(buttons[3].isSelected());
		}
	}

	private class PictureList extends JList<String> {
		private static final long serialVersionUID = -1590535043374240998L;
		Image img;

		PictureList(String name, String... data) {
			super(data);
			try {
				img = javax.imageio.ImageIO.read(new File(name));
			} catch (Exception e) {
				e.printStackTrace();
			}
			setSelectedIndex(0);
		}

		public void paint(Graphics g) {
			super.paint(g);

			Graphics2D g2d = (Graphics2D) g;
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .3f));
			g2d.drawImage(img, 0, 0, this);

		}
	}

}

class MultiPlayerPanel extends JPanel {
	private static final long serialVersionUID = -9026427378366361896L;

	MultiPlayerPanel() {
		setOpaque(false);
	}
}
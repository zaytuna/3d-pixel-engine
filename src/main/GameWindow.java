package main;

import game.Player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;

import util.Colorful;
import util.MusicPlayer;
import util.Ticker;

public class GameWindow extends JFrame implements ActionListener {
	private static final long serialVersionUID = 8804551243963680484L;
	public static String instructions = "Press:\n" + "P                     pause\n"
			+ "Numpad +        increase camera radius\n" + "Numpad -         the reverse\n"
			+ "Numpad 4        decrease FOV\n" + "Numpad 6        increase FOV\n"
			+ "C                    point clouds\n" + "W                   wireframe\n" + "G                    grid\n"
			+ "O                   tile-based texture\n" + "Shift+G           normal (no grid)\n"
			+ "F5-F8             change color schemes\n" + "Mouse            move\n"
			+ "Arrow keys      turn (Different in FPSCamera than LookAtCamera)\n"
			+ "Esc                Switch between the aforementioned cameras\n"
			+ "F1                  See data from the current camera";
	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	public Player owner;
	public JTextField txt = new JTextField(30);
	public GameDisplay display;
	public Ticker ticker;
	public MiniMap minimap;
	public JPanel content = new JPanel(null);

	// MENUS
	public JMenuBar bar;
	public MenuHandler menuHandler = new MenuHandler();

	JMenu file = new JMenu("File");
	String[] fileNames = { "Exit", "Save" };
	JMenuItem[] fileItems = new JMenuItem[fileNames.length];
	JMenu create = new JMenu("Create...");
	String[] createNames = { "Terrain", "Unit", "Race" };
	JMenu speed = new JMenu("Speed");
	String[] speedNames = { "Ticker Speed", "Game Speed", "Scroll speed" };
	JMenuItem[] speedItems = new JMenuItem[speedNames.length];
	JMenu misc = new JMenu("Miscalaneus options");
	String[] miscNames = { "Keyboard Movement", "Mouse Movement", "Draw Grid" };
	JCheckBoxMenuItem[] miscItems = new JCheckBoxMenuItem[miscNames.length];
	JMenu sound = new JMenu("Sound");
	JMenu music = new JMenu("Music");
	JRadioButtonMenuItem[] mItems = new JRadioButtonMenuItem[MusicPlayer.files.length];
	JMenu instMenu;
	public MenuScreen menuScreen;

	GameWindow(MenuScreen m) {
		this.menuScreen = m;

		content.setBackground(Color.BLACK);
		m.setLocation(0, 0);
		m.setSize(screen);

		content.add(m);
		add(content, BorderLayout.CENTER);

		// INSTANTANE THE FRAME
		setLocation(0, 0);
		setUndecorated(true);
		addWindowListener(new WindowHandler());
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setSize(screen);
		setVisible(true);
	}

	public void start(Player p, GameDisplay o) {
		content.remove(menuScreen);
		content.revalidate();
		content.repaint();

		this.owner = p;
		display = o;

		ticker = new Ticker();
		minimap = new MiniMap(GameDriver.ME, 200, 200);

		// SET UP
		bar = new JMenuBar();
		//setJMenuBar(bar);
		bar.setBackground(new Color(80, 0, 0));
		txt.addActionListener(this);
		bar.setBorder(new BevelBorder(BevelBorder.RAISED));
		// Menus
		instantaneMenu(fileNames, fileItems, file, bar);
		instantaneMenu(speedNames, speedItems, speed, bar);
		instantaneMenu(miscNames, miscItems, misc, bar);
		ButtonGroup bg = new ButtonGroup();

		for (int i = 0; i < mItems.length; i++) {
			mItems[i] = new JRadioButtonMenuItem(MusicPlayer.files[i].getName());
			mItems[i].addActionListener(menuHandler);
			mItems[i].setMnemonic(MusicPlayer.files[i].getName().charAt(0));
			bg.add(mItems[i]);
			music.add(mItems[i]);
			sound.setForeground(Color.CYAN);
		}

		bar.add(sound);
		sound.add(music);
		misc.add(txt);

		// Set up instruction panel
		JPanel instPanel = new Colorful(Color.RED, Color.BLACK, new Point(40, 40), new Point(30, 30));
		instPanel.setLayout(null);
		JTextArea text = new JTextArea();
		text.setOpaque(false);
		text.setBounds(5, 5, 360, 320);
		text.setFont(new Font("SERIF", Font.PLAIN, 14));
		text.setText(instructions);
		text.setForeground(new Color(255, 204, 51));
		instPanel.add(text);
		instMenu = new JMenu("Keystrokes");
		instMenu.setForeground(Color.CYAN);
		bar.add(instMenu);
		instPanel.setPreferredSize(new Dimension(370, 330));
		instMenu.add(instPanel);

		// Set Bounds
		ticker.setBounds(220, screen.height - 65, screen.width - 230, 40);
		display.setBounds(0, 0, screen.width, screen.height - 65);
		display.selectionArea.setBounds(0, 0, 158, screen.height - 250);
		minimap.setBounds(10, screen.height - 240, 200, 200);

		ticker.setBorder(new EtchedBorder(Color.RED, Color.ORANGE));
		display.mm = minimap;

		// Add Components

		content.add(display);
		content.add(display.selectionArea);
		content.add(ticker);
		content.add(minimap);

		content.setComponentZOrder(minimap, 0);
		content.setComponentZOrder(display.selectionArea, 1);
		content.setComponentZOrder(ticker, 2);
		content.setComponentZOrder(display, 3);

		content.revalidate();
		content.repaint();
	}

	public void instantaneMenu(String[] names, JMenuItem[] items, JMenu menu, JMenuBar bar) {
		for (int i = 0; i < names.length; i++) {
			if (items instanceof JCheckBoxMenuItem[])
				items[i] = new JCheckBoxMenuItem(names[i]);
			else
				items[i] = new JMenuItem(names[i]);
			items[i].addActionListener(menuHandler);
			items[i].setMnemonic(names[i].charAt(0));
			menu.add(items[i]);
			menu.setForeground(Color.CYAN);
		}
		bar.add(menu);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == txt) {
			ticker.addMessage(txt.getText(), Color.RED);
		}
	}

	private class WindowHandler extends WindowAdapter implements WindowListener {
		public void windowClosing(WindowEvent evt) {
			// ASK IF CLOSING IS A GOOD IDEA
			if (JOptionPane.showConfirmDialog(GameWindow.this, "Do you wish to exit?", "Exiting...",
					JOptionPane.YES_NO_OPTION) == 0)
				System.exit(0);
		}
	}

	private class MenuHandler implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if (evt.getSource() == fileItems[0])
				System.exit(0);
			else if (evt.getSource() == miscItems[2])
				display.mode = 0;
			for (int i = 0; i < mItems.length; i++) {
				if (evt.getSource() == mItems[i])
					GameDriver.mp.setMusicTo(i);
			}
		}
	}
}
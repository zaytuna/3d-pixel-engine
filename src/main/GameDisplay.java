package main;

import framework.DrawingPoint;
import framework.Face;
import framework.LookAtCamera;
import framework.MapObject;
import framework.MapTile;
import framework.Point3D;
import framework.Terrain;
import framework.Texture;
import framework.Triangle2D;
import game.Player;
import game.SelectionArea;
import game.Unit;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;

import util.BooleanReference;
import util.ColorScheme;
import util.Methods;

public class GameDisplay extends JPanel implements Runnable {
	private static final long serialVersionUID = 2178295715220818442L;
	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	public static final int TEXTURE = 0;
	public static final int GRID = 1;
	public static final int WIRE_FRAME = 2;
	public static final int POINT_CLOUD = 3;

	public static double AMBIENT_LIGHT = .1;

	public int tolerence = 0;
	SelectionArea selectionArea;
	Player owner;
	ArrayList<MapObject> inOrder = new ArrayList<MapObject>();
	Terrain terrain;
	ColorScheme colorType;
	MemoryImageSource source;
	Image view;
	int mode = 0; // 0 = Gouraud, 1 = Gouraud+Grid, 2 = Wireframe, 3 =
	// Pointclouds, 4 = Tile-texture, 5 = Texture, 6 = Height-based coloring, 7
	// = Hardware render
	int[] pixels;
	final int WIDTH, HEIGHT;
	long startTime;
	MiniMap mm = null;
	public boolean shouldDisplayInfo = false;
	int fps = 0;
	double alpha = .7;
	boolean dirLights = true;
	Point mouse, screenPos;
	Set<MapObject> toAdd = new HashSet<MapObject>();
	boolean shouldAdd = false;
	boolean shadows = true;
	boolean clock = true;

	Face mouseOverFace;

	BufferedImage img, hardwareRender;

	// **
	private int counter = 0;

	// Updating Vars
	ArrayList<Triangle2D> tris = new ArrayList<Triangle2D>();
	BooleanReference blocked = new BooleanReference();

	/**
	 *@param dispFor
	 *        : The player who 'owns' this diplay
	 *@param t
	 *        : The terrain; can be null if this is part of the main game
	 *@param blah
	 *        : The color scheme of this display
	 *@param isMainGame
	 *        : true if this class will be integrated with the rest of the game;
	 *        false otherwise
	 *@param actualWidth
	 *        : Actual Width of this GameDisplay
	 *@param actualWidth
	 *        : Actual Height of this GameDisplay
	 */
	GameDisplay(Player dispFor, Terrain t, ColorScheme blah, boolean isMainGame, int actualWidth, int actualHeight) {
		super(true);

		if (isMainGame) {
			for (Unit u : GameDriver.allUnits)
				inOrder.add(u);

			terrain = GameDriver.terrain;
		}

		setBackground(Color.BLACK);
		MouseHandler asfd = new MouseHandler();
		addMouseListener(asfd);
		addMouseMotionListener(asfd);
		owner = dispFor;
		selectionArea = new SelectionArea(owner);

		this.terrain = t;
		this.colorType = blah;

		WIDTH = actualWidth;
		HEIGHT = actualHeight;
		pixels = new int[WIDTH * HEIGHT];
		hardwareRender = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);

		for (int i = 0; i < terrain.patches.length; i++)
			inOrder.add(terrain.getPatch(i));

		source = new MemoryImageSource(WIDTH, HEIGHT, pixels, 0, WIDTH);
		source.setAnimated(true);
		source.setFullBufferUpdates(true);
		view = createImage(source);

		java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
		service.execute(this);
		service.shutdown();
	}

	public void addToWorld(MapObject u) {
		toAdd.add(u);
		shouldAdd = true;
	}

	public void update() {
		Arrays.fill(pixels, 0);
		mouse = Methods.m();
		try {
			screenPos = getLocationOnScreen();
		} catch (Exception e) {
		}
		blocked.setValue(true);

		if (counter++ % 10 == 0) {
			counter %= 10;
			fps = 10000 / (int) ((System.currentTimeMillis() - startTime));
			startTime = System.currentTimeMillis();
		}

		if (mode == 3) {
			for (MapObject m : inOrder)
				for (DrawingPoint p : m.getMesh()) {
					int x = owner.camera.getScreenX(p, blocked, WIDTH);
					int y = owner.camera.getScreenY(p, blocked, HEIGHT);

					if (!blocked.getBoolean()) {
						if (dirLights) {
							int r = Math.min((int) (300 / p.distanceFrom(owner.camera)), 30);

							for (int i = x - r; i <= x + r; i++)
								for (int j = y - r; j <= y + r; j++)
									if (i < WIDTH && j < HEIGHT) {
										/*
										 * if(p instanceof MapVertex)
										 * try{pixels[i+j*WIDTH] =
										 * shade(p.getRGB
										 * (colorType),getPercent((
										 * (MapVertex)p).normal.
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * 
										 * angleBetween(GameDriver.sunDir),shadowed
										 * (p,1)));
										 * }catch(IndexOutOfBoundsException e){}
										 * else
										 */
										try {
											pixels[i + j * WIDTH] = p.getRGB(colorType);
										} catch (IndexOutOfBoundsException e) {
										}
									}
						} else {
							if (x < WIDTH && y < HEIGHT)
								try {
									pixels[x + y * WIDTH] = Color.GREEN.getRGB();
								} catch (IndexOutOfBoundsException e) {
								}
						}
					}
					blocked.setValue(true);
				}

			owner.camera.applyChanges();
			source.newPixels(0, 0, WIDTH, HEIGHT);
			repaint();
		} else {
			tris.clear();

			Collections.sort(inOrder, owner.camera.getMOSorter());

			for (MapObject mo : inOrder)
				update(mo);

			owner.camera.applyChanges();

			render(tris);
		}
		if (shouldAdd) {
			for (MapObject u : toAdd)
				inOrder.add(u);
			shouldAdd = false;
		}
	}

	public boolean shadowed(Point3D p, int step) {
		if (shadows) {
			double dx = GameDriver.sunDir.x * step;
			double dy = GameDriver.sunDir.y * step;

			for (int i = 0; true; i++) {
				try {
					if (!(dx * i + p.x < terrain.getWidth() && dx * i + p.x > 0 && dy * i + p.y < terrain.getHeight() && dy
							* i + p.y > 0))
						break;

					if (terrain.getHeightAt((int) (dx * i + p.x), (int) (dy * i + p.y)) > p.z + GameDriver.sunDir.z * i
							* step
							&& terrain.getHeightAt((int) (dx * i + p.x) + 1, (int) (dy * i + p.y)) > p.z
									+ GameDriver.sunDir.z * i * step
							&& terrain.getHeightAt((int) (dx * i + p.x), (int) (dy * i + p.y) + 1) > p.z
									+ GameDriver.sunDir.z * i * step)
						return true;
				} catch (IndexOutOfBoundsException e) {
					break;
				}
			}
		}

		return false;
	}

	public void update(MapObject asdf) {
		asdf.getMesh().sortF(owner.camera);
		for (Face f : asdf.getMesh().getFaces()) {
			if (mode == 4) {
				double percent = dirLights ? getPercent(f.getNormal().angleBetween(GameDriver.sunDir), shadowed(f
						.getP1().getLocation(), 1)) : 1;

				Triangle2D t = new Triangle2D(owner.camera.getScreenX(f.getP1().getLocation(), blocked, WIDTH),
						owner.camera.getScreenY(f.getP1().getLocation(), blocked, HEIGHT), owner.camera.getScreenX(f
								.getP2().getLocation(), blocked, WIDTH), owner.camera.getScreenY(f.getP2()
								.getLocation(), blocked, HEIGHT), owner.camera.getScreenX(f.getP3().getLocation(),
								blocked, WIDTH), owner.camera.getScreenY(f.getP3().getLocation(), blocked, HEIGHT), f,
						shade(f.getRGB(colorType), percent), 0, 0);
				if (!blocked.getBoolean())
					tris.add(t);
			} else if (mode == 5) {
				double percent = dirLights ? getPercent(f.getNormal().angleBetween(GameDriver.sunDir), shadowed(f
						.getP1().getLocation(), 1)) : 1;

				Triangle2D t = new Triangle2D(owner.camera.getScreenX(f.getP1().getLocation(), blocked, WIDTH),
						owner.camera.getScreenY(f.getP1().getLocation(), blocked, HEIGHT), owner.camera.getScreenX(f
								.getP2().getLocation(), blocked, WIDTH), owner.camera.getScreenY(f.getP2()
								.getLocation(), blocked, HEIGHT), owner.camera.getScreenX(f.getP3().getLocation(),
								blocked, WIDTH), owner.camera.getScreenY(f.getP3().getLocation(), blocked, HEIGHT), f,
						shade(f.getRGB(colorType), percent), 0, 0);
				if (!blocked.getBoolean()) {
					tris.add(t);
					if (f instanceof MapTile) {
						t.u1 = ((MapTile) f).m1.u;
						t.v1 = ((MapTile) f).m1.v;
						t.u2 = ((MapTile) f).m2.u;
						t.v2 = ((MapTile) f).m2.v;
						t.u3 = ((MapTile) f).m3.u;
						t.v3 = ((MapTile) f).m3.v;
					}
				}
			} else {
				double percent1 = 1;
				double percent2 = 1;
				double percent3 = 1;

				if (f instanceof MapTile) {
					MapTile t = (MapTile) f;
					percent1 = dirLights ? getPercent(t.m1.normal.angleBetween(GameDriver.sunDir), shadowed(t.m1, 1))
							: 1;
					percent2 = dirLights ? getPercent(t.m2.normal.angleBetween(GameDriver.sunDir), shadowed(t.m2, 1))
							: 1;
					percent3 = dirLights ? getPercent(t.m3.normal.angleBetween(GameDriver.sunDir), shadowed(t.m3, 1))
							: 1;
				}

				Triangle2D t = new Triangle2D(owner.camera.getScreenX(f.getP1().getLocation(), blocked, WIDTH),
						owner.camera.getScreenY(f.getP1().getLocation(), blocked, HEIGHT), owner.camera.getScreenX(f
								.getP2().getLocation(), blocked, WIDTH), owner.camera.getScreenY(f.getP2()
								.getLocation(), blocked, HEIGHT), owner.camera.getScreenX(f.getP3().getLocation(),
								blocked, WIDTH), owner.camera.getScreenY(f.getP3().getLocation(), blocked, HEIGHT), f,
						/* owner.va.shadeFogOfWar( */shade(mode == 6 ? getRGB(f.getP1().getLocation().z) : f.getP1()
								.getRGB(colorType), percent1),// owner.va.getPercentShade(f.getP1().getLocation())),
						/* owner.va.shadeFogOfWar( */shade(mode == 6 ? getRGB(f.getP2().getLocation().z) : f.getP2()
								.getRGB(colorType), percent2),// owner.va.getPercentShade(f.getP2().getLocation())),
						/* owner.va.shadeFogOfWar( */shade(mode == 6 ? getRGB(f.getP3().getLocation().z) : f.getP3()
								.getRGB(colorType), percent3));// ,owner.va.getPercentShade(f.getP3().getLocation())));
				if (!blocked.getBoolean())
					tris.add(t);

			}
			blocked.setValue(true);
		}
	}

	public static int getRGB(double z) {
		return new Color(Math.abs((int) (Terrain.pseudoRandom((int) z) * 255)), Math.abs((int) (Terrain
				.pseudoRandom((int) z - 1) * 255)), Math.abs((int) (Terrain.pseudoRandom((int) z + 1) * 255))).getRGB();
	}

	public static int shade(int rgb, double percent) {
		// (alpha << 24)|(red << 16)|(green << 8)|blue;
		int r = (rgb >> 16) & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = (rgb >> 0) & 0xff;
		return (int) ((255 << 24) | ((int) (r * percent) << 16) | ((int) (g * percent) << 8) | (int) (b * percent));
	}

	public static double getPercent(double angle, boolean b) {
		return (angle >= Math.PI / 2 || b) ? AMBIENT_LIGHT : Math.cos(angle) * .9 + .1;
	}

	public void render(java.util.List<Triangle2D> list) {
		if (mode == 7) {
			Graphics g = hardwareRender.getGraphics();
			//g.setColor(Color.BLACK);
			//g.fillRect(0, 0, WIDTH, HEIGHT);
			for (int i = 0; i < list.size(); i++) {
				Triangle2D t = list.get(i);
				g.setColor(new Color(t.rgb1));
				g.fillPolygon(new int[] { t.x1, t.x2, t.x3 }, new int[] { t.y1, t.y2, t.y3 }, 3);
			}
			g.dispose();
		} else {
			for (int i = 0; i < list.size(); i++) {
				Triangle2D t = list.get(i);
				if (mode == 0 || mode == 1 || mode == 6) {
					try {
						/*
						 * boolean b = false;
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * 
						 * if(Methods.colorsAreClose(tolerence,t.rgb1,t.rgb2,t.rgb3
						 * ))
						 * b =
						 * drawTriangle(t.x1,t.y1,t.x2,t.y2,t.x3,t.y3,t.rgb1);
						 * else
						 */
						boolean b = drawGouraudTriangle(t.x1, t.y1, t.x2, t.y2, t.x3, t.y3, t.rgb1, t.rgb2, t.rgb3);
						if (b)
							drawTriangle(t.x1, t.y1, t.x2, t.y2, t.x3, t.y3, Color.BLUE.getRGB());

						mouseOverFace = t.rep;
					} catch (NullPointerException e) {
					}
				} else if (mode == 2) {
					drawLine(t.x1, t.y1, t.x2, t.y2);
					drawLine(t.x3, t.y3, t.x2, t.y2);
					drawLine(t.x1, t.y1, t.x3, t.y3);
				} else if (mode == 4) {
					try {
						boolean b = drawTriangle(t.x1, t.y1, t.x2, t.y2, t.x3, t.y3, t.rgb1);
						if (b)
							drawTriangle(t.x1, t.y1, t.x2, t.y2, t.x3, t.y3, Color.BLUE.getRGB());

						mouseOverFace = t.rep;
					} catch (NullPointerException e) {
					}
				} else if (mode == 5) {
					drawTexturedTriangle(t.x1, t.y1, t.x2, t.y2, t.x3, t.y3, t.u1, t.v1, t.u2, t.v2, t.u3, t.v3,
							this.img);
				}

			}
			source.newPixels(0, 0, WIDTH, HEIGHT);
		}
		repaint();
	}

	public void drawPolygon(Polygon p) {
		for (int i = 0; i < p.npoints; i++)
			drawLine(p.xpoints[i], p.ypoints[i], p.xpoints[(i + 1) % p.npoints], p.ypoints[(i + 1) % p.npoints]);
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(1);
			} catch (Exception e) {
				System.out.println("ERROR: Insomnia at line 139!");
			}

			update();
		}
	}

	public void fillPolygon(Polygon p, int rgb) {
		Rectangle bounds = p.getBounds();
		int[] crossing = new int[p.npoints];
		int last = 0;
		int index = 0;

		for (int j = bounds.y; j < bounds.height + bounds.y; j++) {
			index = 0;
			Arrays.fill(crossing, Integer.MAX_VALUE);
			last = p.npoints - 1;
			for (int i = 0; i < p.npoints; i++) {
				if (p.ypoints[i] < j && p.ypoints[last] >= j || p.ypoints[last] < j && p.ypoints[i] >= j) {
					crossing[index++] = (int) ((double) (j - p.ypoints[last])
							* (double) ((p.xpoints[i] - p.xpoints[last]) / (double) (p.ypoints[i] - p.ypoints[last])) + p.xpoints[last]);
				}

				last = i;
			}
			Methods.insertionSort(crossing, crossing.length);

			for (int i = 0; i < index; i += 2) {
				for (int z = crossing[i]; z <= crossing[i + 1]; z++) {
					/*
					 * int alpha = 255;
					 * int red = (int)(Math.random()*255);
					 * int blue = (int)(Math.random()*255);
					 * int green = (int)(Math.random()*255);
					 */
					try {
						pixels[j * WIDTH + z] = GameDriver.paused ? -8355712 : rgb;
					} catch (Exception e) {
					}// (alpha << 24)|(red << 16)|(green << 8)|blue;
				}
			}
		}
		if (mode == 1)
			for (int i = 0; i < p.npoints; i++) {
				drawLine(p.xpoints[i], p.ypoints[i], p.xpoints[(i + 1) % p.npoints], p.ypoints[(i + 1) % p.npoints]);
			}
	}

	public boolean drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int rgb) {
		boolean bool = false;
		// y1<y2<y3 (Sorted Points)
		if (y1 <= y2 && y2 <= y3)
			;
		else {
			if (y1 <= y3 && y3 <= y2)
				bool = drawTriangle(x1, y1, x3, y3, x2, y2, rgb);
			else if (y2 <= y1 && y1 <= y3)
				bool = drawTriangle(x2, y2, x1, y1, x3, y3, rgb);
			else if (y2 <= y3 && y3 <= y1)
				bool = drawTriangle(x2, y2, x3, y3, x1, y1, rgb);
			else if (y3 <= y1 && y1 <= y2)
				bool = drawTriangle(x3, y3, x1, y1, x2, y2, rgb);
			else if (y3 <= y2 && y2 <= y1)
				bool = drawTriangle(x3, y3, x2, y2, x1, y1, rgb);

			return bool;
		}

		for (int y = y1; y < y2; y++) {
			int a = ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;

			for (int x = Math.min(b, a); x <= Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					if (x == mouse.x - screenPos.x && y == mouse.y - screenPos.y)
						bool = true;
					try {
						pixels[y * WIDTH + x] = GameDriver.paused ? -8355712 : rgb;
					} catch (IndexOutOfBoundsException e) {
					}
				}
		}
		for (int y = y2; y < y3; y++) {
			int a = ((y - y2) * (x3 - x2)) / (y3 - y2) + x2;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;

			for (int x = Math.min(b, a); x <= Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					if (x == mouse.x - screenPos.x && y == mouse.y - screenPos.y)
						bool = true;
					try {
						pixels[y * WIDTH + x] = GameDriver.paused ? -8355712 : rgb;
					} catch (IndexOutOfBoundsException e) {
					}
				}
		}
		if (mode == 1 || GameDriver.paused) {
			drawLine(x1, y1, x2, y2, 0);
			drawLine(x3, y3, x2, y2, 0);
			drawLine(x1, y1, x3, y3, 0);
		}
		return bool;
	}

	public boolean drawGouraudTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int rgb1, int rgb2, int rgb3) {
		boolean bool = false;
		// y1<y2<y3 (Sorted Points)
		if (!(y1 <= y2 && y2 <= y3)) {
			if (y1 <= y3 && y3 <= y2)
				return drawGouraudTriangle(x1, y1, x3, y3, x2, y2, rgb1, rgb3, rgb2);
			else if (y2 <= y1 && y1 <= y3)
				return drawGouraudTriangle(x2, y2, x1, y1, x3, y3, rgb2, rgb1, rgb3);
			else if (y2 <= y3 && y3 <= y1)
				return drawGouraudTriangle(x2, y2, x3, y3, x1, y1, rgb2, rgb3, rgb1);
			else if (y3 <= y1 && y1 <= y2)
				return drawGouraudTriangle(x3, y3, x1, y1, x2, y2, rgb3, rgb1, rgb2);
			else if (y3 <= y2 && y2 <= y1)
				return drawGouraudTriangle(x3, y3, x2, y2, x1, y1, rgb3, rgb2, rgb1);
		}

		for (int y = y1; y < y2; y++) {
			int a = ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			int rgba = interpolateRGB(rgb1, rgb2, (y - y1) / (double) (y2 - y1));
			int rgbb = interpolateRGB(rgb1, rgb3, (y - y1) / (double) (y3 - y1));

			for (int x = Math.min(b, a); x < Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					if (x == mouse.x - screenPos.x && y == mouse.y - screenPos.y)
						bool = true;
					int rgb = b > a ? interpolateRGB(rgba, rgbb, (x - a) / (double) (b - a)) : interpolateRGB(rgbb,
							rgba, (x - b) / (double) (a - b));
					try {
						pixels[y * WIDTH + x] = GameDriver.paused ? -8355712 : rgb;
					} catch (IndexOutOfBoundsException e) {
					}
				}
		}
		for (int y = y2; y < y3; y++) {
			int a = ((y - y2) * (x3 - x2)) / (y3 - y2) + x2;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			int rgba = interpolateRGB(rgb2, rgb3, (y - y2) / (double) (y3 - y2));
			int rgbb = interpolateRGB(rgb1, rgb3, (y - y1) / (double) (y3 - y1));

			for (int x = Math.min(b, a); x < Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					if (x == mouse.x - screenPos.x && y == mouse.y - screenPos.y)
						bool = true;
					int rgb = b > a ? interpolateRGB(rgba, rgbb, (x - a) / (double) (b - a)) : interpolateRGB(rgbb,
							rgba, (x - b) / (double) (a - b));
					try {
						pixels[y * WIDTH + x] = GameDriver.paused ? -8355712 : rgb;
					} catch (IndexOutOfBoundsException e) {
					}
				}
		}
		if (mode == 1 || GameDriver.paused) {
			drawLine(x1, y1, x2, y2, 0);
			drawLine(x3, y3, x2, y2, 0);
			drawLine(x1, y1, x3, y3, 0);
		}
		return bool;
	}

	public boolean drawTexturedTriangle(int x1, int y1, int x2, int y2, int x3, int y3, Texture t) {
		return drawTexturedTriangle(x1, y1, x2, y2, x3, y3, t.u1, t.v1, t.u2, t.v2, t.u3, t.v3, t.img);
	}

	public boolean drawTexturedTriangle(int x1, int y1, int x2, int y2, int x3, int y3, double u1, double v1,
			double u2, double v2, double u3, double v3, BufferedImage img) {
		boolean bool = false;
		// y1<y2<y3 (Sorted Points)
		if (!(y1 <= y2 && y2 <= y3)) {
			if (y1 <= y3 && y3 <= y2)
				bool = drawTexturedTriangle(x1, y1, x3, y3, x2, y2, u1, v1, u3, v3, u2, v2, img);
			else if (y2 <= y1 && y1 <= y3)
				bool = drawTexturedTriangle(x2, y2, x1, y1, x3, y3, u2, v2, u1, v1, u3, v3, img);
			else if (y2 <= y3 && y3 <= y1)
				bool = drawTexturedTriangle(x2, y2, x3, y3, x1, y1, u2, v2, u3, v3, u1, v1, img);
			else if (y3 <= y1 && y1 <= y2)
				bool = drawTexturedTriangle(x3, y3, x1, y1, x2, y2, u3, v3, u1, v1, u2, v2, img);
			else if (y3 <= y2 && y2 <= y1)
				bool = drawTexturedTriangle(x3, y3, x2, y2, x1, y1, u3, v3, u2, v2, u1, v1, img);

			return bool;
		}

		for (int y = y1; y < y2; y++) {
			int a = ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			double ua = (u2 - u1) * (y - y1) / (double) (y2 - y1) + u1;
			double va = (v2 - v1) * (y - y1) / (double) (y2 - y1) + v1;
			double ub = (u3 - u1) * (y - y1) / (double) (y3 - y1) + u1;
			double vb = (v3 - v1) * (y - y1) / (double) (y3 - y1) + v1;

			for (int x = Math.min(b, a); x < Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					try {
						if (x == mouse.x - screenPos.x && y == mouse.y - screenPos.y)
							bool = true;
					} catch (NullPointerException npe) {
					}
					double u = b > a ? (ub - ua) * (x - a) / (double) (b - a) + ua : (ua - ub) * (x - b)
							/ (double) (a - b) + ub;
					double v = b > a ? (vb - va) * (x - a) / (double) (b - a) + va : (va - vb) * (x - b)
							/ (double) (a - b) + vb;
					try {
						pixels[y * WIDTH + x] = GameDriver.paused ? -8355712 : img.getRGB((int) (img.getWidth() * u),
								(int) (img.getHeight() * v));
					} catch (IndexOutOfBoundsException e) {
					}
				}
		}
		for (int y = y2; y < y3; y++) {
			int a = ((y - y2) * (x3 - x2)) / (y3 - y2) + x2;
			int b = ((y - y1) * (x3 - x1)) / (y3 - y1) + x1;
			double ua = (u3 - u2) * (y - y2) / (double) (y3 - y2) + u2;
			double va = (v3 - v2) * (y - y2) / (double) (y3 - y2) + v2;
			double ub = (u3 - u1) * (y - y1) / (double) (y3 - y1) + u1;
			double vb = (v3 - v1) * (y - y1) / (double) (y3 - y1) + v1;

			for (int x = Math.min(b, a); x <= Math.max(b, a); x++)
				if (x > 0 && x < WIDTH && y < HEIGHT && y > 0) {
					try {
						if (x == mouse.x - screenPos.x && y == mouse.y - screenPos.y)
							bool = true;
					} catch (NullPointerException npe) {
					}
					double u = b > a ? (ub - ua) * (x - a) / (double) (b - a) + ua : (ua - ub) * (x - b)
							/ (double) (a - b) + ub;
					double v = b > a ? (vb - va) * (x - a) / (double) (b - a) + va : (va - vb) * (x - b)
							/ (double) (a - b) + vb;
					try {
						pixels[y * WIDTH + x] = GameDriver.paused ? -8355712 : img.getRGB((int) (img.getWidth() * u),
								(int) (img.getHeight() * v));
					} catch (IndexOutOfBoundsException e) {
					}
				}
		}
		if (mode == 1 || GameDriver.paused) {
			drawLine(x1, y1, x2, y2);
			drawLine(x3, y3, x2, y2);
			drawLine(x1, y1, x3, y3);
		}
		return bool;
	}

	public static int interpolateRGB(int rgb1, int rgb2, double percent) {
		// int a1 = (rgb1 >> 24) & 0xff;
		int r1 = (rgb1 >> 16) & 0xff;
		int g1 = (rgb1 >> 8) & 0xff;
		int b1 = (rgb1 >> 0) & 0xff;

		// int a2 = (rgb2 >> 24) & 0xff;
		int r2 = (rgb2 >> 16) & 0xff;
		int g2 = (rgb2 >> 8) & 0xff;
		int b2 = (rgb2 >> 0) & 0xff;

		return (/* ((int)(a1+(a2-a1)*percent)<<16) */(255 << 24) | ((int) (r1 + (r2 - r1) * percent) << 16)
				| ((int) (g1 + (g2 - g1) * percent) << 8) | (int) (b1 + (b2 - b1) * percent));
	}

	public void drawLine(int x1, int y1, int x2, int y2, int rgb) {
		int dy = y2 - y1;
		int dx = x2 - x1;
		float t = 0.5F; // offset for rounding

		if (x1 < WIDTH && y1 < HEIGHT)
			try {
				pixels[y1 * WIDTH + x1] = rgb;
			} catch (Exception e) {
			}

		if (Math.abs(dx) > Math.abs(dy)) // slope < 1
		{
			float m = (float) dy / (float) dx; // compute slope
			t += y1;
			dx = (dx < 0) ? -1 : 1;
			m *= dx;
			while (x1 != x2) {
				x1 += dx; // step to next x value
				t += m; // add slope to y value

				if (x1 < WIDTH && t < HEIGHT)
					try {
						pixels[(int) t * WIDTH + x1] = rgb;
					} catch (Exception e) {
					}
			}
		} else// slope >= 1
		{
			float m = (float) dx / (float) dy; // compute slope
			t += x1;
			dy = (dy < 0) ? -1 : 1;
			m *= dy;
			while (y1 != y2) {
				y1 += dy; // step to next y value
				t += m; // add slope to x value

				if (t < WIDTH && y1 < HEIGHT)
					try {
						pixels[y1 * WIDTH + (int) t] = rgb;
					} catch (Exception e) {
					}
			}
		}
	}

	public void drawLine(int x1, int y1, int x2, int y2) {
		drawLine(x1, y1, x2, y2, (200 << 24) | (255 << 16) | (255 << 8) | 255);
	}

	public void setMode(boolean b) {
		mode = b ? 1 : 0;
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(mode == 7 ? hardwareRender : view, 0, 0, null);
		g.setColor(Color.WHITE);
		g.drawString(fps + " Frames Per Second", getWidth() - 150, 30);

		if (GameDriver.isKeyPressed("F1") || shouldDisplayInfo) {
			g.drawString("Alpha: " + (int) Math.toDegrees(owner.camera.getThetaXY()), getWidth() - 100, 55);
			g.drawString("Beta: " + (int) Math.toDegrees(owner.camera.getThetaXYZ()), getWidth() - 100, 80);
			g.drawString("Position: " + owner.camera.toString(), getWidth() - 180, 115);
			g.drawString("FOV: " + (int) Math.toDegrees(owner.camera.getFOVH()) + ","
					+ (int) Math.toDegrees(owner.camera.getFOVV()), getWidth() - 100, 140);

			if (owner.camera instanceof LookAtCamera) {
				LookAtCamera l = (LookAtCamera) owner.camera;
				g.drawString("Focus: " + l.focus.toString(), getWidth() - 180, 170);

				try {
					Point p = l.locationOf(l.focus, WIDTH, HEIGHT);
					g.fillOval(p.x - 5, p.y - 5, 10, 10);
				} catch (Exception e) {
				}
			}
		}
		if (GameDriver.isKeyPressed("F3"))
			g.drawString("Tolerence of " + tolerence + " RGB", getWidth() - 150, 200);

		mm.repaint();
	}

	public ArrayList<Unit> getSelectedUnits() {
		return null;
	}

	private class MouseHandler extends MouseAdapter implements MouseListener {
		public void mousePressed(MouseEvent evt) {
		}

		public void mouseReleased(MouseEvent evt) {
		}

		public void mouseDragged(MouseEvent evt) {
		}

		public void mouseClicked(MouseEvent evt) {
		}
	}

	public Color getTypeColor(int t) {
		// Mud, water, grass, sand,
		return colorType.getColorForType(t);
	}

	public void sort() {
		Collections.sort(inOrder, owner.camera.getMOSorter());
	}
}
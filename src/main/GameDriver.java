package main;

import framework.FPSCamera;
import framework.LookAtCamera;
import framework.Point2D;
import framework.Point3D;
import framework.Terrain;
import framework.UnseenException;
import framework.Vector3D;
import game.Player;
import game.Unit;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import util.ColorScheme;
import util.Methods;
import util.MusicPlayer;

public class GameDriver implements KeyListener, Runnable {
	public static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	public static final String[] RECOGNIZED_STATES = { "IDLE", "MOVE_TO", "ATTACK", "PATROL", "BUILD", "GATHER",
			"REPAIR", "STOP", "CAST_SPELL" };
	public static ArrayList<Unit> allUnits = new ArrayList<Unit>();
	public static Player ME;
	public ArrayList<Player> players = new ArrayList<Player>();
	static int index = 0;
	public static Set<String> down = new HashSet<String>();
	public static Terrain terrain;// new Terrain(70,70,0,10);
	public static ColorScheme[] schemes = {
			new ColorScheme(new String[] { "Water", "Sand", "Mud", "Grass", "Red Rock", "Dirt" },
					new Color(11, 38, 217), new Color(216, 193, 152), new Color(130, 85, 52), new Color(11, 139, 0),
					new Color(217, 126, 74), new Color(130, 85, 52)),
			new ColorScheme(new String[] { "Blue", "Blue", "Blue", "Blue", "Blue" }, new Color(98, 53, 191), new Color(
					72, 57, 127), new Color(60, 50, 140), new Color(113, 21, 219), new Color(127, 45, 143)) };
	public GameDisplay display;
	public GameWindow window;
	public static boolean paused = false;
	public static Vector3D sunDir = new Vector3D(1, 0, .5);
	public static MusicPlayer mp;
	public static final double GRAVITY = .05;
	java.util.concurrent.locks.ReentrantLock lock = new java.util.concurrent.locks.ReentrantLock();
	public float trans = .9f;

	GameDriver() {
		MenuScreen screen = new MenuScreen();

		window = new GameWindow(screen);

		screen.waitTillDone();
		terrain = screen.getTerrain();

		ME = new Player(screen.getPlayerColor(), new Point3D(-10, -10, 0));

		terrain.computeNormals();
		terrain.calculateUVs();

		display = new GameDisplay(ME, GameDriver.terrain, GameDriver.schemes[0], true, GameDriver.screen.width - 158,
				GameDriver.screen.height - 65);
		java.awt.image.BufferedImage img = screen.getImage();
		if (img != null)
			display.img = img;
		window.start(ME, display);

		players.add(ME);
		display.selectionArea.pane.addKeyListener(this);
		display.selectionArea.pause.addKeyListener(this);
		display.selectionArea.pause.requestFocus();
		display.selectionArea.pause.transferFocus();
		display.addKeyListener(this);

		mp = new MusicPlayer();

		java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
		service.execute(this);
		service.shutdown();

		KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().addKeyListener(this);

		/**
		 * TESTING FOG OF WAR
		 * SkinnedMesh mesh = new SkinnedMesh();
		 * mesh.add(new Bone(1,0,0,new Point3D(0,0,0),new Point3D(5,5,10)));
		 * mesh.add(new DrawingPoint(0,0,0));
		 * mesh.add(new DrawingPoint(0,1,0));
		 * mesh.add(new DrawingPoint(1,0,0));
		 * mesh.add(new DrawingPoint(1,1,0));
		 * mesh.add(new DrawingPoint(0,0,1));
		 * mesh.add(new DrawingPoint(0,1,1));
		 * mesh.add(new DrawingPoint(1,0,1));
		 * mesh.add(new DrawingPoint(1,1,1));
		 * for(int i = 0; i < mesh.size(); i++)
		 * mesh.get(i).setNeighbors(Methods.getArray(mesh,new
		 * DrawingPoint[mesh.size()]));
		 * mesh.calculateFaces();
		 * Unit u = new Unit(new StaticUnit(1,1,1,1,1,1,1,1,1,1),new
		 * Point(5,5));
		 * u.setRendition(mesh);
		 * ME.addUnit(u);
		 * display.addToWorld(u);
		 * //
		 */
		while (true) {
			// catches everything such that in an absence of music, we don't
			// die.
			try {
				Thread.sleep(60000);
				while (paused)
					try {
						Thread.sleep(100);
					} catch (Exception e) {
					}
				mp.setMusicTo(index++);
			} catch (Exception e) {
			}

		}
	}

	public void keyReleased(KeyEvent evt) {
		lock.lock();
		down.remove(KeyEvent.getKeyText(evt.getKeyCode()));
		lock.unlock();
	}

	public static boolean isKeyPressed(String key) {
		return down.contains(key);
	}

	public void keyTyped(KeyEvent evt) {
	}

	public void keyPressed(KeyEvent evt) {
		lock.lock();
		down.add(KeyEvent.getKeyText(evt.getKeyCode()));
		lock.unlock();
	}

	public static boolean componentContainsMouse(Component c) {
		return c.contains(Methods.m().x - c.getLocationOnScreen().x, Methods.m().y - c.getLocationOnScreen().y);
	}

	public void run() {
		int counter = 0;
		while (true) {
			// KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().addKeyListener(this);
			try {
				Thread.sleep(20);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!paused) {
				// if(isKeyPressed("M"))
				sunDir.incrementAngle(.01);
				if (!componentContainsMouse(display.mm)) {
					int dx = (Methods.m().x - screen.width / 2);
					int dy = (Methods.m().y - screen.height / 2);

					if (Math.abs(dx) > 100 || Math.abs(dy) > 100) {
						double chx = Math.pow(dx / (double) 400, 5);
						double chy = Math.pow(dy / (double) 300, 5);
						Point2D act = Methods.getRotatedPoint(new Point2D(chx, chy), ME.camera.getThetaXY());

						ME.camera.setY(act.y / 30 + ME.camera.buffer.y);
						ME.camera.setX(act.x / 30 + ME.camera.buffer.x);

						if (ME.camera instanceof FPSCamera) {
							try {
								ME.camera.velocity.z -= GRAVITY;
								ME.camera.setZ(Math.max(terrain.getHeightAt((int) ME.camera.x, (int) ME.camera.y) + 3,
										ME.camera.z + ME.camera.velocity.z));
								if (ME.camera.buffer.z == terrain.getHeightAt((int) ME.camera.x, (int) ME.camera.y) + 3)
									ME.camera.velocity.set(0, 0, 0);
							} catch (IndexOutOfBoundsException e) {
								ME.camera.z = Math.max(0, ME.camera.z - GRAVITY);
							}
						} else if (ME.camera instanceof LookAtCamera) {
							((LookAtCamera) ME.camera).updateFocus();

							/*
							 * ME.camera.velocity.z -= GRAVITY;
							 * 
							 * 
							 * 
							 * 
							 * 
							 * 
							 * try{((LookAtCamera)ME.camera).setFocusHeight(Math.
							 * max(terrain.getHeightAt(
							 * 
							 * 
							 * 
							 * 
							 * 
							 * (int)ME.camera.x,(int)ME.camera.y)+3,((LookAtCamera
							 * )ME.camera).focus.z+ME.camera.velocity.z));
							 * if(ME.camera.z==terrain.getHeightAt((int)((
							 * LookAtCamera)ME.camera).focus.x,
							 * (int)((LookAtCamera)ME.camera).focus.y)+3)
							 * ME.camera.velocity.set(0,0,0);
							 * }catch(IndexOutOfBoundsException e){}
							 * 
							 * ((LookAtCamera)ME.camera).updatePosition();
							 */
						}
					}

				}

				if (display.mm != null)
					display.mm.update();

				lock.lock();

				if (++counter % 1 == 0)
					for (String key : down) {
						if (key.equals("Left"))
							ME.camera.incrementHorizontalAngle(-.05);
						else if (key.equals("Right"))
							ME.camera.incrementHorizontalAngle(.05);
						else if (key.equals("Up"))
							ME.camera.incrementVerticalAngle(-.05);
						else if (key.equals("Down"))
							ME.camera.incrementVerticalAngle(.05);
						else if (key.equals("NumPad-6"))
							ME.camera.decreaseView();
						else if (key.equals("NumPad-4"))
							ME.camera.increaseView();
						else if (key.equals("Space")) {
							ME.camera.setZ(ME.camera.speed + ME.camera.buffer.z);
							if (ME.camera instanceof LookAtCamera)
								((LookAtCamera) ME.camera).updateFocus();
						} else if (key.equals("B")) {
							ME.camera.setZ(ME.camera.buffer.z - ME.camera.speed);
							if (ME.camera instanceof LookAtCamera)
								((LookAtCamera) ME.camera).updateFocus();
						} else if (key.equals("G") && down.contains("Shift"))
							display.mode = 0;
						else if (key.equals("G"))
							display.mode = 1;
						else if (key.equals("W"))
							display.mode = 2;
						else if (key.equals("C"))
							display.mode = 3;
						else if (key.equals("L")) {
							if (down.contains("Shift")) {
								terrain.setHeightAt((int) (Math.random() * terrain.getWidth()),
										(int) (Math.random() * terrain.getHeight()), Math.random() * 400 - 180);
							} else
								terrain.smooth(2);
							terrain.computeNormals();
						} else if (key.equals("F5"))
							display.colorType = GameDriver.schemes[0];
						else if (key.equals("F6"))
							display.colorType = GameDriver.schemes[1];
						else if (key.equals("NumPad -") && ME.camera instanceof LookAtCamera) {
							((LookAtCamera) ME.camera).changeRadius(1);
							((LookAtCamera) ME.camera).updatePosition();
						} else if (key.equals("NumPad +") && ME.camera instanceof LookAtCamera) {
							((LookAtCamera) ME.camera).changeRadius(-1);
							((LookAtCamera) ME.camera).updatePosition();
						} else if (key.equals("Escape") && ME.camera instanceof LookAtCamera)
							ME.setToFPSCamera();
						else if (key.equals("Escape") && ME.camera instanceof FPSCamera)
							ME.setToRTSCamera();
						else if (key.equals("O"))
							display.mode = 4;
						else if (key.equals("Z") && display.tolerence >= 1)
							display.tolerence--;
						else if (key.equals("X") && display.tolerence <= 254)
							display.tolerence++;
						else if (key.equals("T"))
							display.mode = 5;
						else if (key.equals("Q"))
							display.mode = 6;
						else if (key.equals("H"))
							display.mode = 7;

						else if (counter % 100 == 0) {
							if (key.equals("P")) {
								paused = !paused;
								mp.mute(paused);
							} else if (key.equals("F1") && down.contains("Ctrl"))
								display.shouldDisplayInfo = !display.shouldDisplayInfo;
							else if (key.equals("S"))
								display.dirLights = !display.dirLights;
							else if (key.equals("Enter")) {
								window.instMenu.doClick(1);
							} else if (key.equals("M"))
								display.shadows = !display.shadows;
						}
					}

				lock.unlock();

			}
		}
	}

	public static void main(String[] args) throws UnseenException {
		new GameDriver();
	}
}
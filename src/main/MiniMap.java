package main;

import framework.LookAtCamera;
import framework.Point2D;
import framework.Point3D;
import game.Player;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import util.Methods;

class MiniMap extends JPanel implements MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 2463615555405600321L;
	Player owner;
	double tileW, tileH;
	BufferedImage tImage;
	Point2D i1, i2, i3, i4;// Clockwise

	MiniMap(Player p, int width, int height) {
		super(true);
		owner = p;
		tileW = width / (double) GameDriver.terrain.getWidth();
		tileH = height / (double) GameDriver.terrain.getHeight();
		setBackground(new Color(171, 200, 226));
		setBorder(new BevelBorder(BevelBorder.RAISED));

		tImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = tImage.createGraphics();
		paintTerrain(g2d);
		g2d.dispose();

		addMouseListener(this);
		addMouseMotionListener(this);

		update();
	}

	public void update() {
		double y1 = owner.camera.z * Math.tan(owner.camera.getThetaXYZ() - owner.camera.getFOVH());
		double y2 = owner.camera.z * Math.tan(owner.camera.getThetaXYZ() + owner.camera.getFOVH());
		double x1 = y1 * Math.tan(owner.camera.getFOVH());
		double x2 = y2 * Math.tan(owner.camera.getFOVH());

		i1 = Methods.getRotatedPoint(x1, y1, owner.camera.getThetaXY());
		i2 = Methods.getRotatedPoint(-x1, y1, owner.camera.getThetaXY());
		i3 = Methods.getRotatedPoint(-x2, y2, owner.camera.getThetaXY());
		i4 = Methods.getRotatedPoint(x2, y2, owner.camera.getThetaXY());

		i1.translate(owner.camera.x, owner.camera.y);
		i2.translate(owner.camera.x, owner.camera.y);
		i3.translate(owner.camera.x, owner.camera.y);
		i4.translate(owner.camera.x, owner.camera.y);
	}

	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		/*
		 * ((Graphics2D)g).rotate(-owner.camera.getThetaXY());
		 * 
		 * ((Graphics2D)g).translate(getWidth()/2-Methods.getRotateX(getWidth()/2
		 * ,-getHeight()/2,owner.camera.getThetaXY()),
		 * -getHeight()/2-Methods.getRotateY(getWidth()/2,-getHeight()/2,owner.
		 * camera.getThetaXY()));
		 */

		super.paintComponent(g);

		g.drawImage(tImage, 0, 0, null);

		g.setColor(Color.RED);
		g.drawLine((int) (i1.x * tileW), (int) (i1.y * tileH), (int) (i2.x * tileW), (int) (i2.y * tileH));
		g.drawLine((int) (i2.x * tileW), (int) (i2.y * tileH), (int) (i3.x * tileW), (int) (i3.y * tileH));
		g.drawLine((int) (i3.x * tileW), (int) (i3.y * tileH), (int) (i4.x * tileW), (int) (i4.y * tileH));
		g.drawLine((int) (i4.x * tileW), (int) (i4.y * tileH), (int) (i1.x * tileW), (int) (i1.y * tileH));

		g.setColor(new Color(0, 160, 0));
		g.fillOval((int) (owner.camera.x * tileW) - 5, (int) (owner.camera.y * tileH) - 5, 10, 10);
		g.setColor(Color.BLACK);
		g.drawOval((int) (owner.camera.x * tileW) - 5, (int) (owner.camera.y * tileH) - 5, 10, 10);

		g.drawLine((int) (owner.camera.x * tileW), (int) (owner.camera.y * tileH), (int) (30 * Math.sin(owner.camera
				.getThetaXYZ()) * Math.cos(owner.camera.getThetaXY() - Math.PI / 2))
				+ (int) (owner.camera.x * tileW), (int) (30 * Math.sin(owner.camera.getThetaXYZ()) * Math
				.sin(owner.camera.getThetaXY() - Math.PI / 2))
				+ (int) (owner.camera.y * tileH));
	}

	public void paintTerrain(Graphics2D g) {
		g.setColor(new Color(171, 200, 226));
		g.fillRect(0, 0, 200, 200);
	}

	public void mousePressed(MouseEvent evt) {
		try {
			if (owner.camera instanceof LookAtCamera) {
				((LookAtCamera) owner.camera).setFocus(new Point3D(evt.getX() / tileW, evt.getY() / tileH,
						GameDriver.terrain.getHeightAt((int) (evt.getX() / tileW), (int) (evt.getY() / tileH))));
				((LookAtCamera) owner.camera).updatePosition();
			} else {
				owner.camera.set(evt.getX() / tileW, evt.getY() / tileH, GameDriver.terrain.getHeightAt((int) (evt
						.getX() / tileW), (int) (evt.getY() / tileH)));
			}
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public void mouseMoved(MouseEvent evt) {
	}

	public void mouseDragged(MouseEvent evt) {
		try {
			if (owner.camera instanceof LookAtCamera) {
				((LookAtCamera) owner.camera).setFocus(new Point3D(evt.getX() / tileW, evt.getY() / tileH,
						GameDriver.terrain.getHeightAt((int) (evt.getX() / tileW), (int) (evt.getY() / tileH))));
				((LookAtCamera) owner.camera).updatePosition();
			} else {
				owner.camera.set(evt.getX() / tileW, evt.getY() / tileH, GameDriver.terrain.getHeightAt((int) (evt
						.getX() / tileW), (int) (evt.getY() / tileH)));
			}
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public void mouseClicked(MouseEvent evt) {
	}

	public void mouseReleased(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void mouseEntered(MouseEvent evt) {
	}
}
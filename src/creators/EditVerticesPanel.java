package creators;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import util.ColorScheme;
import util.Colorful;
import framework.MapTile;
import framework.MapVertex;
import framework.Terrain;

class EditVerticesPanel extends Colorful implements ChangeListener, ActionListener {

	private static final long serialVersionUID = -6747314544525983863L;
	String[] heightNames = { "Sheer Drop", "Linear Drop", "Logistic Drop" };
	JRadioButton[] heightOpt = new JRadioButton[heightNames.length];
	TileTypeChooser type;
	ChooseHeightComponent height;
	JSpinner cost = new JSpinner(new SpinnerNumberModel(10, 1, 50, 1));
	JCheckBox walkable = new JCheckBox("Walkable");
	ArrayList<MapVertex> selectedVertices;
	ArrayList<MapTile> selectedTiles;
	JSlider slider = new JSlider(JSlider.VERTICAL, -100, 100, 10);
	Terrain terrain;
	CreateMap owner;

	EditVerticesPanel(CreateMap owner, ColorScheme cs, int theHeight, ArrayList<MapVertex> sv, ArrayList<MapTile> st,
			Terrain ter) {
		super(Color.MAGENTA, Color.CYAN);
		this.selectedTiles = st;
		this.selectedVertices = sv;
		this.terrain = ter;
		this.owner = owner;

		setBorder(new CompoundBorder(new LineBorder(Color.BLACK, 2), new SoftBevelBorder(BevelBorder.RAISED)));
		setLayout(null);
		ButtonGroup group = new ButtonGroup();
		setPreferredSize(new Dimension(460, 155));
		for (int i = 0; i < heightNames.length; i++) {
			heightOpt[i] = new JRadioButton(heightNames[i]);
			group.add(heightOpt[i]);
			heightOpt[i].setBounds(i * 110 + 20, 10, 100, 25);
			heightOpt[i].setOpaque(false);
			add(heightOpt[i]);
		}
		heightOpt[0].setSelected(true);
		type = new TileTypeChooser(cs);
		type.setBounds(20, 40, 370, 35);
		add(type);
		walkable.setOpaque(false);
		walkable.setBounds(310, 80, 80, 25);
		add(walkable);
		cost.setBounds(20, 80, 70, 25);
		add(cost);
		height = new ChooseHeightComponent(0, type);
		height.setBounds(397, 3, 60, 140);
		add(height);
		setSize(getPreferredSize());
		if (theHeight > 0)
			setLocation(-getWidth(), theHeight);

		height.addListener(this);
		type.addListener(this);
		walkable.addActionListener(this);
		cost.addChangeListener(this);
	}

	public void newSelected() {
		if (selectedVertices.size() > 0) {
			ArrayList<Double> list = new ArrayList<Double>();
			for (MapVertex v : selectedVertices)
				list.add(v.z);
			height.setHeight(mode(list));
		}
		ArrayList<Integer> list = new ArrayList<Integer>();
		if (selectedTiles.size() > 0) {
			for (MapVertex v : selectedVertices)
				list.add(v.type);

			type.setType(mode(list));
			list.clear();
		}
		if (selectedVertices.size() > 0) {
			for (MapVertex v : selectedVertices)
				list.add(v.walkable ? 1 : 0);

			walkable.setSelected(mode(list) == 1);
			list.clear();

			for (MapVertex v : selectedVertices)
				list.add(v.cost);

			cost.setValue(mode(list));
			list.clear();
		}
	}

	public static <T extends Number> double avg(java.util.Collection<T> asdf) {
		double tot = 0;
		for (T i : asdf)
			tot += i.doubleValue();

		return tot / asdf.size();
	}

	public static <T extends Number> T mode(java.util.Collection<T> asdf) {
		java.util.HashMap<T, Integer> map = new java.util.HashMap<T, Integer>();

		for (T value : asdf) {
			if (!map.keySet().contains(value))
				map.put(value, new Integer(1));
			else
				map.put(value, new Integer(map.get(value) + 1));
		}

		if (map.size() > 0) {
			int max = map.get(map.keySet().iterator().next());
			T maxKey = map.keySet().iterator().next();

			for (T i : map.keySet())
				if (map.get(i) > max) {
					max = map.get(i);
					maxKey = i;
				}

			return maxKey;
		}
		return null;
	}

	public void stateChanged(ChangeEvent evt) {
		try {
			if (evt.getSource() == height)
				for (MapVertex v : selectedVertices) {
					if (heightOpt[0].isSelected())
						v.z = height.getFinalHeight();
					else if (heightOpt[1].isSelected() && terrain != null)
						terrain.setLinearHeightAt(v.x, v.y, height.getFinalHeight(), slider.getValue() / (double) 10);
					else if (heightOpt[2].isSelected() && terrain != null)
						terrain.setLogisticHeightAt(v.x, v.y, height.getFinalHeight(), slider.getValue() / (double) 10);
				}
			else if (evt.getSource() == type) {
				for (MapVertex v : selectedVertices) {
					v.type = type.getType();
				}
			} else if (evt.getSource() == cost) {
				for (MapVertex v : selectedVertices)
					v.cost = (Integer) cost.getValue();
			}
			if (owner != null)
				owner.repaint();
		} catch (Exception e) {
			System.out.println(e + "\nTerrain is null? " + (terrain == null));
		}
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == walkable)
			for (MapVertex v : selectedVertices)
				v.walkable = walkable.isSelected();

		if (owner != null)
			owner.repaint();
	}
}
package creators;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import main.GameDisplay;
import util.Colorful;
import framework.DrawingPoint;
import framework.Face;
import framework.LookAtCamera;
import framework.Mesh;
import framework.Point3D;
import framework.SkinnedMesh;
import framework.UnseenException;

class RenditionCreationPanel extends Colorful implements ActionListener, MouseListener, MouseMotionListener, Runnable {
	private static final long serialVersionUID = 1374627568303170147L;
	Mesh mesh = new Mesh();
	SkinnedMesh sMesh;
	boolean isSkinned = false;
	int phase = 0;
	String[] phaseNames = { "1. Create Model", "2. Divide into Body Parts", "3. Texture", "4. Create Bones",
			"5. Determine Connections", "6. Animate" };

	JLabel top = new JLabel("", JLabel.CENTER);
	JPanel sidePanel = new Colorful(Color.BLUE, Color.WHITE, new Point(50, 50));
	Display display = new Display();

	// phase1 buttons
	JToggleButton addVertex = new JToggleButton("Create Vertices");
	JToggleButton connectVertices = new JToggleButton("Connect Vertices");
	JToggleButton weld = new JToggleButton("Weld Vertices");
	JToggleButton sketch = new JToggleButton("Sketch guidelines");

	JButton mirror = new JButton("Mirror Vertices");
	JButton smooth = new JButton("Smooth vertices");

	JButton undo = new JButton("Undo");
	JButton clear = new JButton("Clear");
	int mode = 0;

	ArrayList<Point> points = new ArrayList<Point>();

	RenditionCreationPanel(int w, int h, boolean b) {
		setProgress(0);
		isSkinned = b;

		sidePanel.setLayout(null);
		sidePanel.setPreferredSize(new Dimension(150, h));
		sidePanel.add(addVertex);
		sidePanel.add(connectVertices);
		sidePanel.add(weld);
		sidePanel.add(mirror);
		sidePanel.add(sketch);
		sidePanel.add(smooth);
		sidePanel.add(undo);
		sidePanel.add(clear);

		addVertex.setSelected(true);

		addVertex.setBounds(5, 5, 140, 25);
		connectVertices.setBounds(5, 35, 140, 25);
		weld.setBounds(5, 65, 140, 25);
		sketch.setBounds(5, 95, 140, 25);
		mirror.setBounds(5, 155, 140, 25);
		smooth.setBounds(5, 185, 140, 25);
		undo.setBounds(2, 600, 70, 25);
		clear.setBounds(72, 600, 75, 25);

		undo.addActionListener(this);
		clear.addActionListener(this);
		addVertex.addActionListener(this);
		connectVertices.addActionListener(this);
		weld.addActionListener(this);
		sketch.addActionListener(this);
		mirror.addActionListener(this);
		smooth.addActionListener(this);
		display.addMouseListener(this);
		display.addMouseMotionListener(this);

		setBackground(new Color(221, 255, 255));

		setLayout(new BorderLayout());

		add(top, BorderLayout.NORTH);
		add(sidePanel, BorderLayout.WEST);
		add(display, BorderLayout.CENTER);

		new Thread(this).start();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(50);
			} catch (Exception e) {
			}
			display.camera.incrementHorizontalAngle(.03);
			display.repaint();
		}
	}

	public void setProgress(int stage) {
		String tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		String pre = "<U><Font color=\"#000055\">";
		String post = "</FONT></U>";

		String text = "<HTML><HEAD></HEAD><BODY>";

		for (int i = 0; i < phaseNames.length; i++)
			text += (i == stage ? pre : "") + phaseNames[i] + (i == stage ? post + tab : tab);
		top.setText(text);
	}

	public void mousePressed(MouseEvent evt) {
		points.clear();
		if (mode == 3)
			points.add(evt.getPoint());
	}

	public void mouseReleased(MouseEvent evt) {
		if (mode == 3) {
			points.add(evt.getPoint());

			int[] x = new int[points.size()];
			int[] y = new int[points.size()];

			for (int i = 0; i < points.size(); i++) {
				x[i] = points.get(i).x;
				y[i] = points.get(i).y;
			}
			display.guidelines.add(new Polygon(x, y, points.size()));
		}
	}

	public void mouseClicked(MouseEvent evt) {
		if (mode == 0)
			mesh.add(new DrawingPoint(evt.getX() / (double) getWidth(), 0, evt.getY() / (double) getHeight()));
	}

	public void mouseDragged(MouseEvent evt) {
		if (mode == 3)
			points.add(evt.getPoint());
	}

	public void mouseMoved(MouseEvent evt) {
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public Mesh getRendition() {
		return mesh;
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == connectVertices) {
			connectVertices.setSelected(true);
			addVertex.setSelected(false);
			weld.setSelected(false);
			sketch.setSelected(false);
			mode = 1;
		} else if (evt.getSource() == addVertex) {
			connectVertices.setSelected(false);
			addVertex.setSelected(true);
			weld.setSelected(false);
			sketch.setSelected(false);
			mode = 0;
		} else if (evt.getSource() == weld) {
			connectVertices.setSelected(false);
			addVertex.setSelected(false);
			weld.setSelected(true);
			sketch.setSelected(false);
			mode = 2;
		} else if (evt.getSource() == sketch) {
			connectVertices.setSelected(false);
			addVertex.setSelected(false);
			weld.setSelected(false);
			sketch.setSelected(true);
			mode = 3;
		}
	}

	public SkinnedMesh getSkinnedRendition() {
		return sMesh;
	}

	private class Display extends JPanel {
		private static final long serialVersionUID = 2139604924853002163L;
		LookAtCamera camera = new LookAtCamera();
		ArrayList<Polygon> guidelines = new ArrayList<Polygon>();

		Display() {
			super(true);
			camera.setFocus(new Point3D(0, 0, 0));
			camera.setRadius(3.3);
			camera.updatePosition();
			camera.applyChanges();
		}

		public void paint(Graphics asdf) {
			super.paint(asdf);

			Graphics2D g = ((Graphics2D) asdf);
			g.setColor(Color.BLACK);

			for (int i = 0; i < points.size() - 1; i++) {
				g.drawLine(points.get(i).x, points.get(i).y, points.get((i + 1) % points.size()).x, points.get((i + 1)
						% points.size()).y);
			}
			for (Polygon p : guidelines)
				g.drawPolyline(p.xpoints, p.ypoints, p.npoints);

			for (Point3D p : mesh) {
				try {
					Point uv = camera.locationOf(p, getWidth(), getHeight());
					g.fillOval(uv.x - 2, uv.y - 2, 4, 4);
				} catch (UnseenException ue) {
				}
			}
			if (mesh.hasFaces())
				for (Face f : mesh.getFaces()) {
					try {
						Point p1 = camera.locationOf(f.getP1().getLocation(), getWidth(), getHeight());
						Point p2 = camera.locationOf(f.getP2().getLocation(), getWidth(), getHeight());
						Point p3 = camera.locationOf(f.getP3().getLocation(), getWidth(), getHeight());
						g.setColor(new Color(GameDisplay.getRGB(f.getP1().getLocation().z)));
						g.setStroke(new BasicStroke(2));

						g.drawLine(p1.x, p1.y, p2.x, p2.y);
						g.drawLine(p2.x, p2.y, p3.x, p3.y);
						g.drawLine(p1.x, p1.y, p3.x, p3.y);

					} catch (UnseenException ue) {
					}
				}
		}
	}
}
package creators;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import util.ColorScheme;

class TileTypeChooser extends JComponent implements MouseListener {
	private static final long serialVersionUID = -8774306489007954615L;
	ColorScheme cs;
	int choice = 0;
	java.util.Set<ChangeListener> listeners = new java.util.HashSet<ChangeListener>();

	TileTypeChooser(ColorScheme cs) {
		this.cs = cs;
		addMouseListener(this);
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);
		repaint();
	}

	public void addListener(ChangeListener cl) {
		listeners.add(cl);
	}

	public void paintComponent(Graphics g3) {
		super.paintComponent(g3);
		Graphics2D g = (Graphics2D) g3;

		// g.setColor(c[choice]);
		// g.fillRect(0,0,getWidth(),getHeight());

		for (int i = 0; i < cs.size(); i++) {
			g.setColor(cs.colors[i]);
			g.fillRect(getWidth() / cs.size() * i + 4, 4, getWidth() / cs.size() - 3, getHeight() - 16);
			g.drawString(cs.text[i], getWidth() / cs.size() * i + 4, getHeight() - 1);
		}
		g.setColor(Color.RED);
		g.setStroke(new BasicStroke(3));
		g.drawRect(getWidth() / cs.size() * choice + 4, 4, getWidth() / cs.size() - 3, getHeight() - 16);
	}

	public void setType(int c) {
		this.choice = c;
		repaint();
	}

	public int getType() {
		return choice;
	}

	public Color getColor(int alpha) {
		return cs.getColorForType(choice, 190);
	}

	public void mousePressed(MouseEvent evt) {
		for (int i = 0; i < cs.size(); i++)
			if (new Rectangle(getWidth() / cs.size() * i + 4, 4, getWidth() / cs.size() - 3, getHeight() - 16)
					.contains(evt.getPoint())) {
				choice = i;
				fireActionListeners();
				break;
			}

		repaint();
	}

	public void fireActionListeners() {
		for (ChangeListener cl : listeners)
			cl.stateChanged(new ChangeEvent(this));
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void mouseReleased(MouseEvent evt) {
	}

	public void mouseClicked(MouseEvent evt) {
	}
}
package creators;

import game.StaticUnit;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

class MeshCreator extends JFrame implements ActionListener, Runnable {
	private static final long serialVersionUID = -1433670057498812991L;
	// int a, int cool, int aRange, int sRange, int d, int h, int en, int sheild
	JButton finish = new JButton("Finish");
	JToggleButton view = new JToggleButton("View Drawing");

	boolean done = false;
	RenditionCreationPanel rcp = new RenditionCreationPanel(750, 850, true);
	int sliding = 0;
	ArrayList<String> states = new ArrayList<String>();

	JTextField[] fields = new JTextField[11];
	JLabel[] labels = new JLabel[11];
	String[] names = { "Attack", "Cool Down Time", "Attack Range", "Sight Range", "Defence", "Health", "Energy",
			"Sheilds", "Height", "Speed", "COST:" };
	StaticUnit unit = new StaticUnit(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

	MeshCreator() {
		setLayout(null);
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setUndecorated(true);
		centerFrame(1200, 800);
		rcp.setBounds(1200, 27, 750, 650);
		view.setBounds(1050, 2, 150, 25);
		finish.setBounds(550, 720, 100, 25);

		rcp.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		add(view);
		add(rcp);
		add(finish);

		for (int i = 0; i < fields.length; i++) {
			fields[i] = new JTextField();
			labels[i] = new JLabel(names[i], SwingConstants.RIGHT);
			labels[i].setBounds(20 + 10 * i, 20 + 30 * i, 100, 25);
			fields[i].setBounds(130 + 10 * i, 20 + 30 * i, 100, 25);
			add(fields[i]);
			add(labels[i]);
		}
		fields[10].setEnabled(false);
		fields[10].setBackground(Color.LIGHT_GRAY);

		// MotionListener m = new MotionListener(this);
		// addMouseListener(m);
		// addMouseMotionListener(m);

		view.addActionListener(this);
		finish.addActionListener(this);

		java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
		service.execute(this);
		service.shutdown();
	}

	public void paint(Graphics go) {
		super.paint(go);

		Graphics2D g = (Graphics2D) go;
		g.setStroke(new BasicStroke(5));
		g.setColor(Color.ORANGE);
		g.drawLine(0, 0, getWidth(), 0);
		g.drawLine(0, 0, 0, getHeight());
		g.drawLine(getWidth(), getHeight(), 0, getHeight());
		g.drawLine(getWidth(), getHeight(), getWidth(), 0);
	}

	public class MotionListener extends MouseAdapter implements MouseMotionListener, MouseListener {
		Component owner;
		Point pressedAt = null;

		public MotionListener(Component c) {
			owner = c;
		}

		public void mousePressed(MouseEvent evt) {
			pressedAt = evt.getPoint();
		}

		public void mouseReleased(MouseEvent evt) {
			owner.setBounds(owner.getX() - pressedAt.x + evt.getX(), owner.getY() - pressedAt.y + evt.getY(), owner
					.getWidth(), owner.getHeight());
		}

		public void mouseDragged(MouseEvent evt) {
			owner.setBounds(owner.getX() - pressedAt.x + evt.getX(), owner.getY() - pressedAt.y + evt.getY(), owner
					.getWidth(), owner.getHeight());
		}
	}

	public StaticUnit createUnit() {
		done = false;
		setVisible(true);

		while (!done) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException i) {
			}
		}
		done = false;
		setVisible(false);

		return unit;
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == finish) {
			int q = 0;
			try {
				unit.setRendition(rcp.getSkinnedRendition());
				unit.setPossibleStates(states);

				int[] ints = new int[9];

				for (; q < ints.length; q++) {
					ints[q] = Integer.parseInt(fields[q].getText());
					fields[q].setBorder(new javax.swing.border.LineBorder(Color.DARK_GRAY, 1));
					fields[q].setBorder(new javax.swing.border.LineBorder(Color.RED, 3));
				}

				unit.maxStats = unit.new MaxStats(ints[0], ints[1], ints[2], ints[3], ints[4], ints[5], ints[6],
						ints[7], ints[8], ints[9]);
				done = true;

			} catch (NumberFormatException nfe) {

				JOptionPane.showMessageDialog(this, "Please fill in all the fields with their respective"
						+ "\nquantities in the correct format...");
			}
		} else if (evt.getSource() == view) {
			if (view.isSelected())
				sliding = 1;
			else
				sliding = -1;
		}
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(30);
			} catch (Exception e) {
			}
			if (sliding == 1) {
				rcp.setLocation(rcp.getX() - 10, rcp.getY());
				if (rcp.getX() <= getWidth() - 780) {
					repaint();
					sliding = 0;
				}
			} else if (sliding == -1) {
				rcp.setLocation(rcp.getX() + 10, rcp.getY());
				if (rcp.getX() >= 1200) {
					repaint();
					sliding = 0;
				}
			}
		}
	}

	public void centerFrame(int frameWidth, int frameHeight) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth, frameHeight);
	}

	public static void main(String[] kjls) {
		MeshCreator uc = new MeshCreator();
		uc.createUnit();
	}
}
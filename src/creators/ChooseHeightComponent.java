package creators;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.IllegalComponentStateException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class ChooseHeightComponent extends JComponent implements Runnable, MouseListener {
	private static final long serialVersionUID = -9183234763243836L;
	double height;
	int bottomSpace = 0;
	final int PIXELS_PER_HEIGHT = 5;
	TileTypeChooser ttc;
	boolean pressed = false;
	java.util.Set<ChangeListener> listeners = new java.util.HashSet<ChangeListener>();

	ChooseHeightComponent(int height, TileTypeChooser ttc) {
		this.height = height;
		this.ttc = ttc;
		new Thread(this).start();
		addMouseListener(this);
		setBorder(new javax.swing.border.LineBorder(Color.GREEN, 2));
	}

	public void mousePressed(MouseEvent evt) {
		if (m().y - getLocationOnScreen().y < 20 || m().y - getLocationOnScreen().y > getHeight() - 20)
			pressed = true;
	}

	public void mouseReleased(MouseEvent evt) {
		pressed = false;
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void mouseClicked(MouseEvent evt) {
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void setBounds(int a, int b, int c, int d) {
		super.setBounds(a, b, c, d);
		repaint();
	}

	public double getFinalHeight() {
		return height;
	}

	public void setHeight(double h) {
		height = h;
		repaint();
	}

	public void addListener(ChangeListener cl) {
		listeners.add(cl);
	}

	public void paintComponent(Graphics g6) {
		// super.paintComponent(g);
		Graphics2D g = (Graphics2D) g6;
		g.setColor(new Color(238, 238, 238));
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(Color.BLACK);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// g.drawLine(getWidth()/2,0,getWidth()/2,getHeight());

		// g.drawLine(0,getHeight()-bottomSpace,getWidth(),getHeight()-bottomSpace);

		if (Math.abs(height) * PIXELS_PER_HEIGHT <= getHeight()) {
			g.drawLine(0, getHeight() - bottomSpace, getWidth() / 2, getHeight() - bottomSpace
					- (int) (height * PIXELS_PER_HEIGHT));
			g.drawLine(getWidth(), getHeight() - bottomSpace, getWidth() / 2, getHeight() - bottomSpace
					- (int) (height * PIXELS_PER_HEIGHT));

			g.setColor(ttc.getColor(1));

			g.fillPolygon(new int[] { 0, 0, getWidth() / 2, getWidth(), getWidth() }, new int[] { getHeight(),
					getHeight() - bottomSpace, getHeight() - bottomSpace - (int) (height * PIXELS_PER_HEIGHT),
					getHeight() - bottomSpace, getHeight() }, 5);
		} else {
			int temp = (int) (getWidth() / 2 - (getHeight() * getWidth() / (2 * Math.abs(height) * PIXELS_PER_HEIGHT)));

			g
					.drawPolygon(new int[] { 0, 0, temp, getWidth() / 2, getWidth() - temp, getWidth(), getWidth() },
							new int[] { getHeight(), Math.max(getHeight() - bottomSpace, 0),
									Math.max(getHeight() - bottomSpace, 0), height > 0 ? 0 : getHeight(),
									Math.max(getHeight() - bottomSpace, 0), Math.max(getHeight() - bottomSpace, 0),
									getHeight() }, 7);

			g.setColor(ttc.getColor(1));

			g
					.fillPolygon(new int[] { 0, 0, temp, getWidth() / 2, getWidth() - temp, getWidth(), getWidth() },
							new int[] { getHeight(), Math.max(getHeight() - bottomSpace, 0),
									Math.max(getHeight() - bottomSpace, 0), height > 0 ? 0 : getHeight(),
									Math.max(getHeight() - bottomSpace, 0), Math.max(getHeight() - bottomSpace, 0),
									getHeight() }, 7);
		}

		g.setColor(new Color(100, 0, 0, 100));
		g.fillRect(0, 0, getWidth(), 20);
		g.fillRect(0, getHeight() - 20, getWidth(), 20);
		g.setFont(new Font("SERIF", Font.BOLD, 20));
		g.setColor(new Color(0, 50, 50));
		g.drawString("" + height, getWidth() / 2 - 10, 50);
	}

	public static Point m() {
		return MouseInfo.getPointerInfo().getLocation();
	}

	public void run() {
		while (true) {
			try {
				// System.out.println(pressed);

				try {
					Thread.sleep(50);
				} catch (Exception e) {
				}
				if (new Rectangle(getLocationOnScreen().x, getLocationOnScreen().y, getWidth(), getHeight())
						.contains(m())
						&& pressed) {
					if (m().y - getLocationOnScreen().y < 20) {
						height++;
						for (ChangeListener cl : listeners)
							cl.stateChanged(new ChangeEvent(this));
					}
					if (m().y - getLocationOnScreen().y > getHeight() - 20) {
						height--;
						for (ChangeListener cl : listeners)
							cl.stateChanged(new ChangeEvent(this));
					}

				}
				// else
				{
					if (bottomSpace < -height * PIXELS_PER_HEIGHT)
						bottomSpace += 4;
					if (bottomSpace > -height * PIXELS_PER_HEIGHT && bottomSpace >= 4)
						bottomSpace -= 4;
				}
				repaint();
			} catch (IllegalComponentStateException asdf) {
			}
		}
	}
	/*
	 * public static void main(String args[])
	 * {
	 * JFrame frame = new JFrame("BLAH");
	 * frame.setBounds(50,50,400,400);
	 * frame.setLayout(null);
	 * ChooseHeightComponent chc = new ChooseHeightComponent(30, new
	 * Color(0,0,255,150));
	 * chc.setBounds(0,0,200,200);
	 * frame.add(chc);
	 * frame.setVisible(true);
	 * }
	 */
}
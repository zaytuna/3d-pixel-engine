package creators;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.MemoryImageSource;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import util.BooleanReference;
import util.ColorScheme;
import framework.AllignedPlane;
import framework.DrawingPoint;
import framework.LookAtCamera;
import framework.MapObject;
import framework.Point3D;

class DisplayMapObjectPanel extends JPanel implements Runnable, ActionListener {
	private static final long serialVersionUID = 8271337033674290307L;
	public final static int CLOCKWISE = 0;
	public final static int COUNTERCLOCKWISE = 1;

	final double increment = 0.005;

	int sliding = 0;

	double thetaR = 0;
	MapObject object;// initialized in constructor
	ColorScheme cs;// initialized in constructor
	Point3D point;// initialized in constructor
	Point3D center;// initialized in constructor
	AllignedPlane plane = AllignedPlane.XY;
	int direction = COUNTERCLOCKWISE;
	JCheckBox grid = new JCheckBox("Grid");
	JCheckBox move = new JCheckBox("Moving");
	JTextField rad = new JTextField(2);
	LookAtCamera camera = new LookAtCamera();
	BooleanReference blocked = new BooleanReference();
	JButton updateFocus = new JButton("Update Focus");

	MemoryImageSource source;
	int[] pixels;
	Image view;

	public final int WIDTH, HEIGHT;

	DisplayMapObjectPanel(ColorScheme asdf, int width, int height) {
		super(true);
		WIDTH = width;
		HEIGHT = height;
		pixels = new int[WIDTH * HEIGHT];

		this.cs = asdf;
		setBackground(Color.WHITE);
		setLayout(new BorderLayout());

		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.setOpaque(false);

		move.setOpaque(false);
		move.setForeground(new Color(0, 100, 100));
		grid.setOpaque(false);
		grid.setForeground(new Color(0, 160, 0));
		rad.addActionListener(this);

		south.add(move);
		south.add(grid);
		south.add(rad);
		south.add(updateFocus);

		updateFocus.addActionListener(this);

		add(south, BorderLayout.SOUTH);
		setBorder(new javax.swing.border.LineBorder(Color.GREEN, 4));

		source = new MemoryImageSource(WIDTH, HEIGHT, pixels, 0, WIDTH);
		source.setAnimated(true);
		source.setFullBufferUpdates(true);
		view = createImage(source);

		addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				if (evt.getX() - evt.getY() > 460)
					sliding = -1;
				else if (evt.getY() - evt.getX() > 460)
					sliding = 1;
			}
		});
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == updateFocus) {
			center = object.getCenter();
			camera.setFocus(center);
		} else if (evt.getSource() == rad) {
			camera.setRadius(Double.parseDouble(rad.getText()));
			camera.updatePosition();
		}
	}

	public void start(MapObject mo) {
		this.object = mo;
		rad.setText(String.valueOf((int) (object.getBoundingSphereRadius() + 10)));
		center = object.getCenter();

		camera.radius = (object.getBoundingSphereRadius() + 10);
		camera.setFocus(center);

		new Thread(this).start();
	}

	public void update() {
		java.util.Arrays.fill(pixels, 0);
		for (DrawingPoint p : object.getMesh()) {
			int x = camera.getScreenX(p, blocked, WIDTH);
			int y = camera.getScreenY(p, blocked, HEIGHT);

			if (!blocked.getBoolean())
				for (int i = x - 3; i < x + 3; i++)
					for (int j = y - 3; j < y + 3; j++)
						if (i < WIDTH && j < HEIGHT && i > 0 && j > 0)
							pixels[i + j * WIDTH] = new Color(255 - (Math.abs(i - x) * Math.abs(i - x) + Math
									.abs(j - y)
									* Math.abs(j - y)) * 255 / 18, 0, 0).getRGB();
		}
		source.newPixels(0, 0, WIDTH, HEIGHT);
		repaint();
	}

	public void paint(Graphics gg) {
		super.paint(gg);

		if (view != null) {
			Graphics2D g = (Graphics2D) gg;
			g.setColor(Color.BLACK);
			g.drawString(camera.toString() + "        " + (camera.getThetaXY() / Math.PI) + "*PI", 50, 25);

			g.drawImage(view, 0, 0, null);

			g.setColor(Color.ORANGE);
			g.fillPolygon(new int[] { getWidth() - 40, getWidth(), getWidth() }, new int[] { 0, 0, 40 }, 3);
			g.setColor(Color.BLACK);
			g.drawPolygon(new int[] { getWidth() - 40, getWidth(), getWidth() }, new int[] { 0, 0, 40 }, 3);
			g.drawLine(getWidth() - 15, 15, getWidth() - 2, 2);
			g.drawLine(getWidth() - 2, 2, getWidth() - 10, 4);
			g.drawLine(getWidth() - 2, 2, getWidth() - 4, 10);

		}
	}

	public void run() {
		int counter = 0;
		while (true) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}

			if (move.isSelected()) {
				camera.incrementHorizontalAngle(increment);
				camera.applyChanges();
			}
			if (move.isSelected() || ++counter % 40 == 0)
				update();

			if (sliding == 1 && getY() < 30)
				setLocation(getX() - 15, getY() + 15);
			else if (sliding == -1 && getY() + getHeight() > 40)
				setLocation(getX() + 15, getY() - 15);
		}
	}
}
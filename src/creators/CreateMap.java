package creators;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import main.MenuScreen;
import util.ColorScheme;
import util.Colorful;
import util.TypeThisField;
import framework.MapTile;
import framework.MapVertex;
import framework.Point3D;
import framework.Terrain;

public class CreateMap extends JPanel implements ActionListener, MouseListener, MouseMotionListener, KeyListener,
		Runnable {
	private static final long serialVersionUID = -1842115606848858195L;

	public Terrain ter;
	boolean done = false;
	ArrayList<MapVertex> selectedV = new ArrayList<MapVertex>();
	ArrayList<MapTile> selectedT = new ArrayList<MapTile>();
	HashSet<String> down = new HashSet<String>();

	TypeThisField[] params = new TypeThisField[4];
	String[] strings = { "Width of terrain", "Height of terrain", "Default Height", "Default Cost" };
	JButton recreate = new JButton("Recreate");
	JMenuItem save = new JMenuItem("Save");
	JMenuItem load = new JMenuItem("Load");
	JMenuItem exit = new JMenuItem("Exit");
	JPanel creation = new Colorful(Color.CYAN, Color.MAGENTA);
	EditVerticesPanel edit;
	int sliding = 0;
	boolean in = false;
	MapVertex nearest = null;
	int move = 1;
	double rotation = 0;
	ColorScheme cs;// Comupter Science, Competetive Soccer, Counter Strike,
	// Color Scheme
	Point begin, end;
	JMenuBar bar = new JMenuBar();

	JMenu creator = new JMenu("Create");
	JMenu file = new JMenu("File");
	MenuScreen owner;

	JButton smooth = new JButton("Smooth Terrain");
	JMenuItem random = new JMenuItem("Random Terrain", new ImageIcon("Random.jpg"));
	JButton tile = new JButton("Auto Tile");

	DisplayMapObjectPanel display;
	JButton back = new JButton("Back");

	public double x = 0, y = 0;
	public double pix;
	public int h = 0;

	public CreateMap(ColorScheme cs, int width, int height, MenuScreen owner) {
		super(true);

		bar.setBounds(0, 0, width, 25);
		bar.setBackground(Color.DARK_GRAY);
		bar.setBorder(null);
		add(bar);
		bar.add(file);
		bar.add(creator);

		this.cs = cs;
		this.owner = owner;
		h = height;
		setPreferredSize(new Dimension(width, 800));
		setLayout(null);
		creation.setLayout(null);

		file.add(save);
		file.add(load);
		file.add(exit);

		file.setForeground(Color.WHITE);
		creator.setForeground(Color.WHITE);

		exit.addActionListener(this);
		save.addActionListener(this);
		load.addActionListener(this);

		creation.setPreferredSize(new Dimension(640, 55));
		for (int i = 0; i < params.length; i++) {
			params[i] = new TypeThisField(strings[i]);
			params[i].setBounds(i * 150 + 10, 2, 140, 25);
			params[i].addKeyListener(this);
			creation.add(params[i]);
		}
		random.setFont(new Font("OLD ENGLISH TEXT MT", Font.PLAIN, 26));

		recreate.addKeyListener(this);
		recreate.addActionListener(this);
		recreate.setBounds(200, 29, 200, 25);
		creation.add(recreate);
		creator.add(creation);
		creator.add(random);
		random.addActionListener(this);
		smooth.addActionListener(this);
		tile.addActionListener(this);
		display = new DisplayMapObjectPanel(cs, 500, 500);
		display.setBounds(width - 40, -460, 500, 500);
		add(display);
		back.setBounds(width - 240, height - 40, 130, 35);
		add(back);
		back.setFont(new Font("SERIF", Font.BOLD, 25));
		back.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.RAISED));
		back.setBackground(new Color(0, 0, 160));
		back.setForeground(Color.ORANGE);
		smooth.setBounds(width - 140, 540, 130, 30);
		add(smooth);
		tile.setBounds(width - 140, 570, 130, 30);
		add(tile);

		back.addActionListener(this);

		GridPanel gp = new GridPanel();
		gp.setBounds(0, 0, width, height);
		gp.setBackground(Color.BLACK);
		add(gp);
		tile.setEnabled(false);
		smooth.setEnabled(false);

		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
		addMouseListener(owner);
	}

	public Terrain createTerrain() {
		done = false;

		while (!done) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException i) {
			}
		}
		done = false;

		return ter;
	}

	public void mouseDragged(MouseEvent evt) {
		end = evt.getPoint();
		repaint();
	}

	public void mousePressed(MouseEvent evt) {
		begin = evt.getPoint();
		repaint();
	}

	public void mouseReleased(MouseEvent evt) {
		if (end != null && begin != null && ter != null) {
			if (!down.contains("Shift")) {
				selectedV.clear();
				selectedT.clear();
			}
			Rectangle rect = new Rectangle(Math.min(begin.x, end.x), Math.min(begin.y, end.y), Math
					.abs(begin.x - end.x), Math.abs(begin.y - end.y));

			for (MapVertex v : ter)
				if (rect.contains(new Point((int) ((v.x - x) * pix), (int) ((v.y - y) * pix))))
					selectedV.add(v);

			for (MapTile t : ter.tiles())
				if (rectContains(rect, t.getP1().getLocation(), t.getP2().getLocation(), t.getP3().getLocation()))
					selectedT.add(t);

			edit.newSelected();

			if (!in)
				sliding = 1;

			begin = null;
			end = null;
			repaint();
		}
	}

	public boolean rectContains(Rectangle rect, Point3D... points) {
		for (Point3D p : points)
			if (!rect.contains(new Point((int) ((p.x - x) * pix), (int) ((p.y - y) * pix))))
				return false;
		return true;
	}

	public void mouseClicked(MouseEvent evt) {
		if (ter != null) {
			if (nearest != null) {
				if (down.contains("Ctrl")) {
					if (!down.contains("Shift"))
						selectedV.clear();

					for (MapVertex mv : ter)
						if (mv.z == nearest.z)
							selectedV.add(mv);
				} else if (down.contains("Shift"))
					selectedV.add(nearest);
				else {
					selectedV.clear();
					selectedV.add(nearest);
				}

			}

			if (getTile(evt.getPoint()) != null) {
				if (!in)
					sliding = 1;
				if (down.contains("Ctrl")) {
					if (!down.contains("Shift"))
						selectedT.clear();
				} else if (down.contains("Shift"))
					selectedT.add(getTile(evt.getPoint()));
				else {
					selectedT.clear();
					selectedT.add(getTile(evt.getPoint()));
				}
			} else
				sliding = -1;

			repaint();
		}

		KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().addKeyListener(this);
	}

	public MapTile getTile(Point p) {
		try {
			int i = (int) (p.x / pix + x);
			int j = (int) (p.y / pix + y);
			int k = (int) ((p.x + p.y + y * pix + x * pix) / ((i + 1) * pix + (j * pix)));
			return ter.getTileAt(i, j, k);
		} catch (IndexOutOfBoundsException ioobe) {
		}

		return null;
	}

	public void mouseMoved(MouseEvent evt) {
		if (ter != null) {
			nearest = nearest(new Point(evt.getX() + (int) (x * pix), evt.getY() + (int) (y * pix)));
			repaint();
		}
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}

	public void keyPressed(KeyEvent evt) {
		String key = KeyEvent.getKeyText(evt.getKeyCode());
		down.add(key);
	}

	public void keyReleased(KeyEvent evt) {
		String key = KeyEvent.getKeyText(evt.getKeyCode());
		down.remove(key);
	}

	public void keyTyped(KeyEvent evt) {
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == recreate) {
			ter = new Terrain(Integer.parseInt(params[0].text), Integer.parseInt(params[1].text), Integer
					.parseInt(params[2].text), Integer.parseInt(params[3].text));

			if (ter.getWidth() < 30 && ter.getHeight() < 30)
				pix = (getHeight() - 70) / (ter.getHeight() - 1);
			else
				pix = 35;

			y = -50 / (double) pix;
			x = -20 / (double) pix;

			edit = new EditVerticesPanel(this, cs, h, selectedV, selectedT, ter);
			add(edit);
			new Thread(this).start();
			setComponentZOrder(edit, 1);
			display.start(ter);
			tile.setEnabled(true);
			smooth.setEnabled(true);

			revalidate();
			repaint();
		} else if (evt.getSource() == save) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("Maps/"));

			int r = chooser.showSaveDialog(this);

			if (r == JFileChooser.APPROVE_OPTION)
				ter.save(chooser.getSelectedFile());
		} else if (evt.getSource() == load) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("Maps/"));

			int r = chooser.showOpenDialog(this);

			if (r == JFileChooser.APPROVE_OPTION)
				ter = Terrain.load(chooser.getSelectedFile());

			if (ter.getWidth() < 30 && ter.getHeight() < 30)
				pix = (getHeight() - 70) / (ter.getHeight() - 1);
			else
				pix = 35;

			y = -50 / (double) pix;
			x = -20 / (double) pix;

			edit = new EditVerticesPanel(this, cs, h, selectedV, selectedT, ter);
			add(edit);
			new Thread(this).start();
			setComponentZOrder(edit, 1);
			display.start(ter);
			tile.setEnabled(true);
			smooth.setEnabled(true);

			revalidate();
			repaint();
		} else if (evt.getSource() == back)
			owner.setPhase(-1);
		else if (evt.getSource() == random) {
			ter = Terrain.random(Integer.parseInt(params[0].text), Integer.parseInt(params[1].text), .15, 3);

			if (ter.getWidth() < 30 && ter.getHeight() < 30)
				pix = (getHeight() - 70) / (ter.getHeight() - 1);
			else
				pix = 35;

			y = -50 / (double) pix;
			x = -20 / (double) pix;

			edit = new EditVerticesPanel(this, cs, h, selectedV, selectedT, ter);
			add(edit);
			new Thread(this).start();
			setComponentZOrder(edit, 1);
			tile.setEnabled(true);
			smooth.setEnabled(true);
			display.start(ter);

			revalidate();
			repaint();
		} else if (evt.getSource() == tile && ter != null)
			ter.tile(.15);
		else if (evt.getSource() == smooth && ter != null)
			ter.smooth(2);
		else if (evt.getSource() == exit)
			System.exit(0);
	}

	public MapVertex nearest(Point p) {
		for (MapVertex m : ter) {
			if (areWithin(new Point((int) (pix * m.x), (int) (pix * m.y)), p, (int) (pix / 6)))
				return m;
		}
		return null;
	}

	public static boolean areWithin(Point p1, Point p2, int d) {
		return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y) <= d * d;
	}

	public void run() {
		int numSteps = 30;
		while (true) {
			if (ter != null) {
				try {
					Thread.sleep(10);
				} catch (Exception e) {
				}
				if (sliding == 1) {
					edit.setLocation(edit.getX() + edit.getWidth() / numSteps, edit.getY() - edit.getHeight()
							/ numSteps);
					if (edit.getX() >= 0 && edit.getY() + edit.getHeight() <= getHeight()) {
						in = true;
						sliding = 0;
					}
				} else if (sliding == -1) {
					edit.setLocation(edit.getX() - edit.getWidth() / numSteps, edit.getY() + edit.getHeight()
							/ numSteps);
					if (edit.getY() >= getHeight() && edit.getX() + edit.getWidth() <= 0) {
						in = false;
						sliding = 0;
					}
				}
				for (String key : down) {
					if (key.equals("Up"))
						y -= Math.log(move / (double) pix + 1) / 10;
					else if (key.equals("Left"))
						x -= Math.log(move / (double) pix + 1) / 10;
					else if (key.equals("Down"))
						y += Math.log(move / (double) pix + 1) / 10;
					else if (key.equals("Right"))
						x += Math.log(move / (double) pix + 1) / 10;

					repaint();
				}
				if (down.contains("Up") || down.contains("Left") || down.contains("Down") || down.contains("Right"))
					move++;
				else
					move = 1;
			}
		}

	}

	public static void setAlpha(Color q, int a) {
		Color asdf = new Color(q.getRed(), q.getGreen(), q.getBlue(), a);
		q = asdf;
	}

	public class GridPanel extends JPanel {
		private static final long serialVersionUID = 3296412791514741205L;

		public void paint(Graphics gg) {
			super.paint(gg);
			Graphics2D g = (Graphics2D) gg;

			g.translate((int) (-x * pix), (int) (-y * pix));

			if (ter != null) {
				double h = (getHeight()) / (double) pix;
				double w = (getWidth()) / (double) pix;
				/*
				 * for(int i = 10+(int)(x*pix)+(int)((x%1)*pix); i <=
				 * Math.min((int)(x*pix)+getWidth(),ter.getWidth()*pix); i+=pix)
				 * 
				 * 
				 * 
				 * g.drawLine(i,60+(int)(y*pix),i,Math.min((int)(y*pix)+getHeight
				 * (),ter.getHeight()*pix)-10);
				 * for(int j = 60+(int)(y*pix)+(int)((y%1)*pix); j <=
				 * Math.min((int)(y*pix)+getHeight(),ter.getHeight()*pix);
				 * j+=pix)
				 * 
				 * 
				 * 
				 * g.drawLine(10+(int)(x*pix),j,Math.min((int)(y*pix)+getHeight()
				 * ,ter.getHeight()*pix)-10,j);
				 */

				for (int i = (int) Math.max(x, 0); i <= Math.min(x + w, ter.getWidth() - 1); i++)
					g.drawLine((int) (i * pix), (int) Math.max(y * pix, 0), (int) (i * pix),
							(int) ((ter.getHeight() - 1) * pix));

				// if(x%1!=0)g.drawLine((int)(ter.getWidth()*pix),Math.max(0,(int)(y*pix)),
				// (int)(ter.getWidth()*pix),(int)(pix*Math.min(ter.getHeight(),h+y)));

				for (int j = (int) Math.max(y, 0); j <= Math.min(y + h, ter.getHeight() - 1); j++)
					g.drawLine((int) Math.max(x * pix, 0), (int) (j * pix), (int) ((ter.getWidth() - 1) * pix),
							(int) (j * pix));

				// if(y%1!=0)g.drawLine((int)(-x*pix),(int)(ter.getHeight()*pix),
				// (int)((ter.getWidth())*pix),(int)(ter.getHeight()*pix));

				for (int i = (int) x; i <= w + x + 1; i++)
					for (int j = (int) y; j <= w + y; j++) {
						for (int k = 0; k < 2; k++) {
							if (!(i >= 0 && j >= 0) || !(i < ter.getWidth() && j < ter.getHeight()))
								continue;

							g.setColor(Color.BLACK);
							g.drawString(getNumberWithin(ter.getHeightAt((int) i, (int) j), .01), (int) (i * pix),
									(int) (j * pix));

							if (ter.getVertexAt(i, j).walkable) {
								if (i < ter.getWidth() - 1 && j < ter.getHeight() - 1) {
									Color c = cs.colors[ter.getTileAt(i, j, k).m1.type];
									g.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 200));
									g.fillPolygon(new int[] { (int) (i * pix), (int) ((i + k) * pix),
											(int) ((i + 1) * pix) }, new int[] { (int) ((j + 1) * pix),
											(int) ((j + k) * pix), (int) (j * pix) }, 3);

									g.setColor(Color.RED);
									g.setStroke(new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));

									if (selectedT.contains(ter.getTileAt(i, j, k)))
										g.drawPolygon(new int[] { (int) (i * pix), (int) ((i + k) * pix),
												(int) ((i + 1) * pix) }, new int[] { (int) ((j + 1) * pix),
												(int) ((j + k) * pix), (int) (j * pix) }, 3);
								}
								g.setStroke(new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));

								if (selectedV.contains(ter.getVertexAt(i, j))) {
									g.setColor(Color.ORANGE);
									g.drawOval((int) (i * pix - pix / 6), (int) (j * pix - pix / 6), (int) (pix / 3),
											(int) (pix / 3));

								}
								if (nearest == ter.getVertexAt(i, j)) {
									g.setColor(Color.GREEN);
									g.drawOval((int) (i * pix - pix / 6), (int) (j * pix - pix / 6), (int) (pix / 3),
											(int) (pix / 3));
								}
								g.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));
							}
						}
					}

				if (begin != null && end != null) {
					g.setColor(new Color(190, 0, 0, 100));
					g.fillRoundRect(Math.min(begin.x, end.x) + (int) (x * pix), Math.min(begin.y, end.y)
							+ (int) (y * pix), Math.abs(begin.x - end.x), Math.abs(begin.y - end.y), 40, 40);
				}
				g.setColor(Color.WHITE);
			}
		}
	}

	public static String getNumberWithin(double num, double prec) {
		return "" + ((double) ((int) (num / prec)) * (double) prec);
	}
}
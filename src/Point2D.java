

public class Point2D {
	public double x;
	public double y;

	public Point2D(double a, double b) {
		this.x = a;
		this.y = b;
	}

	public void set(double a, double b) {
		this.x = a;
		this.y = b;
	}

	public void translate(double a, double b) {
		x += a;
		y += b;
	}

	public double distanceFrom(Point2D other) {
		return Math.sqrt((other.x - x) * (other.x - x) + (other.y - y) * (other.y - y));
	}
}
package framework;

import java.awt.*;

import util.ColorScheme;
import util.EvaluateMethod;

public class DrawingPoint extends Point3D implements Placable, java.io.Serializable {
	private static final long serialVersionUID = -5930158561790634610L;

	public String modelX;
	public String modelY;
	public String modelZ;
	public boolean moves = false;
	public DrawingPoint[] neighbors;

	DrawingPoint(double x, double y, double z, String equx, String equy, String equz) {
		super(x, y, z);
		moves = true;
		modelX = equx;
		modelY = equy;
		modelZ = equz;
		rgb = Color.GREEN.getRGB();
		System.out.println(rgb);
	}

	DrawingPoint() {
		this(0, 0, 0);
	}

	public DrawingPoint(double x, double y, double z) {
		super(x, y, z);
		moves = false;
		rgb = Color.GREEN.getRGB();
	}

	public void setNeighbors(DrawingPoint... neighbors) {
		this.neighbors = neighbors;
	}

	public int getRGB(ColorScheme cs) {
		return rgb;
	}

	public Point3D getPoint(double counter) {
		if (moves) {
			double x = EvaluateMethod.evaluate(modelX.replaceAll("x", counter + ""));
			double y = EvaluateMethod.evaluate(modelY.replaceAll("x", counter + ""));
			double z = EvaluateMethod.evaluate(modelZ.replaceAll("x", counter + ""));
			set(x, y, z);
		}

		return this;
	}

	public DrawingPoint[] getConnectedVertices() {
		return neighbors;
	}

	public void update(int mstime) {
	}

	public Point3D getLocation() {
		return getPoint(Mesh.counter);
	}

	public void setModels(String x, String y, String z) {
		if (x == null || x.equals(""))
			return;
		if (y == null || y.equals(""))
			return;
		if (z == null || z.equals(""))
			return;

		this.modelX = x;
		this.modelY = y;
		this.modelZ = z;
		if (x.indexOf("x") < 0 && y.indexOf("x") < 0 && z.indexOf("x") < 0) {
			this.moves = false;
			set(Integer.parseInt(x), Integer.parseInt(y), Integer.parseInt(z));
			return;
		}
		this.moves = true;
	}

	public void setImmutable() {
		getPoint(Mesh.counter);
		moves = false;
	}

}
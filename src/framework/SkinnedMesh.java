package framework;

import java.util.ArrayList;

public class SkinnedMesh extends Mesh implements Runnable {
	private static final long serialVersionUID = 6850686012842803299L;

	Bone[] bones;
	Animation current;
	int currentFrame;
	int delay;
	Point3D pivot;
	public Bone currentBone;

	public SkinnedMesh() {
		bones = new Bone[0];
	}

	public SkinnedMesh(Bone[] bones) {
		this.bones = bones;
	}

	public void run() {
		while (currentFrame < current.getNumFrames()) {
			try {
				Thread.sleep(delay);
			} catch (Exception e) {
			}
			currentFrame++;
			updatePoints();
		}
	}

	public Point3D getCenter() {
		return pivot;
	}

	public void setAnimation(Animation a) {
		currentFrame = 0;
		current = a;
	}

	public ArrayList<Face> getFaces() {
		return super.getFaces();
	}

	public void add(Bone b) {
		Bone[] copy = new Bone[bones.length + 1];
		for (int i = 0; i < bones.length; i++)
			copy[i] = bones[i];
		copy[bones.length] = b;

		bones = copy;

		currentBone = b;
	}

	public boolean add(DrawingPoint dp) {
		try {
			dp.add(currentBone.worldPos);
			super.add(dp);
			currentBone.attatchVertex(dp);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	public void updatePoints() {
		clear();

		for (Bone b : bones)
			b.addVerticesTo(this);
	}

	public Point3D getLocation() {
		return pivot;
	}
}
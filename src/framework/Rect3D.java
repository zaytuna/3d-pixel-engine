package framework;

public class Rect3D {
	public Point3D start;
	public double w, h, d;

	public Rect3D(double x, double y, double z, double w, double h, double d) {
		start = new Point3D(x, y, z);
		this.w = w;
		this.h = h;
		this.d = d;
	}
}
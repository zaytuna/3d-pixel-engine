package framework;

public class BoneTransform {
	public double dx, dy, dz, da, db, dg;

	public BoneTransform(double a, double b, double c, double d, double e, double f) {
		dx = a;
		dy = b;
		dz = c;
		da = d;
		db = e;
		dg = f;
	}
}
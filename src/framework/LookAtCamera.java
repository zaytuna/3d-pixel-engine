package framework;

import util.Methods;

public class LookAtCamera extends Camera {
	private static final long serialVersionUID = -2507490799660851945L;
	public Point3D focus = new Point3D(0, 0, 0);
	public double alpha;
	public double radius;

	public LookAtCamera() {
		this(0, 0, 0);
	}

	public LookAtCamera(double x, double y, double z) {
		this(x, y, z, 5 * Math.PI / 4, Math.PI / 2, 20);
	}

	public LookAtCamera(double x, double y, double z, double thetaH, double thetaV, int r) {
		super(0, 0, 0);
		focus.set(x, y, z);
		alpha = thetaH;
		beta = thetaV;
		radius = r;
		horizontalFOV = Math.toRadians(90 / 2);
		verticalFOV = Math.toRadians(75 / 2);
		buffer.update();
		updatePosition();
	}

	public double getNorthboundAngle() {
		return Methods.normalize(alpha - Math.PI / 2);
	}

	public void changeRadius(double r) {
		buffer.radius = buffer.radius + r;
	}

	public void setRadius(double r) {
		buffer.radius = r;
	}

	public void setFocus(Point3D p) {
		buffer.fx = p.x;
		buffer.fy = p.y;
		buffer.fz = p.z;
		updatePosition();
	}

	public void updateFocus() {
		double height = buffer.radius * Math.sin(buffer.beta - Math.PI / 2);
		double sideRadius = buffer.radius * Math.cos(buffer.beta - Math.PI / 2);

		buffer.fx = buffer.x - sideRadius * Math.cos(buffer.alpha + Math.PI / 2);
		buffer.fy = buffer.y - sideRadius * Math.sin(buffer.alpha + Math.PI / 2);
		buffer.fz = buffer.z - height;
	}

	public void set(int x, int y, int z) {
		buffer.x = x;
		buffer.y = y;
		buffer.z = z;
		updateFocus();
	}

	public void setHorizontalAngle(double angle, boolean sendToBuffer) {
		if (sendToBuffer)
			buffer.alpha = Methods.normalize(angle + Math.PI / 2);
		else
			this.alpha = Methods.normalize(angle + Math.PI / 2);
	}

	public void setFocusHeight(double d) {
		buffer.fz = d;
		updatePosition();
	}

	public void incrementHorizontalAngle(double amt) {
		buffer.alpha += amt;
		buffer.alpha = Methods.normalize(buffer.alpha);
		updatePosition();
	}

	public void incrementVerticalAngle(double amt) {
		super.incrementVerticalAngle(amt);
		updatePosition();
	}

	public void updatePosition() {
		double height = buffer.radius * Math.sin(buffer.beta - Math.PI / 2);
		double sideRadius = buffer.radius * Math.cos(buffer.beta - Math.PI / 2);

		buffer.x = buffer.fx + sideRadius * Math.cos(buffer.alpha + Math.PI / 2);
		buffer.y = buffer.fy + sideRadius * Math.sin(buffer.alpha + Math.PI / 2);
		buffer.z = buffer.fz + height;
	}
}
package framework;

import java.awt.*;
import java.util.*;

import util.BooleanReference;
import util.Methods;

public abstract class Camera extends Point3D implements Comparator<Face> {
	private static final long serialVersionUID = 6241230570898842490L;
	public static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	protected double beta, horizontalFOV, verticalFOV;
	public double speed = 1;
	public Vector3D velocity;
	// java.util.concurrent.locks.ReentrantLock lock = new
	// java.util.concurrent.locks.ReentrantLock();
	double nearDistance, farDistance;
	Plane[] frustrum = new Plane[6];// Left, Right, Top, Bottom, Near, Far
	BooleanReference nothing = new BooleanReference();

	public DeltaPosition buffer = new DeltaPosition();

	public Camera() {
		this(0, 0, 0);
	}

	public Camera(double x, double y, double z) {
		this(x, y, z, Math.PI / 2);
	}

	public Camera(double x, double y, double z, double thetaV) {
		super(x, y, z);
		beta = thetaV;
		velocity = new Vector3D(0, 0, 0);
		horizontalFOV = Math.toRadians(90 / 2);
		verticalFOV = Math.toRadians(75 / 2);
	}

	public void setX(double x) {
		buffer.x = x;
	}

	public void setY(double y) {
		buffer.y = y;
	}

	public void setZ(double z) {
		buffer.z = z;
	}

	public void decreaseView() {
		buffer.fovH = horizontalFOV * .95;
		buffer.fovV = verticalFOV * .95;
	}

	public void increaseView() {
		buffer.fovH = horizontalFOV / .95;
		buffer.fovV = verticalFOV / .95;
	}

	public void applyChanges() {
		buffer.apply();
	}

	public abstract double getNorthboundAngle();

	public abstract void incrementHorizontalAngle(double amt);

	public abstract void setHorizontalAngle(double angle, boolean sendToBuffer);

	public void setHorizontalAngle(double angle) {
		setHorizontalAngle(angle, true);
	}

	public void incrementVerticalAngle(double amt) {
		buffer.beta = Methods.normalize(amt + buffer.beta);
		if (buffer.beta < -Math.PI + .1)
			buffer.beta = Math.PI;
		else if (buffer.beta < 0)
			buffer.beta = 0;
	}

	public double getFOVH() {
		return horizontalFOV;
	}

	public double getFOVV() {
		return verticalFOV;
	}

	public double getThetaXYZ() {
		return beta;
	}

	public double getThetaXY() {
		return getNorthboundAngle();
	}

	public boolean cull(MapObject mo) {
		return false;
	}

	public void setVerticalAngle(double angle) {
		buffer.beta = Methods.normalize(angle);
	}

	public int getScreenX(Point3D v, int w) {
		return getScreenX(v, nothing, w);
	}

	public int getScreenY(Point3D v, int h) {
		return getScreenY(v, nothing, h);
	}

	public int getScreenX(Point3D v, BooleanReference isBlocked, int w) {
		Point3D t = Point3D.subtract(v, this);
		double theta = angleDifference(t.getHorizontalAngle(), getNorthboundAngle());
		double angleDifference = Math.signum(theta)
				* Math.acos(Math.sqrt((t.x * t.x + t.y * t.y) * Math.cos(theta) * Math.cos(theta) + t.z * t.z)
						/ t.getDistanceFromOrigin());

		isBlocked.andX(Math.abs(angleDifference) > horizontalFOV / 2);

		return w / 2 + (int) (angleDifference * w / horizontalFOV);
	}

	public int getSide(Point3D p) {
		if (Math.abs(angleDifference(p.getHorizontalAngle(), getNorthboundAngle())) < Math.PI / 2)
			return 1;
		return -1;
	}

	public int getScreenY(Point3D v, BooleanReference isBlocked, int h) {
		Point3D p = Point3D.subtract(v, this);
		double dist = Methods.getRotateY(p.x, p.y, -getNorthboundAngle());
		double angleDifference = angleDifference(getSide(p) * Math.acos(p.z / Math.sqrt(dist * dist + p.z * p.z)), beta);

		isBlocked.andY(Math.abs(angleDifference) > verticalFOV / 2);

		return h / 2 + (int) (angleDifference * h / verticalFOV);
	}

	public static double angleDifference(double angle1, double angle2) {
		// Rotate angle1 with angle2 so that the sought after
		// angle is between the resulting angle and the x-axis
		angle1 -= angle2;

		// "Normalize" angle1 to range [-180,180)
		while (angle1 < -Math.PI)
			angle1 += 2 * Math.PI;
		while (angle1 >= Math.PI)
			angle1 -= 2 * Math.PI;

		// angle1 has the signed answer, just "unsign it"
		return angle1;
	}

	public <T extends Placable> Point locationOf(T obj, int w, int h) throws UnseenException {
		return locationOf(obj.getLocation(), w, h);
	}

	public Point locationOf(Point3D object, int w, int h) throws UnseenException {
		return new Point(getScreenX(object, w), getScreenY(object, h));
	}

	public MapObjectSorter getMOSorter() {
		return new MapObjectSorter();
	}

	public class MapObjectSorter implements Comparator<MapObject> {
		public int compare(MapObject a, MapObject b) {
			return (int) Math.signum(b.getCenter().distanceFrom(Camera.this) - a.getCenter().distanceFrom(Camera.this));
		}
	}

	public int compare(Face a, Face b) {
		return (int) Math.signum(b.getCenter().distanceFrom(this) - a.getCenter().distanceFrom(this));
	}

	public class DeltaPosition {
		public double x = 0, y = 0, z = 0, beta = 0, alpha = 0, fovH = 0, fovV = 0, radius = 0, fx = 0, fy = 0, fz = 0;

		public void apply() {
			setHorizontalAngle(alpha, false);
			Camera.this.beta = beta;
			Camera.this.x = x;
			Camera.this.y = y;
			Camera.this.z = z;
			horizontalFOV = fovH;
			verticalFOV = fovV;

			if (Camera.this instanceof LookAtCamera) {
				LookAtCamera l = ((LookAtCamera) Camera.this);
				l.radius = radius;
				l.focus.x = fx;
				l.focus.y = fy;
				l.focus.z = fz;
			}
		}

		public void update() {
			alpha = getThetaXY();
			beta = getThetaXYZ();
			x = Camera.this.x;
			y = Camera.this.y;
			z = Camera.this.z;
			fovH = horizontalFOV;
			fovV = verticalFOV;

			if (Camera.this instanceof LookAtCamera) {
				LookAtCamera l = ((LookAtCamera) Camera.this);
				radius = l.radius;
				fx = l.focus.x;
				fy = l.focus.y;
				fz = l.focus.z;
			}
		}
	}
}
package framework;

import java.util.*;

import util.Methods;

public class Bone {
	private double length, alpha, beta, gamma;
	private Point3D start, end;
	private ArrayList<Bone> attachedToTop;
	private ArrayList<Bone> attachedToBottom;
	private ArrayList<VertexAttachment> transformedVertices;
	public Point3D worldPos = new Point3D(0, 0, 0);

	public Bone(double l, double a, double b, Point3D start, Point3D worldPos) {
		this.length = l;
		this.alpha = a;
		this.beta = b;
		this.worldPos = worldPos;
		this.start = start;
		updateEndPosition();
	}

	public void attatchVertex(DrawingPoint p) {
		Point3D deltaPos = Point3D.subtract(Point3D.subtract(p, start), worldPos);

		double xPrime = Methods.getRotateX(deltaPos.x, deltaPos.z, -beta);
		double zPrime = Methods.getRotateY(deltaPos.x, deltaPos.z, -beta);

		double xDoublePrime = Methods.getRotateX(xPrime, deltaPos.y, -alpha);
		double yPrime = Methods.getRotateY(xPrime, deltaPos.y, -alpha);

		double yDoublePrime = Methods.getRotateX(yPrime, zPrime, -gamma);
		double zDoublePrime = Methods.getRotateY(yPrime, zPrime, -gamma);

		transformedVertices.add(new VertexAttachment(p, xDoublePrime, yDoublePrime, zDoublePrime));
	}

	public void apply(BoneTransform b) {
		setStart(b.dx + start.x, b.dy + start.y, b.dz + start.z);
		setAlpha(b.da + alpha);
		setBeta(b.db + beta);
		setGamma(b.dg + gamma);
	}

	public void attachToTop(Bone b) {
		attachedToTop.add(b);
	}

	public void attachToBottom(Bone b) {
		attachedToBottom.add(b);
	}

	public void setStart(Point3D pos) {
		setStart(pos.x, pos.y, pos.z);
	}

	private void updateStartPosition() {
		double height = length * Math.sin(beta);
		double sideDist = length * Math.cos(beta);

		this.start = new Point3D(end.x - sideDist * Math.cos(alpha), end.y - sideDist * Math.sin(alpha), end.z - height);
	}

	private void updateEndPosition() {
		double height = length * Math.sin(beta);
		double sideDist = length * Math.cos(beta);

		this.end = new Point3D(start.x + sideDist * Math.cos(alpha), start.y + sideDist * Math.sin(alpha), start.z
				+ height);
	}

	private void updateAdjacentBones() {
		for (Bone b : attachedToTop)
			b.setStart(end.x, end.y, end.z);
		for (Bone b : attachedToBottom)
			b.setEnd(start.x, start.y, start.z);
	}

	public void setStart(double x, double y, double z) {
		start.set(x, y, z);
		updateVertexPositions();
		updateEndPosition();
		updateAdjacentBones();
	}

	public void setEnd(double x, double y, double z) {
		end.set(x, y, z);
		updateVertexPositions();
		updateStartPosition();
		updateAdjacentBones();
	}

	public void setGamma(double r) {
		gamma = r;
		updateVertexPositions();
	}

	public void setBeta(double v) {
		beta = v;
		updateVertexPositions();
		updateEndPosition();
		updateAdjacentBones();
	}

	public void setAlpha(double h) {
		alpha = h;
		updateVertexPositions();
		updateEndPosition();
		updateAdjacentBones();
	}

	public double getAlpha() {
		return alpha;
	}

	public double getBeta() {
		return beta;
	}

	public double getGamma() {
		return gamma;
	}

	public void updateVertexPositions() {
		for (VertexAttachment v : transformedVertices) {
			double yPrime = Methods.getRotateX(v.y, v.z, gamma);
			double zPrime = Methods.getRotateY(v.y, v.z, gamma);

			double xPrime = Methods.getRotateX(v.x, yPrime, alpha);
			double yDoublePrime = Methods.getRotateY(v.x, yPrime, alpha);

			double xDoublePrime = Methods.getRotateX(xPrime, zPrime, -beta);
			double zDoublePrime = Methods.getRotateY(xPrime, zPrime, -beta);

			v.vertex.set(worldPos.x + xDoublePrime + start.x, worldPos.y + yDoublePrime + start.y, worldPos.z
					+ zDoublePrime + start.z);
		}
	}

	public void addVerticesTo(List<DrawingPoint> list) {
		for (VertexAttachment v : transformedVertices)
			list.add(v.vertex);
	}

	public class VertexAttachment {
		DrawingPoint vertex;
		double x, y, z;

		VertexAttachment(DrawingPoint vertex, double o, double r, double p) {
			this.x = o;
			this.y = r;
			this.z = p;
		}
	}
}
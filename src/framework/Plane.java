package framework;

public class Plane {
	public double a, b, c, d;

	public Plane(Vector3D v, Point3D loc) {
		this.a = v.x;
		this.b = v.y;
		this.c = v.z;
		this.d = -loc.x * a - loc.y * b - loc.z * c;
	}

	public Plane(Point3D p1, Point3D p2, Point3D p3) {
		a = p1.y * (p2.z - p3.z) + p2.y * (p3.z - p1.z) + p3.y * (p1.z - p2.z);
		b = p1.z * (p2.x - p3.x) + p2.z * (p3.x - p1.x) + p3.z * (p1.x - p2.x);
		c = p1.x * (p2.y - p3.y) + p2.x * (p3.y - p1.y) + p3.x * (p1.y - p2.y);
		d = -p1.x * (p2.y * p3.z - p3.y * p2.z) + p2.x * (p3.y * p1.z - p1.y * p3.z) + p3.x
				* (p1.y * p2.z - p2.y * p1.z);
	}

	public static Vector3D getNormalFromPoints(Point3D p1, Point3D p2, Point3D p3) {
		return new Vector3D(p1.y * (p2.z - p3.z) + p2.y * (p3.z - p1.z) + p3.y * (p1.z - p2.z), p1.z * (p2.x - p3.x)
				+ p2.z * (p3.x - p1.x) + p3.z * (p1.x - p2.x), p1.x * (p2.y - p3.y) + p2.x * (p3.y - p1.y) + p3.x
				* (p1.y - p2.y));
	}

	public Vector3D getNormal() {
		return new Vector3D(a, b, c);
	}

	public int getSideOf(Point3D p) {
		return (int) Math.signum(a * p.x + b * p.y + c * p.z + d);
	}
}
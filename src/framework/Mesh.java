package framework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import util.Methods;

public class Mesh extends ArrayList<DrawingPoint> implements java.io.Serializable, Runnable {
	private static final long serialVersionUID = 1893765665514805177L;

	public static boolean first = true;
	public static double counter = 0;
	public Point3D offset = new Point3D(0, 0, 0);

	public ArrayList<Face> faces = new ArrayList<Face>();

	public Mesh() {
		super();
		if (first) {
			java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
			service.execute(this);
			service.shutdown();
		}
		first = false;
	}

	public void setOffsetReference(Point3D p) {
		offset = p;
	}

	public boolean add(DrawingPoint dp) {
		return add(dp, false);
	}

	public boolean add(DrawingPoint dp, boolean update) {
		boolean b = super.add(dp);
		if (update)
			calculateFaces();
		return b;
	}

	public boolean hasFace(Face f) {
		for (Face asdf : faces)
			if (asdf.occupiesSameSpace(f))
				return true;
		return false;
	}

	public boolean hasFaces() {
		return !(faces.size() == 0);
	}

	public void sortF(Comparator<Face> c) {
		Collections.sort(faces, c);
	}

	public <T extends Face> void setFaces(ArrayList<T> f) {
		faces.clear();

		for (T face : f)
			faces.add(face);
	}

	public void calculateFaces() {
		for (DrawingPoint n : this)
			calculateFacesFor(n);
	}

	public void calculateFacesFor(DrawingPoint dp) {
		for (DrawingPoint n : dp.neighbors)
			for (DrawingPoint m : n.neighbors)
				if (Methods.contains(dp.neighbors, m)) {
					Triangle3D face = new Triangle3D(dp, n, m);
					if (!hasFace(face)) {
						faces.add(face);
					}
				}
	}

	public ArrayList<Face> getFaces() {
		if (faces.size() == 0 && size() > 0)
			calculateFaces();
		return faces;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException ie) {
			}
			counter += .1;
		}
	}
}
package framework;

import util.ColorScheme;
import util.Methods;

public abstract class Face implements java.io.Serializable {
	private static final long serialVersionUID = 8651271257579202674L;
	public Vector3D normal;

	public Face() {
	}

	public Plane getPlane() {
		return new Plane(getP1().getLocation(), getP2().getLocation(), getP3().getLocation());
	}

	public void computeNormal() {
		normal = Plane.getNormalFromPoints(getP1().getLocation(), getP2().getLocation(), getP3().getLocation());
		if (normal.z < 0)
			normal.negate();
	}

	public Vector3D getNormal() {
		if (normal == null)
			computeNormal();

		return normal;
	}

	public abstract int getRGB(ColorScheme s);

	public abstract Placable getP1();

	public abstract Placable getP2();

	public abstract Placable getP3();

	// abstract Point3D getHighestPoint();
	// abstract Point3D getMidPoint();
	// abstract Point3D getLowPoint();
	public Point3D getCenter() {
		Point3D p1 = getP1().getLocation();
		Point3D p2 = getP2().getLocation();
		Point3D p3 = getP3().getLocation();
		return new Point3D((p1.x + p2.x + p3.x) / 3, (p1.y + p2.y + p3.y) / 3, (p1.z + p2.z + p3.z) / 3);
	}

	public double getBoundingSphereRadius() {
		Point3D center = getCenter();
		return Methods.max(getP1().getLocation().distanceFrom(center), getP2().getLocation().distanceFrom(center),
				getP3().getLocation().distanceFrom(center));
	}

	public boolean occupiesSameSpace(Face f) {
		return (((f.getP1().equals(getP1())) || (f.getP1().equals(getP2())) || (f.getP1().equals(getP3())))
				&& ((f.getP2().equals(getP1())) || (f.getP2().equals(getP2())) || (f.getP2().equals(getP3()))) && ((f
				.getP3().equals(getP1()))
				|| (f.getP3().equals(getP2())) || (f.getP3().equals(getP3()))));
	}
}
package framework;

import java.awt.*;
import java.util.*;

public class Patch implements MapObject, Iterable<MapVertex> {
	public Rectangle area;
	public static int numCreated = 0;
	public int index;
	public Terrain owner;
	public Mesh mesh;

	public Patch(Rectangle a, Terrain ter) {
		index = numCreated++;
		area = a;
		owner = ter;
		mesh = new Mesh();

		for (MapVertex mv : this)
			mesh.add(mv, false);

		// mesh.calculateFaces();
		mesh.setFaces(owner.getMapTileSubsetList(area));
	}

	public String toString() {
		return "Patch " + index + " of " + numCreated + "\nUsing" + area.toString();
	}

	public Point3D getCenter() {
		double x = area.x + area.width / (double) 2;
		double y = area.y + area.height / (double) 2;
		double z = 0;
		for (MapVertex m : this)
			z += m.z;

		return new Point3D(x, y, z / (area.width * area.height));
	}

	public ArrayList<Point3D> getPoints() {
		ArrayList<Point3D> list = new ArrayList<Point3D>();
		for (MapVertex m : this)
			list.add(m.getLocation());
		return list;
	}

	public Mesh getMesh() {
		return mesh;
	}

	public Point3D getLocation() {
		return getCenter();
	}

	public Iterator<MapVertex> iterator() {
		return new TIterator();
	}

	public Rect3D getBoundingBox() {
		return new Rect3D(area.x, area.y, minHeight(), area.x + area.width, area.y + area.height, maxHeight());
	}

	public double minHeight() {
		double min = Integer.MAX_VALUE;
		for (MapVertex v : this)
			if (v.z < min)
				min = v.z;
		return min;
	}

	public double maxHeight() {
		double max = Integer.MIN_VALUE;
		for (MapVertex v : this)
			if (v.z > max)
				max = v.z;
		return max;
	}

	public double getBoundingSphereRadius() {
		Point3D p = getCenter();
		return Math.sqrt(area.width * area.width + area.height * area.height + p.z * p.z);
	}

	class TIterator implements Iterator<MapVertex> {
		MapVertex current = null;

		public boolean hasNext() {
			if (current == null)
				return true;
			return !(current.x == area.width + area.x - 1 && current.y == area.height + area.y - 1);
		}

		public MapVertex next() {
			if (current == null)
				current = owner.getVertexAt(area.x, area.y);
			else
				current = owner.getVertexAt((current.x + 1 - area.x) % (area.width) + area.x, current.y
						+ (current.x + 1 - area.x) / (area.width));
			return current;
		}

		public void remove() {
		}
	}

	public static void main(String args[]) {
		Patch patch = new Patch(new Rectangle(1, 1, 2, 2), new Terrain(20, 20, 10, 10));
		for (MapVertex m : patch)
			System.out.print("(" + m.x + "," + m.y + ")   ");
	}
}
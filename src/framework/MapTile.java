package framework;

import game.Unit;
import util.ColorScheme;

public class MapTile extends Face implements java.io.Serializable {
	private static final long serialVersionUID = 6409964174013636255L;

	public MapVertex m1, m2, m3;
	/*
	 * Forest 30
	 * Jungle 40
	 * SandDesert 20
	 * RockDesert 15
	 * Plains 15
	 * Tundra 20
	 * Icesheet 20
	 * Hills 30
	 * Mountains 50
	 * HighMountains 60
	 * Swamp 50
	 * Bog 40
	 * RedRock 30
	 * City 13
	 * Ocean 20
	 * Lake 10
	 * River 15
	 * PavedRoad 10
	 * DirtRoad 13
	 */
	transient Unit occuppyingUnit = null;

	public MapTile(MapVertex m1, MapVertex m2, MapVertex m3) {
		this.m1 = m1;
		this.m2 = m2;
		this.m3 = m3;
	}

	public Placable getP1() {
		return m1;
	}

	public Placable getP2() {
		return m2;
	}

	public Placable getP3() {
		return m3;
	}

	public int getRGB(ColorScheme s) {
		return s.getColorForType(m1.type).getRGB()
				| (m1.type == 0 ? (int) (Math.random() * 25) | (int) (Math.random() * 25) << 8
						| (int) (Math.random() * 25) << 16 : 0);
	}
}
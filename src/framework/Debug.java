package framework;

import java.io.*;

public class Debug {
	public static File dump = new File("log.txt");

	public static void init() {
		try {
			if (!dump.exists())
				dump.createNewFile();
		} catch (Exception e) {
		}
	}

	public static void log(Throwable e) {
		try {
			if (!dump.exists())
				init();
			StackTraceElement[] s = e.getStackTrace();
			BufferedWriter out = new BufferedWriter(new FileWriter(dump));
			for (int i = 0; i < s.length; i++) {
				out.write("\n\tAt " + s[i].toString());
				out.flush();
			}
			out.close();
		} catch (Exception f) {
			javax.swing.JOptionPane.showMessageDialog(null, "ERROR in Debug.log(Throwable)\n" + f);
		}
	}
}
package framework;

import java.util.*;

public class Animation {
	public SkinnedMesh owner;
	public Map<Bone, ArrayList<BoneTransform>> transformations = new HashMap<Bone, ArrayList<BoneTransform>>();
	public String name;

	public Animation(SkinnedMesh owner, String name) {
		this.owner = owner;
		this.name = name;
	}

	public void addTransform(Bone b, BoneTransform t) {
		if (!transformations.keySet().contains(b))
			transformations.put(b, new ArrayList<BoneTransform>());
		transformations.get(b).add(t);

	}

	public BoneTransform getTranformation(int num, Bone b) {
		return transformations.get(b).get(num % transformations.get(b).size());
	}

	public int getNumFrames() {
		int max = Integer.MIN_VALUE;
		for (Bone b : transformations.keySet()) {
			if (transformations.get(b).size() > max)
				max = transformations.get(b).size();
		}
		return max;
	}
}

package framework;

import util.ColorScheme;

public class MapVertex extends DrawingPoint implements Placable, java.io.Serializable {
	private static final long serialVersionUID = 457896758308491465L;
	
	public transient final static int WIDTH = 30;
	public transient final static int HEIGHT = 30;
	public transient final static int DEPTH = 30;
	public boolean walkable;
	public int x, y, cost, type;
	public transient MapVertex[] neighbors;
	// For Pathfinding
	public transient int F, G, H;
	public transient boolean open, closed;
	public transient MapVertex parentTile;
	public transient Vector3D normal = new Vector3D(0, 0, 1);
	public transient double u = 0, v = 0;

	public MapVertex(int x, int y, double h, int c, int type, boolean b) {
		super((double) x, (double) y, h);
		this.cost = c;
		this.walkable = b;
		this.x = x;
		this.y = y;
		this.type = type;
		super.setNeighbors(neighbors);

	}

	public void setUVCoordinate(double x, double y) {
		this.u = x;
		this.v = y;
	}

	public void computeNormal() {
		if (normal == null)
			normal = new Vector3D(0, 0, 0);
		else
			normal.set(0, 0, 0);
		if (neighbors.length > 1) {
			for (int i = 0; i < neighbors.length; i++) {
				Vector3D v = Plane.getNormalFromPoints(neighbors[i], neighbors[(i + 1) % neighbors.length], this);
				if (v.z < 0)
					v.negate();
				normal.add(v);
			}
		}
	}

	public void setHeight(double h) {
		this.z = h;
	}

	public static Point3D interpolatePoint3D(Point3D a, Point3D b, double p) {
		return new Point3D(a.x + p * (b.x - a.x), a.y + p * (b.y - a.y), a.z + (b.z - a.z));
	}

	public Point3D getLocation() {
		return this;
	}

	public int getRGB(ColorScheme cs) {
		return cs.getColorForType(type).getRGB();
	}

	public void setNeighbors(MapVertex... n) {
		this.neighbors = n;
		super.setNeighbors(neighbors);
	}
}
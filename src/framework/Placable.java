package framework;

import util.ColorScheme;

public interface Placable {
	public Point3D getLocation();

	public int getRGB(ColorScheme cs);
}
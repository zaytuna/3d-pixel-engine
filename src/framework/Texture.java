package framework;

import java.awt.image.*;

public class Texture {
	public double u1, u2, u3, v1, v2, v3;
	public BufferedImage img;

	public Texture(BufferedImage image, double u1, double u2, double u3, double v1, double v2, double v3) {
		this.img = image;

		this.u1 = u1;
		this.v1 = v1;
		this.u2 = u2;
		this.v2 = v2;
		this.v3 = v3;
		this.u3 = u3;
	}

	public Texture(double u1, double u2, double u3, double v1, double v2, double v3, String name)
			throws java.io.IOException {
		this(javax.imageio.ImageIO.read(new java.io.File(name)), u1, v1, u2, v2, u3, v3);
	}

}
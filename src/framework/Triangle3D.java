package framework;

import util.ColorScheme;

public class Triangle3D extends Face {
	private static final long serialVersionUID = 1921835710290811069L;
	Point3D p1, p2, p3;
	int rgb = java.awt.Color.RED.getRGB();

	public Triangle3D(Point3D... points) {
		this.p1 = points[0];
		this.p2 = points[1];
		this.p3 = points[2];
	}

	public Point3D getP1() {
		return p1;
	}

	public Point3D getP2() {
		return p2;
	}

	public Point3D getP3() {
		return p3;
	}

	public int getRGB(ColorScheme cs) {
		return rgb;
	}
}
package framework;

public interface MapObject {
	public Mesh getMesh();

	public double getBoundingSphereRadius();

	public Point3D getCenter();

	public Rect3D getBoundingBox();
}
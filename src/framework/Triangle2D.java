package framework;

public class Triangle2D {
	public int x1, y1, x2, y2, x3, y3;
	public int rgb1, rgb2, rgb3;
	public double u1, v1, u2, v2, u3, v3;
	public Face rep;

	public Triangle2D(int x1, int y1, int x2, int y2, int x3, int y3, Face f, int rgb1, int rgb2, int rgb3) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.x3 = x3;
		this.y3 = y3;
		this.rep = f;
		this.rgb1 = rgb1;
		this.rgb2 = rgb2;
		this.rgb3 = rgb3;
	}

	public void set(int x1, int y1, int x2, int y2, int x3, int y3, Face f, int rgb1, int rgb2, int rgb3) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.x3 = x3;
		this.y3 = y3;
		this.rep = f;
		this.rgb1 = rgb1;
		this.rgb2 = rgb2;
		this.rgb3 = rgb3;
	}

	public Face getFace() {
		return rep;
	}

	public String toString() {
		return "Triangle of(" + x1 + "," + y1 + "), (" + x2 + "," + y2 + "), and (" + x3 + "," + y3 + ")";
	}
}
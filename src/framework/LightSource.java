package framework;

public interface LightSource {
	public Vector3D getRayTo(Point3D p);

	public double getIntensity();

	public int getColor();
}
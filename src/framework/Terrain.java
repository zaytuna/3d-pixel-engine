package framework;

import java.awt.*;
import java.io.*;
import java.util.*;

import util.Methods;

public class Terrain implements java.io.Serializable, Iterable<MapVertex>, MapObject
{
	public static final long serialVersionUID = -5391891230414138563L;

	transient public static int DEFAULT_TERRAIN_TYPE = 4;
	transient public final static int[] dx = {-1,-1,-1,0,0,1,1,1};
	transient public final static int[] dy = {-1,0,1,-1,1,-1,0,1};

	public MapVertex[][] vertices;//[X][Y]
	public MapTile[][][] tiles;//[x][y][1 or 0]
	public transient Patch[] patches;

	transient int patchWidth,patchHeight;
	transient Mesh mesh = new Mesh();

	public Terrain(int x, int y, int defaultHeightForTerrain,int defaultCostToCross)
	{
		System.out.print(defaultHeightForTerrain);
		vertices = new MapVertex[x][y];
		tiles = new MapTile[x-1][y-1][2];

		for(int i = 0; i < x; i++)
			for(int j = 0; j < y; j++)
				vertices[i][j] = new MapVertex(i,j,defaultHeightForTerrain, defaultCostToCross, DEFAULT_TERRAIN_TYPE, true);

		for(int i = 0; i < x-1; i++)
			for(int j = 0; j < y-1; j++)
				for(int k = 0; k < 2; k++)
					tiles[i][j][k] = new MapTile(k==0?vertices[i][j]:vertices[i+1][j+1],
						vertices[i+1][j],vertices[i][j+1]);

		for(int i = 0; i < x; i++)
			for(int j = 0; j < y; j++)
			{
				ArrayList<MapVertex> neighbors = new ArrayList<MapVertex>();
				for(int q = 0; q < 8; q++)
					if((i+dx[q]>=0)&&(j+dy[q]>=0)&&(i+dx[q]<x)&&(j+dy[q]<y))
						neighbors.add(vertices[i+dx[q]][j+dy[q]]);
				MapVertex[] array = new MapVertex[neighbors.size()];
				for(int n = 0; n < neighbors.size(); n++)
					array[n] = neighbors.get(n);
				vertices[i][j].setNeighbors(array);
			}

		for(int i = 0; i < x; i++)
			for(int j = 0; j < y; j++)
				mesh.add(vertices[i][j]);

		mesh.setFaces(getMapTileSubsetList(new Rectangle(0,0,x,y)));
		createPatches(Math.max((x+y)/70,1));
	}
	public static double pseudoRandom(int x)
	{
		x = (x<<13) ^ x;
 		return (1.0f - ((x * (x*x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0d);
	}
	public double getHeightAt(double x, double y)
	{
		return 0;
	}
	public void enforceMax(double m)
	{
		for(MapVertex v: this)
		{
			if(v.z>m)v.z = m;
			else if(v.z<-m)v.z = -m;
		}
	}
	public static Terrain random(int x, int y, double percentWater,int smooth)
	{
		Terrain t = randomize(x,y,(int)(Math.random()*300),25,20,4);
		t.multiply(randomize(x,y,(int)(Math.random()*300),.5,1,4));
		t.scale(60);

		//Tiling
		for(int i = 0 ; i < smooth; i++)
			t.smooth(2);

		//t.enforceMax(50);

		t.tile(percentWater);
		t.computeNormals();

		return t;
	}
	public static Terrain fractal(double percentWater,double roughness,int lod)
	{
		int divisions = 1<<lod;
		Terrain t = new Terrain(divisions+1,divisions+1,0,10);
		t.fill(-1234);

		t.setHeightAt(0,0,Math.random()*50-25);
		t.setHeightAt(divisions,0,Math.random()*50-25);
		t.setHeightAt(0,divisions,Math.random()*50-25);
		t.setHeightAt(divisions,divisions,Math.random()*100-50);

		t.diamondSquare(0,0,divisions,roughness,lod);
		t.tile(percentWater);
		return t;
	}
	public void fill(double d)
	{
		for(MapVertex v:this)
			v.z = d;
	}
	private double random()
	{
		return 2*Math.random()-1;
	}
	public void diamondSquare(int x, int y, int side, double roughness, int lodLeft)
	{
		if(lodLeft<=0)return;

		if(getHeightAt(x+side/2,y)==-1234)setHeightAt(x+side/2,y,(vertices[x][y].z+vertices[x+side][y].z)/2+random()*roughness);
		if(getHeightAt(x+side/2,y+side)==-1234)setHeightAt(x+side/2,y+side,(vertices[x][y+side].z+vertices[x+side][y+side].z)/2+random()*roughness);
		if(getHeightAt(x,y+side/2)==-1234)setHeightAt(x,y+side/2,(vertices[x][y].z+vertices[x][y+side].z)/2+random()*roughness);
		if(getHeightAt(x+side,y+side/2)==-1234)setHeightAt(x+side,y+side/2,(vertices[x+side][y].z+vertices[x+side][y+side].z)/2+random()*roughness);

		setHeightAt(x+side/2,y+side/2,
			(vertices[x][y].z+vertices[x+side][y].z+vertices[x+side][y+side].z+vertices[x][y+side].z)/4+random()*roughness);

		diamondSquare(x,y,side/2,roughness/2,lodLeft-1);
		diamondSquare(x,y+side/2,side/2,roughness/2,lodLeft-1);
		diamondSquare(x+side/2,y,side/2,roughness/2,lodLeft-1);
		diamondSquare(x+side/2,y+side/2,side/2,roughness/2,lodLeft-1);
	}
	public void calculateUVs()
	{
		for(int i = 0; i < getWidth(); i++)
			for(int j = 0; j < getHeight(); j++)
			{
				vertices[i][j].setUVCoordinate(i/(double)getWidth(),j/(double)getHeight());
			}
	}
	public void tile(double percentWater)
	{
		Point[] points = new Point[(int)(Math.random()*getWidth()*getHeight()/6D)];
		int[] colors = new int[points.length];
		int[] radii = new int[points.length];

		for(int q = 0; q < points.length; q++)
		{
			points[q] = new Point((int)(Math.random()*getWidth()),(int)(Math.random()*getHeight()));
			colors[q] = (int)(Math.random()*4)+1;
			radii[q] = (int)(Math.random()*30)+5;
		}

		double min = minHeight()+percentWater*(maxHeight()-minHeight());

		for(int i = 0; i < getWidth(); i++)
			for(int j = 0; j < getHeight(); j++)
			{
				if(getHeightAt(i,j)<=min)
				{
					setHeightAt(i,j,min);
					vertices[i][j].type = 0;
				}
				else for(int l = 0; l < points.length; l++)
					if((i-points[l].x)*(i-points[l].x)+(j-points[l].y)*(j-points[l].y)<radii[l]*radii[l])
					{
						vertices[i][j].type = colors[l];
						break;
					}
					else
						vertices[i][j].type = (int)(Math.random()*4)+1;
			}
	}
	public static Terrain terrainFromImage(java.awt.image.BufferedImage img, int w, int h)
	{
		Terrain t = new Terrain(w,h,0,10);
		double dx = img.getWidth()/w,dy = img.getHeight()/h;

		for(int x = 0; x < w; x++)
			for(int y = 0; y < h; y++)
			{
				int rgb = img.getRGB((int)(x*dx),(int)(y*dy));
				int r = (rgb >> 16) & 0xff;
				int g = (rgb >>  8) & 0xff;
				int b = (rgb >>  0) & 0xff;
				double avg = 255-(r+g+b)/15D;

				t.setHeightAt(x,y,avg-10);
			}

		t.computeNormals();

		return t;
	}
	public static Terrain randomize(int x, int y, int seed, double noiseSize, double persistence, int octaves)
	{
		Terrain ter = new Terrain(x,y,0,10);

		for(int i = 0; i < x; i++)
			for(int j = 0; j < y; j++)
			{
				/*
				code retrieved from "Programming an RTS Game with Direct3D"
				Chapter 4 creating random heightmaps
				*/
				//Scale x & y to the range of 0.0 - m_size
				double xf = ((double)i / (double)x) * noiseSize;
				double yf = ((double)j / (double)y) * noiseSize;

				double total = 0;

				// For each octave
				for(int k=0;k<octaves;k++)
				{
					//Calculate frequency and amplitude (different for each octave)
					double freq = Math.pow(2, k);
					double amp = Math.pow(persistence, k);

					//Calculate the x,y noise coordinates
					double tx = xf * freq;
					double ty = yf * freq;
					int tx_int = (int)tx;
					int ty_int = (int)ty;

					//Calculate the fractions of x & y
					double fracX = tx - tx_int;
					double fracY = ty - ty_int;

					//Get the noise of this octave for each of these 4 points
					double v1 = pseudoRandom(tx_int + ty_int * 57 + seed);
					double v2 = pseudoRandom(tx_int+ 1 + ty_int * 57 + seed);
					double v3 = pseudoRandom(tx_int + (ty_int+1) * 57 + seed);
					double v4 = pseudoRandom(tx_int + 1 + (ty_int+1) * 57 + seed);

					//Smooth in the X-axis
					double i1 = cosInterpolate(v1 , v2 , fracX);
					double i2 = cosInterpolate(v3 , v4 , fracX);

					//Smooth in the Y-axis
					total += cosInterpolate(i1 , i2 , fracY) * amp;
				}

				//Save to heightMap
				ter.setHeightAt(i,j,total);
		}
		ter.computeNormals();
		return ter;
	}
	public static double cosInterpolate(double a, double b, double x)
	{
		double ft = x * Math.PI;
		double f = (1 - Math.cos(ft)) * .5;

		return  a*(1-f) + b*f;
	}
	public void computeNormals()
	{
		for(MapTile m:tiles())
			m.computeNormal();
		for(MapVertex v:this)
			v.computeNormal();
	}
	public Mesh getMesh()
	{
		return mesh;
	}
	public ArrayList<Point3D> getPoints()
	{
		ArrayList<Point3D> list = new ArrayList<Point3D>();

		for(MapVertex v:this)
			list.add(new Point3D(v.x*MapVertex.WIDTH,v.y*MapVertex.HEIGHT,v.z));

		return list;
	}
	public Point3D getCenter()
	{
		return new Point3D(getWidth()/2,getHeight()/2,(int)avgHeight());
	}
	public Rect3D getBoundingBox()
	{
		return new Rect3D(0,0,minHeight(),getWidth(),getHeight(),maxHeight());
	}
	public double getBoundingSphereRadius()
	{
		return (int)(Math.sqrt((maxHeight()-minHeight())*(maxHeight()-minHeight())+
			getWidth()*getWidth()/4+getHeight()*getHeight()/4));
	}
	public void smooth(int r)
	{
		double[][] map = new double[vertices.length][vertices[0].length];
		for(int i = 0; i < vertices.length; i++)
		{
			for(int j = 0; j <vertices[i].length; j++)
			{
				double avg = 0;
				int count = 0;
				for(int q = -r; q<r+1; q++)
					for(int k = -r; k <r+1; k++)
						try
						{
							avg += vertices[i+q][j+k].z;
							count++;
						}
						catch(IndexOutOfBoundsException ioobe){}

				avg = avg/(double)count;
				map[i][j] = avg;
			}
		}

		for(int i = 0; i < vertices.length; i++)
			for(int j = 0; j <vertices[i].length; j++)
				vertices[i][j].z = map[i][j];
	}
	public ArrayList<MapTile> getMapTileSubsetList(Rectangle r)
	{
		ArrayList<MapTile> subset = new ArrayList<MapTile>();
		for(int i = r.x; i < r.width+r.x; i++)
			for(int j = r.y; j < r.height+r.y; j++)
				for(int k = 0; k < 2; k++)
					if(i<getWidth()-1&&j<getHeight()-1)
						subset.add(tiles[i][j][k]);

		return subset;
	}
	public Iterator<MapVertex> iterator()
	{
		return new VIterator();
	}
	public void save(File f)
	{
		try
		{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f.getAbsolutePath()+".map"));
			out.writeObject(this);
			out.flush();
			out.close();
		}
		catch(IOException ioe){ioe.printStackTrace();}
	}
	public MapVertex getVertexAt(int i , int y)
	{
		return vertices[i][y];
	}
	public MapTile getTileAt(int i , int j, int k)
	{
		return tiles[i][j][k];
	}
	public static Terrain load(File f)
	{
		try
		{
			System.out.print("\n\nLoading...");
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
			Terrain t = (Terrain)in.readObject();
			in.close();
			t.mesh = new Mesh();
			for(int i = 0; i < t.getWidth(); i++)
			{
				for(int j = 0; j < t.getHeight(); j++)
				{
					System.out.print(".");
					ArrayList<MapVertex> neighbors = new ArrayList<MapVertex>();
					for(int q = 0; q < 8; q++)
						if((i+dx[q]>=0)&&(j+dy[q]>=0)&&(i+dx[q]<t.getWidth())&&(j+dy[q]<t.getHeight()))
							neighbors.add(t.vertices[i+dx[q]][j+dy[q]]);
					MapVertex[] array = new MapVertex[neighbors.size()];
					for(int n = 0; n < neighbors.size(); n++)
						array[n] = neighbors.get(n);
					t.vertices[i][j].neighbors = array;
					t.mesh.add(t.vertices[i][j]);
				}
			}

			t.mesh.setFaces(t.getMapTileSubsetList(new Rectangle(0,0,t.getWidth(),t.getHeight())));
			t.createPatches(Math.max((t.getWidth()+t.getHeight())/70,1));
			System.out.println("Done");
			return t;
		}
		catch(Exception ioe){ioe.printStackTrace();}
		return null;
	}
	public void setLinearHeightAt(int x, int y, double h, double slope)
	{
		double dh = h-getHeightAt(x,y);
		if(dh<0)slope = -Math.abs(slope);
		int distanceFromBase = (int)(dh/slope);

		for(int i = 0; i < getWidth(); i++)
		{
			for(int j = 0; j < getHeight(); j++)
			{
				try
				{
					double distanceFromXY = Math.sqrt(Math.pow(i-x,2)+Math.pow(j-y,2));
					vertices[i][j].z += slope*Math.max(distanceFromBase-distanceFromXY,0);
				}catch(IndexOutOfBoundsException ioobe){}
			}
		}

	}
	public void setLogisticHeightAt(int x, int y, double h, double slope)
	{
		double dh = h-getHeightAt(x,y);
		for(int i = 0; i < vertices.length; i++)
		{
			for(int j = 0; j <vertices[i].length; j++)
			{
				double distanceFromXY = Math.sqrt(Math.pow(i-x,2)+Math.pow(j-y,2));
				vertices[i][j].z += (dh/(1+Math.pow(Math.E,(distanceFromXY*slope-(2*Math.E/slope)))));
			}
		}

	}
	public void setLinearHeightAt(Rectangle rect, double h, double slope)
	{
		double dh = h-avgHeight(rect);
		if(dh<0)slope = -Math.abs(slope);
		int distanceFromBase = (int)(h/slope);

		for(int i = (rect.x-distanceFromBase); i < (rect.x+rect.width+distanceFromBase); i++)
		{
			for(int j = (rect.y-distanceFromBase); j <(rect.y+rect.height+distanceFromBase); j++)
			{
				try
				{
					Point distPoint = Methods.getClosestPointOn(rect,new Point(i,j));
					double distanceFromXY = Math.sqrt(Math.pow(distPoint.x-i,2)+Math.pow(distPoint.y-j,2));
					vertices[i][j].z += slope*Math.max(distanceFromBase-distanceFromXY,0);
				}
				catch(IndexOutOfBoundsException ioobe){}
			}
		}

	}
	public void setLogisticHeightAt(Rectangle rect, double h, double slope)
	{
		double dh = h-avgHeight(rect);
		for(int i = 0; i < vertices.length; i++)
		{
			for(int j = 0; j <vertices[i].length; j++)
			{
				Point distPoint = Methods.getClosestPointOn(rect,new Point(i,j));
				double distanceFromXY = Math.sqrt(Math.pow(i-distPoint.x,2)+Math.pow(j-distPoint.y,2));
				vertices[i][j].z += dh/(1+Math.pow(Math.E,(distanceFromXY*slope-(2*Math.E/slope))));
			}
		}

	}
	public void setHeightAt(Rectangle rect, double h)
	{
		for(int i = rect.x; i < rect.x+rect.width; i++)
			for(int j = rect.y; j < rect.y+rect.height; j++)
				vertices[i][j].z  = h;
	}
	public double maxHeight()
	{
		double max = getHeightAt(0,0);

		for(int i = 0; i < vertices.length; i++)
			for(int j = 0; j <vertices[i].length; j++)
				if(getHeightAt(i,j)>max)
					max = getHeightAt(i,j);

		return max;
	}
	public double minHeight()
	{
		double min = getHeightAt(0,0);

		for(int i = 0; i < vertices.length; i++)
			for(int j = 0; j <vertices[i].length; j++)
				if(getHeightAt(i,j)<min)
					min = getHeightAt(i,j);

		return min;
	}
	public double avgHeight()
	{
		double total = 0;

		for(int i = 0; i < vertices.length; i++)
			for(int j = 0; j <vertices[i].length; j++)
				total+=vertices[i][j].z;

		return total/(getWidth()*getHeight());
	}
	public double getHeightAt(int x, int y)
	{
		return vertices[x][y].z;
	}
	public String toString()
	{
		String s = "";
		for(int j = 0; j <vertices[0].length; j++)
		{
			for(int i = 0; i < vertices.length; i++)
				s+=vertices[i][j].z+", ";
			s+= "\n";
		}
		return s;
	}
	public int getWidth()
	{
		return vertices.length;
	}
	public int getHeight()
	{
		return vertices[0].length;
	}
	public void setHeightAt(int x, int y, double h)
	{
		vertices[x][y].z = h;
	}
	public Color getColorForHeight(double h)
	{
		return new Color((int)(((h+1)*125)/(maxHeight()+1)),(int)(((h+1)*155)/(maxHeight()+1)),0);
	}
	public Patch getPatch(int index)
	{
		return patches[index];
	}
	public double avgHeight(Rectangle r)
	{
		double avg = 0;
		for(int i = r.x; i < r.width+r.x; i++)
			for(int j = r.y; j < r.height+r.y; j++)
				avg+=getHeightAt(i,j);

		return avg/(r.width*r.height);
	}
	public Patch getPatch(MapVertex mv)
	{
		if(patchWidth==0)
			createPatches((getWidth()+getHeight())/200);
		int x = mv.x/patchWidth;
		int y = mv.y/patchHeight;

		return patches[y*getWidth()/patchWidth+x];
	}
	public Iterable<MapTile> tiles()
	{
		return new Tiles();
	}
	public void createPatches(int numPatches)
	{
		patches = new Patch[numPatches*numPatches];

		patchWidth = (getWidth()-1)/numPatches;
		patchHeight = (getHeight()-1)/numPatches;

		for(int i = 0; i < numPatches; i++)
			for(int j = 0; j <numPatches; j++)
			{
				Rectangle rect = new Rectangle(i*patchWidth,j*patchHeight,
					patchWidth,patchHeight);
				patches[i*numPatches+j] = new Patch(rect,this);
			}
	}
	class VIterator implements Iterator<MapVertex>
	{
		MapVertex current = null;
		public boolean hasNext()
		{
			if(current == null)return true;
			return !(current.x == getWidth()-1&&current.y==getHeight()-1);
		}
		public MapVertex next()
		{
			if(current == null) current = getVertexAt(0,0);
			else current = getVertexAt((current.x+1)%getWidth(),current.y+(current.x+1)/getWidth());

			return current;
		}
		public void remove()
		{
		}
	}
	class TIterator implements Iterator<MapTile>
	{
		int i, j, k;
		boolean first = true;
		public boolean hasNext()
		{
			if(first)return true;
			return !(i == getWidth()-2&&j==getHeight()-2);
		}
		public MapTile next()
		{
			if(first)
			{
				first = false;
				i = 0; j = 0; k = 0;
			}
			k = (k+1)%2;
			j = j+(i+(k+1)/2)/(getWidth()-1);
			i = (i+(k+1)/2)%(getWidth()-1);

			return getTileAt(i,j,k);
		}
		public void remove()
		{
		}
	}
	public void scale(double d)
	{
		for(int i = 0; i < getWidth(); i++)
			for(int j = 0; j < getHeight(); j++)
				setHeightAt(i,j,getHeightAt(i,j)*d);

	}
	public void multiply(Terrain t)
	{
		for(int i = 0; i < getWidth(); i++)
			for(int j = 0; j < getHeight(); j++)
			{
				setHeightAt(i,j,getHeightAt(i,j)*t.getHeightAt(i,j)/(maxHeight()*t.maxHeight()));
			}
	}
	public class Tiles implements Iterable<MapTile>
	{
		public Iterator<MapTile> iterator()
		{
			return new TIterator();
		}
	}
	public static void main(String args[])
	{
		Terrain t = new Terrain(20,50,0,10);
		System.out.println(t);
	}
}
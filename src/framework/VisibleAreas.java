package framework;

import game.Player;
import game.Unit;

import java.util.ArrayList;

import main.GameDriver;

public class VisibleAreas {
	public Player owner;

	public VisibleAreas(Player p) {
		this.owner = p;
	}

	public ArrayList<Unit> getVisibleEnemyUnits() {
		ArrayList<Unit> list = new ArrayList<Unit>();
		for (Unit u : GameDriver.allUnits)
			if (u.owners[0].alliedWith(owner))
				for (Unit x : owner.units)
					if ((x.location.y - u.location.y) * (x.location.y - u.location.y) + (x.location.x - u.location.x)
							* (x.location.x - u.location.x) <= x.stats.sightRange * x.stats.sightRange)
						list.add(u);

		return list;
	}

	// for Fog Of War purposes
	public double getPercentShade(Point3D p) {
		double shade = .1;
		for (Unit u : owner.units)
			shade += Math.max(Math.min((double) u.stats.sightRange - u.location.distanceFrom(p), 1), 0);

		return Math.max(Math.min(shade, 1), 0);
	}

	public static int shadeFogOfWar(int rgb, double percent) {
		// (alpha << 24)|(red << 16)|(green << 8)|blue;
		int r = (rgb >> 16) & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = (rgb >> 0) & 0xff;
		return (255 << 24) | ((int) (percent * (percent * r + (1 - percent) * g + (1 - percent) * b)) << 16)
				| ((int) (percent * ((1 - percent) * r + percent * g + (1 - percent) * b)) << 8)
				| (int) (percent * ((1 - percent) * r + (1 - percent) * g + percent * b));
	}
}
package framework;

import util.Methods;

public class Vector3D {
	public double x, y, z;

	public Vector3D(double x, double y, double z) {
		set(x, y, z);
	}

	public double magnitude() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	public double getHorizontalAngle() {
		return Math.atan2(y, x);
	}

	public double getVerticalAngle() {
		return Math.acos(z / magnitude());
	}

	public double getMagnitude(AllignedPlane direction) {
		switch (direction) {
		case YZ:
			return Math.sqrt(y * y + z * z);
		case XZ:
			return Math.sqrt(x * x + z * z);
		default:
			return Math.sqrt(x * x + y * y);
		}
	}

	public double dotProduct(Vector3D other) {
		return x * other.x + y * other.y + z * other.z;
	}

	public double angleBetween(Vector3D other) {
		return Math.acos(dotProduct(other) / (magnitude() * other.magnitude()));
	}

	public void add(Vector3D v) {
		x += v.x;
		y += v.y;
		z += v.z;
	}

	public Point3D getPoint() {
		return new Point3D(x, y, z);
	}

	public void negate() {
		x *= -1;
		y *= -1;
		z *= -1;
	}

	public void unitize() {
		double m = magnitude();
		z /= m;
		x /= m;
		y /= m;
	}

	public void set(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString() {
		return "Vector3D(" + x + "," + y + "," + z + ")";
	}

	public void incrementAngle(double alpha) {
		double a = Methods.getRotateX(x, y, alpha);
		double b = Methods.getRotateY(x, y, alpha);
		x = a;
		y = b;
	}

	public void incrementVerticalAngle(double beta) {
		double c = Methods.getRotateY(Math.sqrt(x * x + y * y), z, beta);
		double b = Methods.getRotateX(y, z, beta);
		double a = Methods.getRotateX(x, z, beta);

		x = a;
		y = b;
		z = c;
	}
}

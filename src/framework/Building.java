package framework;

import game.StaticUnit;
import game.Unit;

public class Building extends Unit {
	private static final long serialVersionUID = -257959237603801831L;

	public Building(StaticUnit u, java.awt.Point loc) {
		super(u, loc);
	}
}
package framework;

import util.Methods;

public class FPSCamera extends Camera {
	private static final long serialVersionUID = -8285074356832369874L;
	public double alpha;

	public FPSCamera() {
		this(0, 0, 0);
	}

	public FPSCamera(double x, double y, double z) {
		this(x, y, z, 3 * Math.PI / 4, Math.PI / 2);
	}

	public FPSCamera(double x, double y, double z, double thetaH, double thetaV) {
		super(x, y, z);
		alpha = thetaH;
		beta = thetaV;
		horizontalFOV = Math.toRadians(90 / 2);
		verticalFOV = Math.toRadians(75 / 2);
		buffer.update();
	}

	public double getNorthboundAngle() {
		return alpha;
	}

	public void incrementHorizontalAngle(double amt) {
		buffer.alpha += amt;
	}

	public void setHorizontalAngle(double angle, boolean sendToBuffer) {
		if (sendToBuffer)
			buffer.alpha = Methods.normalize(angle);
		else
			this.alpha = angle;
	}

	public double getThetaXY() {
		return alpha;
	}
}
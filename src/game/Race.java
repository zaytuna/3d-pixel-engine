package game;

abstract class Race
{
	abstract java.util.List<StaticUnit> getPossibleUnitsToBuild(StaticBuilding b);
	abstract boolean hasPrerequisite(StaticUnit u, Player p);
	abstract java.util.List<String> getUnitNames();
	abstract String getInsufficientResourceMessage(int type);

	abstract StaticUnit loadUnit(String name);
	public static Race getRace(String name)
	{
		return getRace(name);
	}
}
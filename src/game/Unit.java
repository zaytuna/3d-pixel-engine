package game;

import framework.Animation;
import framework.MapObject;
import framework.Mesh;
import framework.Point3D;
import framework.Rect3D;

import java.awt.Point;

import main.GameDriver;

public class Unit extends StaticUnit implements MapObject, Runnable {
	private static final long serialVersionUID = -4512139365381439198L;

	public static int NORTH = 0;
	public static int SOUTH = 180;
	public static int EAST = 90;
	public static int WEST = 270;
	public static int HALF_RIGHT = 45;
	public static int HALF_LEFT = -45;

	public String currentState = "IDLE";
	public Stats stats;
	public Player[] owners;
	public Point3D location;
	private int direction = NORTH;
	int dirTo = NORTH;

	public Unit(StaticUnit type, Point start) {
		super(type);
		this.stats = new Stats(type.maxStats.attack, type.maxStats.coolDown, type.maxStats.attackRange,
				type.maxStats.sightRange, type.maxStats.defence, type.maxStats.health, type.maxStats.stamina,
				type.maxStats.sheilds);
		this.location = new Point3D(start.x, start.y, GameDriver.terrain.getHeightAt(start.x, start.y));
		mesh.setOffsetReference(location);
	}

	public Point3D getCenter() {
		return new Point3D(location.x, location.y, maxStats.height
				+ GameDriver.terrain.getHeightAt((int) location.x, (int) location.y));
	}

	public double getBoundingSphereRadius() {
		return maxStats.height;
	}

	public Mesh getMesh() {
		return mesh;
	}

	public void setAnimation(Animation a) {
		mesh.setAnimation(a);
	}

	public Rect3D getBoundingBox() {
		return new Rect3D(location.x, location.y, location.z, 1, 1, maxStats.height);
	}

	public void setOwner(Player... owners) {
		this.owners = owners;
	}

	public int getDirection() {
		return direction;
	}

	public void addOwner(Player next) {
		Player[] temp = new Player[owners.length + 1];
		for (int i = 0; i < owners.length; i++)
			temp[i] = owners[i];

		temp[owners.length] = next;
		owners = temp;
	}

	public void setDirection(int dir) {
		dirTo = dir;
		dirTo %= 360;

		new Thread(this).run();
	}

	public void turn(int dir) {
		setDirection(direction + dir);
	}

	public void run() {
		while (direction != dirTo) {
			try {
				Thread.sleep(60);
			} catch (Exception e) {
			}

			direction = direction + (direction - dirTo > dirTo - direction ? -1 : 1);
			direction %= 360;
		}
	}

	public class Stats {
		public int attack, accuracy = 100, coolDown, attackRange, sightRange, defence, heightOffGround = 0, health,
				stamina, sheilds;

		Stats(int a, int cool, int aRange, int sRange, int d, int h, int en, int sheild) {
			attack = a;
			coolDown = cool;
			attackRange = aRange;
			sightRange = sRange;
			defence = d;
			health = h;
			stamina = en;
			sheilds = sheild;
		}

		public void calculateDamage(int d) {
			sheilds -= d;
			health -= (d - sheilds);
		}
	}
}
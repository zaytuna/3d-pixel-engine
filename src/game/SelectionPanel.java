package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import util.Colorful;

class SelectionPanel extends Colorful {
	private static final long serialVersionUID = -4999003086903924026L;
	private ArrayList<Unit> selected = new ArrayList<Unit>();
	Player owner;

	SelectionPanel(Player p) {
		super(p.color.darker(), Color.WHITE, new Point(40, 200), new Point(20, 100));
		this.owner = p;
	}

	public void setUnitsSelected(ArrayList<Unit> units) {
		selected.clear();
		for (Unit u : units)
			selected.add(u);

		repaint();
	}

	public void clearSelection() {
		selected.clear();
	}

	public void paintComponent(Graphics gBLAH) {
		super.paintComponent(gBLAH);

		Graphics2D g = (Graphics2D) gBLAH;

		if (selected.size() <= 20)
			for (int i = 0; i < selected.size(); i++) {
				int v = (int) Math.ceil((double) selected.size() / (double) 2);

				Unit.Stats s = selected.get(i).stats;
				g.setColor(selected.get(i).owners[0].color);
				g.fillRect((int) (i / v) * 80, i % v * 70 + 20, 60, 75);
				g.setColor(opposite(g.getColor()));
				g.setFont(new Font("SERIF", Font.PLAIN, 10));

				g.drawString("Health: " + s.health, (int) (i / v) * 80 + 11, i % v * 70 + 22);
				g.drawString("Atk: " + s.attack, (int) (i / v) * 80 + 22, i % v * 70 + 22);
				g.drawString("Def: " + s.defence, (int) (i / v) * 80 + 33, i % v * 70 + 22);
				g.drawString("Energy: " + s.stamina, (int) (i / v) * 80 + 44, i % v * 70 + 22);
				g.drawString("Sheilds: " + s.stamina, (int) (i / v) * 80 + 55, i % v * 70 + 22);
			}
	}

	public static Color opposite(Color q) {
		return new Color((int) (q.getRed() + 255 / (double) 2) % 255, (int) (q.getGreen() + 255 / (double) 2) % 255,
				(int) (q.getBlue() + 255 / (double) 2) % 255);
	}
}
package game;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;

import main.GameDriver;
import main.GameWindow;
import util.Colorful;

public class SelectionArea extends Colorful implements ActionListener {
	private static final long serialVersionUID = -6558314247210352464L;
	public JTabbedPane pane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
	public SelectionPanel selectionPanel;
	public JToggleButton pause = new JToggleButton("Pause");

	public SelectionArea(Player p) {
		super(Color.BLUE, Color.BLACK, new Point(50, 50));

		selectionPanel = new SelectionPanel(p);

		setLayout(null);

		pane.setBounds(4, 5, 150, GameWindow.screen.height - 320);
		pause.setBounds(9, GameWindow.screen.height - 310, 140, 25);
		add(pane);
		add(pause);
		pause.addActionListener(this);
		pane.addTab("Selected", null, selectionPanel, "Selected Units");
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == pause) {
			GameDriver.mp.mute(!GameDriver.paused);
			GameDriver.paused = pause.isSelected();
			pause.transferFocus();
		}
	}
}
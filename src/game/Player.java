package game;

import framework.Camera;
import framework.FPSCamera;
import framework.LookAtCamera;
import framework.Point3D;
import framework.VisibleAreas;

import java.awt.Color;
import java.util.ArrayList;

import main.GameDriver;

public class Player {
	public ArrayList<Player> allies = new ArrayList<Player>();
	public ArrayList<Unit> units = new ArrayList<Unit>();
	public Color color;
	public String name;
	public Camera camera;
	LookAtCamera lac;
	VisibleAreas va;
	FPSCamera fpc;

	int gold = 50;
	int wood = 50;
	int food = 10;
	int water = 10;
	int iron = 0;
	int hydrogen = 0;
	int stone = 50;

	public Player(Color c, Point3D start) {
		color = c;
		fpc = new FPSCamera(start.x, start.y, start.z);
		lac = new LookAtCamera(start.x, start.y, start.z);

		camera = lac;

		va = new VisibleAreas(this);
	}

	public void setToFPSCamera() {
		camera = fpc;
	}

	public void setToRTSCamera() {
		lac.set(camera.x, camera.y, camera.z);
		lac.setHorizontalAngle(camera.getThetaXY());
		lac.setVerticalAngle(camera.getThetaXYZ());
		camera = lac;
	}

	public boolean alliedWith(Player p) {
		return (p == this || allies.contains(p));
	}

	public Point3D getLocation() {
		return camera;
	}

	public String toString() {
		return name + "\n" + camera.toString();
	}

	public java.util.ArrayList<Unit> getVisibleEnemyUnits() {
		return va.getVisibleEnemyUnits();
	}

	public void addUnit(Unit u) {
		units.add(u);
		GameDriver.allUnits.add(u);
	}
}
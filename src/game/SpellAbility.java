package game;
import java.awt.Point;

import util.EvaluateMethod;

import framework.Point2D;

class SpellAblility
{
	String action;
	int radius;

	SpellAblility(String a, int r)
	{
		this.action = a;
		this.radius = r;
	}

	public void cast(Point2D p, Player owner, boolean affectsSelf, boolean affectsEnemies)
	{

	}
	public void act(Unit u)
	{
		String[] split = action.split("=");

		double value = EvaluateMethod.evaluate(split[1].replaceAll("health",u.stats.health+"").
			replaceAll("defence",u.stats.defence+"").replaceAll("attack",u.stats.attack+"").
			replaceAll("coolDown",u.stats.coolDown+"").replaceAll("srange",u.stats.sightRange+"").
			replaceAll("arange",u.stats.attackRange+"").replaceAll("accuracy",u.stats.accuracy+"").
			replaceAll("stamina",u.stats.stamina+"").replaceAll("sheilds",u.stats.sheilds+""));


		if(split[0].contains("health"))
			u.stats.health = (int)value;
		else if(split[0].contains("damage"))
			u.stats.calculateDamage((int)value);
		else if(split[0].contains("sheilds"))
			u.stats.sheilds = (int)value;
		else if(split[0].contains("accuracy"))
			u.stats.accuracy = (int)value;
		else if(split[0].contains("stamina"))
			u.stats.stamina = (int)value;
		else if(split[0].contains("arange"))
			u.stats.attackRange = (int)value;
		else if(split[0].contains("srange"))
			u.stats.sightRange = (int)value;
		else if(split[0].contains("coolDown"))
			u.stats.coolDown = (int)value;
		else if(split[0].contains("defence"))
			u.stats.defence = (int)value;
		else if(split[0].contains("attack"))
			u.stats.attack = (int)value;
	}
	public static boolean areWithin(Point p1, Point p2, int d)
	{
		return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))<=d;
	}
}
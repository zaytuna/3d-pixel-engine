package game;

import java.util.ArrayList;
import java.util.Queue;

class StaticBuilding extends StaticUnit {
	private static final long serialVersionUID = -6072535894151283518L;
	ArrayList<Queue<Upgrade>> upgrades;

	StaticBuilding(int a, int cool, int aRange, int sRange, int d, int h, int en, int sheild, int hei, int spd,
			String... states) {
		super(a, cool, aRange, sRange, d, h, en, sheild, hei, spd, states);
	}
}
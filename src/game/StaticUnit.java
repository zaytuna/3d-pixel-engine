package game;

import framework.SkinnedMesh;

public class StaticUnit implements java.io.Serializable {
	private static final long serialVersionUID = 2453519943575331314L;
	public boolean isDrawable = false;
	public MaxStats maxStats;
	public String[] states;
	protected SkinnedMesh mesh;

	public StaticUnit(int a, int cool, int aRange, int sRange, int d, int h, int en, int sheild, int hei, int spd,
			String... states) {
		this.maxStats = new MaxStats(a, cool, aRange, sRange, d, h, en, sheild, hei, spd);
		this.states = states;
	}

	StaticUnit(StaticUnit s) {
		this.maxStats = new MaxStats(s.maxStats.attack, s.maxStats.coolDown, s.maxStats.attackRange,
				s.maxStats.sightRange, s.maxStats.defence, s.maxStats.height, s.maxStats.stamina, s.maxStats.sheilds,
				s.maxStats.height, s.maxStats.speed);
		this.states = s.states;
	}

	public void setPossibleStates(String... states) {
		this.states = states;
	}

	public void setPossibleStates(java.util.ArrayList<String> states) {
		this.states = new String[states.size()];
		for (int i = 0; i < states.size(); i++)
			this.states[i] = states.get(i);
	}

	public void setRendition(SkinnedMesh s) {
		mesh = s;
		isDrawable = true;
	}

	public SkinnedMesh getRendition() {
		return mesh;
	}

	public class MaxStats implements java.io.Serializable {
		private static final long serialVersionUID = -4970948244036490320L;
		public int attack, accuracy = 100, coolDown, attackRange, sightRange, defence, health, stamina, sheilds,
				height, speed;

		public MaxStats(int a, int cool, int aRange, int sRange, int d, int h, int en, int sheild, int hei, int sped) {
			attack = a;
			coolDown = cool;
			attackRange = aRange;
			sightRange = sRange;
			defence = d;
			health = h;
			stamina = en;
			sheilds = sheild;
			height = hei;
			speed = sped;
		}
	}
}
package game;

import java.util.*;
import java.lang.reflect.*;

class Upgrade
{
	String name;
	int maxTime;
	ArrayList<Integer> amounts = new ArrayList<Integer>();
	Field field;
	double done = 0;
	ArrayList<StaticUnit> targets;

	Upgrade(int time, Field f, ArrayList<StaticUnit> u, ArrayList<Integer> amts, String name)
	{
		done = 0;
		maxTime = time;
		field = f;
		targets = u;
		this.amounts = amts;
		this.name = name;
	}

	public void nextStep(int efficiency)
	{
		done+= efficiency/(double)maxTime;
	}

	public boolean isDone()
	{
		return done>=1;
	}

	public void finish()
	{
	}
}